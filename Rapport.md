# Rapport de TP : Base de données #

| Nom Prénom     | Numéro étudiant  |
| :------------- | :--------------- |
| SHEIKH Rakib   | 11502605         |
| BOUAFIA Mehdi  | 11607146         |

---
Liens direct faisant référence au TP :

*   [TP 1](#TP1)
*   [TP 2 : Modification d'une base de données sous ORACLE](#TP-2-:-Modification-d'une-base-de-données-sous-ORACLE)
*   [TP 3 : Fonctions Oracles](TP-3-:-Fonctions-Oracles)
*   [TP 4 : SQL Simple, tri et regroupements](#TP-4-:-SQL-Simple,-tri-et-regroupements)
*   [TP 5 : Jointures](#TP-5-:-Jointures)

## TP1 ##

### B. Premiere création de table ###

1.  Création de la table etudiants

```sql
@etudiants.sql
```

2.  Vérification de la table avec

```sql
desc etudiants;
```

```bash
Nom             NULL ?   Type
--------------- -------- -------------
NUMERO          NOT NULL NUMBER(4)
NOM             NOT NULL VARCHAR2(25)
PRENOM          NOT NULL VARCHAR2(25)
SEXE                     CHAR(1)
DATEDENAISSANCE NOT NULL DATE
POIDS                    NUMBER
ANNEE                    NUMBER
```

```sql
select * from etudiants;
```

```bash
aucune ligne sélectionnée
```

desc nom_table affiche les attributs d'une table ainsi que leurs contraintes c'est-à-dire le type, la taille max et si l'attribut peut être NULL ou non(elle ne montre pas les CHECK).
select * from nom_table affiche tout le contenue d'une table.

3.  Définition de l'attribue de la clé primaire.

```sql
numero        NUMBER(4)     PRIMARY KEY,
```

L'attribut numero est la clé primaire.

4.  Les contraintes définit sont :

*   numéro non null && unique (car clée primaire) && 4 carac max
*   Nom non null && 25 carac max
*   Prenom non null && 25 carac max
*   Sexe soit F, soit M && 1 carac max
*   dateDeNaissance non null.

5.  Ajout des lignes suivantes:

```sql
INSERT INTO etudiants VALUES (71, 'Nervac', 'Feguir', 'M', '10/12/1978', 77, 1);
INSERT INTO etudiants VALUES (72, 'Bayard', 'Emeric', 'M', '21/11/1995', 80, 5);
INSERT INTO etudiants VALUES (73, 'Saddik', 'Nady', 'M', '24/05/1999', 15, 2);
INSERT INTO etudiants VALUES (74, 'Sy', 'Jeff', 'M', '01/10/1995', 70, 4);
INSERT INTO etudiants VALUES (69, 'Saguez', 'Virginie', 'F', '10/06/1998', 70, 1);
INSERT INTO etudiants VALUES (55, 'Kashem', 'Sadman', 'M', '10/03/1997', 90, 1);

select * from etudiants;
```

```bash
NUMERO  NOM      PRENOM   S DATEDENA POIDS  ANNEE
------- -------- -------- - -------- ------ ------
71      Nervac   Feguir   M 10/12/78 77     1
72      Bayard   Emeric   M 21/11/95 80     5
73      Saddik   Nady     M 24/05/99 15     2
74      Sy       Jeff     M 01/10/95 70     4
69      Saguez   Virginie F 10/06/98 70     1
55      Kashem   Sadman   M 10/03/97 90     1
```

6.  Ajout de lignes violant les contraintes définies:

```sql
INSERT INTO etudiants VALUES (71, 'Akito', 'Diamando', 'M', '06/07/1995', 77, 1);
```

```bash
ORA-00001: violation de contrainte unique (L3INFO_131.SYS_C001018742)
```

```sql
INSERT INTO etudiants VALUES (21, 'Akito', 'Diamando', 'N', '06/07/1995', 77, 1);
```

```bash
ORA-02290: violation de contraintes (L3INFO_131.SYS_C001018741) de v�rification
```

```sql
INSERT INTO etudiants VALUES (21, 'Akito', 'Diamando', 'M', NULL, 77, 1);
```

```bash
ORA-01400: impossible dinsérer NULL dans ("L3INFO_131"."ETUDIANTS"."DATEDENAISSANCE")
```

On ne peut donc pas insérer une ligne si les contraintes ne sont pas respectés.
Toutes les contraintes sont bien vérifiées.


### C. Base de données avec plusieurs tables ###

```sql
@ecole.sql
```

<div class="page-break"></div>

## TP 2 : Modification d'une base de données sous ORACLE ##

### A. Modification de contraintes ###

1.  Nécessité d'une meilleur définition de contrainte

```sql
CREATE TABLE ETUDIANTS(
  NUMERO        NUMBER(4)     NOT NULL,
  NOM           VARCHAR2(25)  NOT NULL,
  PRENOM        VARCHAR2(25)  NOT NULL,
  SEXE          CHAR(1)       CHECK (SEXE IN ('F', 'M')),
  DATENAISSANCE DATE          NOT NULL,
  POIDS         NUMBER,
  ANNEE         NUMBER,
  CONSTRAINT PK_ETUDIANTS PRIMARY KEY (NUMERO)
);
```

Il est évident qu'il faut une meilleur definition des contraintes car elles ne sont pas nommées, en effet, les contraintes actuelles sont affiché en codé de la manière suivante :

```sql
ttitle ‘La liste des contraintes créées, sur la table ETUDIANTS, est :’
select constraint_name from user_constraints where table_name='ETUDIANTS';
```

```bash
‘Lalistedescontraintescréées,surlatableETUDIANTS,est:’
CONSTRAINT_NAME
------------------------------
SYS_C001019223
SYS_C001019224
SYS_C001019225
SYS_C001019226
SYS_C001019227
PK_ETUDIANTS
```

2.  Redéfinition de la table en nommant les contraintes

```sql
DROP TABLE ETUDIANTS;

CREATE TABLE  ETUDIANTS (
  NUMERO          NUMBER(4),
  NOM             VARCHAR2(25),
  PRENOM          VARCHAR2(25),
  SEXE            CHAR(1),
  DATENAISSANCE   DATE,
  POIDS           NUMBER,
  ANNEE           NUMBER,
  CONSTRAINT PK_ETUDIANTS PRIMARY KEY (NUMERO),
  CONSTRAINT NN_ETUDIANTS_NOM CHECK (NOM IS NOT NULL),
  CONSTRAINT NN_ETUDIANTS_PRENOM CHECK (PRENOM IS NOT NULL),
  CONSTRAINT CHECK_FM_ETUDIANTS_SEXE CHECK (SEXE IN ('F', 'M')),
  CONSTRAINT NN_ETUDIANTS_DATENAISSANCE CHECK (DATENAISSANCE IS NOT NULL)
);

ttitle ‘La liste des contraintes créées, sur la table ETUDIANTS, est :’
select constraint_name from user_constraints where table_name='ETUDIANTS';
```

```bash
‘Lalistedescontraintescréées,surlatableETUDIANTS,est:’
CONSTRAINT_NAME
------------------------------
NN_ETUDIANTS_NOM
NN_ETUDIANTS_PRENOM
CHECK_FM_ETUDIANTS_SEXE
NN_ETUDIANTS_DATENAISSANCE
PK_ETUDIANTS
```

Grâce a cette définition des contraintes il est plus simple d'identifier les contraintes qui ont été violé puisqu'elle sont nommées.

3.  Ajout de 2 contraintes

```sql
ALTER TABLE etudiants ADD CONSTRAINT CHECK_ETUDIANTS_ANNEE CHECK (annee = 1 OR annee = 2);
ALTER TABLE etudiants ADD CONSTRAINT CHECK_ETUDIANTS_POIDS CHECK (POIDS > 30 OR POIDS < 200);
```

4.  Modification des noms des contraintes selon la norme

```sql
ALTER TABLE etudiants RENAME CONSTRAINT CHECK_FM_ETUDIANTS_SEXE to CK_ETUDIANTS_SEXE;
ALTER TABLE etudiants RENAME CONSTRAINT CHECK_ETUDIANTS_ANNEE to CK_ETUDIANTS_ANNEE;
ALTER TABLE etudiants RENAME CONSTRAINT CHECK_ETUDIANTS_POIDS to CK_ETUDIANTS_POIDS;
```
On obtient alors la table se trouvant dans etudiants.sql .

### B- Manipulations de la BD Ecole ###

2.  Ajout des clées étrangères

```sql
/* ACTIVITES_PRATIQUEES */
ALTER TABLE ACTIVITES_PRATIQUEES ADD CONSTRAINT FK_ActPr_num_Eleve_Eleves FOREIGN KEY (NUM_ELEVE) REFERENCES ELEVES(num_eleve);
-- Ici il faut mettre dans la contraintes tout les attributs en meme temps, sinon ca fait une erreur de correspondance de clée primaire ou unique pour cette liste de colonne.
ALTER TABLE ACTIVITES_PRATIQUEES ADD CONSTRAINT FK_ActPr_niveau_ACTIVITES FOREIGN KEY (niveau,nom) REFERENCES ACTIVITES(NIVEAU, NOM);
-- CHARGE
ALTER TABLE CHARGE ADD CONSTRAINT FK_CHARGE_NumProf_PROFESSEURS FOREIGN KEY (Num_Prof) REFERENCES PROFESSEURS(NUM_PROF);
ALTER TABLE CHARGE ADD CONSTRAINT FK_CHARGE_NumCours_COURS FOREIGN KEY (Num_Cours) REFERENCES COURS(NUM_COURS);
-- RESULTATS
ALTER TABLE RESULTATS ADD CONSTRAINT FK_RESULTATS_NumEleve_ELEVES FOREIGN KEY (num_eleve) REFERENCES ELEVES(num_eleve);
ALTER TABLE RESULTATS ADD CONSTRAINT FK_RESULTATS_NumCours_COURS FOREIGN KEY (Num_Cours) REFERENCES COURS(NUM_COURS);
```

3.  Affichage de la structure ELEVES

On fait appel à :

```sql
desc ELEVES;
```

pour afficher la structure de la table.

```bash
Nom             NULL ?   Type
--------------- -------- -------------
NUM_ELEVE       NOT NULL NUMBER(4)
NOM                      VARCHAR2(25)
PRENOM                   VARCHAR2(25)
DATE_NAISSANCE           DATE
POIDS                    NUMBER
ANNEE                    NUMBER
SEXE                     CHAR(1)
```

On fait appel à :

```sql
SELECT * FROM ELEVES;
```

pour lister le contenu de la table.

```bash
Lalistedescontraintescréées,surlatableETUDIANTS,est:
 NUM_ELEVE NOM           PRENOM   DATE_NAI POIDS  ANNEE S
---------- ------------- -------- -------- ------ ------ -
1          Brisefer      Benoit   10/12/78     35      1 M
2          Génial        Olivier  10/04/78     42      1 M
3          Jourdan       Gil      28/06/74     72      2 F
4          Spring        Jerry    16/02/74     78      2 M
5          Tsuno         Yoko     29/10/77     45      1 F
6          Lebut         Marc     29/04/74     75      2 M
7          Lagaffe       Gaston   08/04/75     61      1 M
8          Dubois        Robin    20/04/76     60      2 M
9          Walthéry      Natacha  07/09/77     59      1 F
10         Danny         Buck     15/02/73     82      2 M
```

4.  Ajout des attributs suivants

```sql
ALTER TABLE ELEVES ADD (
	CodePostal NUMBER(5),
	Ville  VARCHAR(20)
);
```

5.  MAJ des adresses des élèves

```sql
UPDATE ELEVES SET Ville = 'PARIS', CodePostal = 75013 WHERE num_eleve = 1;
UPDATE ELEVES SET Ville = 'EPINAY / seine', CodePostal = 93800 WHERE num_eleve = 2;
UPDATE ELEVES SET Ville = 'EPINAY SUR SEINE', CodePostal = 93430 WHERE num_eleve = 5;
UPDATE ELEVES SET Ville = 'EPINAY / ORGE', CodePostal = 91000 WHERE num_eleve = 7;
```

contenue de la table eleves après update :

```bash
NUM_ELEVE  NOM       PRENOM    DATE_NAI  POIDS ANNEE S CODEPOSTAL VILLE
---------- -------- ---------- -------- ------ ----- - ---------- --------------------
1          Brisefer  Benoit    10/12/78  35     1    M 75013       PARIS
2          Génial    Olivier   10/04/78  42     1    M 93800       EPINAY / seine
3          Jourdan   Gil       28/06/74  72     2    F
4          Spring    Jerry     16/02/74  78     2    M
5          Tsuno     Yoko      29/10/77  45     1    F 93430       EPINAY SUR SEINE
6          Lebut     Marc      29/04/74  75     2    M
7          Lagaffe   Gaston    08/04/75  61     1    M 91000       EPINAY / ORGE
8          Dubois    Robin     20/04/76  60     2    M
9          Walthéry  Natacha   07/09/77  59     1    F
10         Danny     Buck      15/02/73  82     2    M
```


6.  Création d'un nouvelle agglomeration

```sql
CREATE TABLE AGGLOMERATION (
  CodePostal NUMBER(5),
  VILLE VARCHAR(20),
  CONSTRAINT PK_AGGLOMERATION_CP PRIMARY KEY (CodePostal)
);
```

7.  Ajout des contraintes

```sql
ALTER TABLE AGGLOMERATION ADD CONSTRAINT CK_AGGLOMERATION_VILLE CHECK (VILLE=UPPER(VILLE));
ALTER TABLE ELEVES ADD CONSTRAINT FK_ELEVES_AGGLOMERATION FOREIGN KEY (CodePostal) REFERENCES agglomeration(CodePostal);
```

8.  Parmi les données qui sont rappelées juste en dessous, lequelles vont donner une erreur ?

93800 EPINAY SUR SEINE et 93800 EPINAY/SEINE ont le même code postale or CodePostal est une clée primaire.
Villetaneuse n'est pas en UPPER donc elle ne respecte pas la contrainte que nous avons ajouté a la question 7.

```sql
INSERT INTO agglomeration values (75001,'PARIS');
INSERT INTO agglomeration values (75013,'PARIS');
INSERT INTO agglomeration values (93800,'EPINAY SUR SEINE');
INSERT INTO agglomeration values (93430,'Villetaneuse');
INSERT INTO agglomeration values (91000,'EPINAY SUR ORGE');
INSERT INTO agglomeration values (93800,'EPINAY/SEINE');
```

```bash
SQL> INSERT INTO agglomeration values (75001,'PARIS');
1 ligne cr��e.

SQL> INSERT INTO agglomeration values (75013,'PARIS');
1 ligne cr��e.

SQL> INSERT INTO agglomeration values (93800,'EPINAY SUR SEINE');
1 ligne cr��e.

SQL> INSERT INTO agglomeration values (93430,'Villetaneuse')
ERREUR � la ligne 1 :
ORA-02290: violation de contraintes (L3INFO_131.CK_AGGLOMERATION_VILLE) de
v�rification

SQL> INSERT INTO agglomeration values (91000,'EPINAY SUR ORGE');
1 ligne cr��e.

SQL> INSERT INTO agglomeration values (93800,'EPINAY/SEINE')
ERREUR � la ligne 1 :
ORA-00001: violation de contrainte unique (L3INFO_131.PK_AGGLOMERATION_CP)
```

```sql
INSERT INTO agglomeration values (93430,'VILLETANEUSE');
```

9.  Mettez à jour la table des élèves pour corriger les nom des villes selon le code postal. Utilisez une seule requête. Affichez son contenu corrigé.

```sql
UPDATE ELEVES set Ville = (SELECT Ville from agglomeration where eleves.CodePostal = agglomeration.CodePostal );
select * from eleves;
```

```bash
NUM_ELEVE  NOM        PRENOM    DATE_NAISS  POIDS  ANNEE  S CODEPOSTAL VILLE
---------- ---------- ---------  ---------- ------ ------ - ---------- --------------------
1          Brisefe    Benoit     10-12-1978 3       1     M 75013      PARIS
2          Génial     Olivier    10-04-1978 42      1     M 93800      EPINAY SUR SEINE
3          Jourdan    Gil        28-06-1974 72      2     F
4          Spring     Jerry      16-02-1974 78      2     M
5          Tsuno      Yoko       29-10-1977 45      1     F 93430      VILLETANEUSE
6          Lebut      Marc       29-04-1974 75      2     M
7          Lagaffe    Gaston     08-04-1975 61      1     M 91000      EPINAY SUR ORGE
8          Dubois     Robin      20-04-1976 60      2     M
9          Walthéry   Natacha    07-09-1977 59	    1     F
10         Danny      Buck       15-02-1973 82      2     M
```

<div class="page-break"></div>

## TP 3 : Fonctions Oracles ##

### A. Exploration de quelques fonctions ORACLE ###

1.  Test des commandes suivantes et explications:

```sql
SELECT RPAD('Soleil',17,'bla') "RPAD exemple" FROM DUAL;
```

Ecrit soleil, le concatène avec 'bla' jusqu'à 17 caractères. (Right padding)

```sql
SELECT LPAD('Master 2 EID',15,'*.') "LPAD exemple" FROM DUAL;
```

Ecrit le caractère '\*.' devant Master 2 EID par concatenation plusieurs fois dans la limite de 15 caractère. (Left padding)

```sql
SELECT SUBSTR('DESS EID',6,3) "SUBSTR exemple" FROM DUAL;
```

Affiche 3 caractère à partir de la sixième case qui est le .

```sql
SELECT SUBSTR('ABCDEFGHIJ',-5,4) "SUBSTR exemple" FROM DUAL;
```

Affiche les 4 caractère suivantes à partir de la case -5 en partant de la fin (donc la case 5 en partant du début).

```sql
SELECT TO_CHAR (SYSDATE, 'MM-DD-YYYY HH24:MI:SS') "Now" FROM DUAL;
```
Affiche la date actuelle et heure du systeme hôte qui heberge le serveur SQL avec ayant comme étiquette "Now".

```sql
SELECT LENGTH('WEB WAREHOUSE') "Longueur en caractères" FROM DUAL;
```
Calcule et affiche la longueur de la chaine WEB WAREHOUSE.

```sql
SELECT ROUND(17.0958,1) "ROUND exemple" FROM DUAL;
```

Arrondie le nombre 17.0958 à 1 chiffre après la virgule.

```sql
SELECT ROUND(17.58,2) "ROUND exemple" FROM DUAL;
```

Arrondie le nombre 17.58 à 2 chiffre apèrs la virgule. (Aucun changement dans ce cas)

```sql
SELECT TRUNC(1958.0917,1) "TRUNC exemple" FROM DUAL;
```
Troncature : Affiche le nombre à 1 chiffre après la virgule sans arrondir.

```sql
SELECT TRUNC(1958.0917,2) "TRUNC exemple" FROM DUAL;
```
Troncature : Affiche le nombre à 2 chiffre après la virgule.

```sql
SELECT ROUND(TO_DATE('17-SEP-2009'), 'YEAR') "New Year" FROM DUAL;
```

Affiche une erreur à cause du SEP, il faut mettre 09 ou 9 pour que ça marche.
Cette requête SQL arrondi une date en +1 pour les mois et année, et au premier jour du mois pour ce qui concerne du jour.

Version corrigé de la requête.
```sql
SELECT ROUND(TO_DATE('17-9-2009'), 'YEAR') "New Year" FROM DUAL;
```

```sql
SELECT SYSDATE FROM DUAL;
```
Affiche la date d'aujourd'hui.

```sql
SELECT EXTRACT(YEAR FROM SYSDATE) FROM DUAL;
```
Recupère la date de l'hôte, et affiche uniquement l'année par extraction de cette date.

```sql
SELECT ADD_MONTHS(SYSDATE,7) FROM DUAL;
```

Ajoute 7 mois à l'affichage de la date de l'hôte.

```sql
SELECT TRUNC(MONTHS_BETWEEN(SYSDATE, TO_DATE('19-JUN-2001'))) AS AGEBB FROM DUAL;
```
Ne marche pas pour la meme raison plus haut. A remplacer par la requête suivante

```sql
SELECT TRUNC(MONTHS_BETWEEN(SYSDATE, TO_DATE('19-6-2001'))) AS AGEBB FROM DUAL;
```

Affiche la différence de mois entre la date donnée et la date de l'hote, le stock dans la colonne AGEBB.

```sql
SELECT TO_NUMBER(TO_CHAR(SYSDATE, 'YYYY')) FROM DUAL;
```
Converti l'année de l'hôte en char, et le reconvertie en nombre.


2.  Changement de format du type de date.

```sql
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY';
```

### B. Exemple sur la vrai table ###

2.  Insertion des lignes :

```sql
INSERT INTO ETUDIANTS VALUES (71, 'Traifor', 'Benoit', 'M', '10/12/1978', 77, 1);
INSERT INTO ETUDIANTS VALUES (72, 'Génial', 'Clément', 'M', '10/04/1978', 72, 1);
INSERT INTO ETUDIANTS VALUES (73, 'Paris', 'Adam', 'M', '28/06/1974', 72, 1);
INSERT INTO ETUDIANTS VALUES (74, 'Parees', 'Clémence', 'F', '20/09/1977', 72, NULL);
INSERT INTO ETUDIANTS VALUES (69, 'Saitout', 'Inès', 'F', '22/11/1969', 69, 2);
INSERT INTO ETUDIANTS VALUES (55, 'Serafoub', 'Izouaf', 'M', '19/09/2013', 81, 1);

```


3.  Test de requêtes

```sql
SELECT DECODE(ANNEE, 1, 'Première', 2, 'Seconde', 'Valeur différente de 1 et de 2 !!') AS ANETUDE FROM ETUDIANTS;
```

```bash
ANETUDE
----------------------------------
Première
Première
Première
Valeur différente de
1 et de 2 !!

Seconde
Première

6 lignes s�lectionn�es.
```

Cette commande DECODE permet de remplacer toute occurence de X par Y.
Dans l'exemple de la requête ci-dessus, Toute occurence de 1 sera remplacé par Première, il en est de même pour 2. En revanche, Toute occurence vide sera remplacé par Valeur différente de 1 et de 2.


```sql
SELECT UPPER(NOM) FROM ETUDIANTS;
```

Cette commande UPPER permer de Capitaliser toute les lettre de nom de la table etudiants (Mise en format du résultat).

```bash
UPPER(NOM)
-------------------------
TRAIFOR
GéNIAL
PARIS
PAREES
SAITOUT
SERAFOUB

6 lignes s�lectionn�es.
```

```sql
SELECT LOWER(NOM) FROM ETUDIANTS;
```
La requête lower permet de mettre tous les lettres de nom en minuscule dans le format de sortie

```bash

LOWER(NOM)
-------------------------
traifor
g��nial
paris
parees
saitout
serafoub

6 lignes s�lectionn�es.
```
Quel est le problème dans la fonction NVL ?
Le problème est que le deuxième argument de nvl doit être du même type de que l'attribue de la table. On peut y corriger en mettant -1 pour l'exemple donnée.

```sql
SELECT NVL(ANNEE, '-1') FROM ETUDIANTS;
```
Remplace les null par une autre valeur.

```sql
SELECT NVL(ANNEE, '-1') AS AN_ETUDE FROM ETUDIANTS;
```

Pareille que le précédente en donnant à la colonne le nom AN_ETUDE.

4.  Gestion de l'affichage.

```sql
COL poids FORMAT 999
TTITLE "Etudiants"
set pages 50
set line 50
```

5.  Interrogation de la BD

*   Affichez le nom et le prénom de tous les étudiants en une seule colonne.

```sql
select (concat(nom,concat(' ', prenom)))"NOM PRENOM" from etudiants;
```

```bash
NOM PRENOM
---------------------------------------------------
Traifor Benoit
Génial Clément
Paris Adam
Parees Clémence
Saitout Inès
Serafoub Izouaf
```

*   Affichez la première lettre du prénom en majuscule suivie d'un point et d'un
espace (. ) suivis du nom en majuscule en une seule colonne, pour les
étudiants de sexe masculin.

```sql
select (concat(concat(upper(SUBSTR(prenom,1,1)), '. '), upper(nom))) from etudiants;
```

```bash
----------------------------
B. TRAIFOR
C. GéNIAL
A. PARIS
C. PAREES
I. SAITOUT
I. SERAFOUB
```

*   Affichez le nom et l’année de naissance des étudiants dont le nom se
prononce comme ‘Paris’.

```sql
SELECT concat (nom, EXTRACT(YEAR from datenaissance)) from etudiants where soundex(nom) = soundex('paris');
```

```bash
CONCAT(NOM,EXTRACT(YEARFROMDATENAISSANCE))
-----------------------------------------------------------------
Paris1974
Parees1977
```

*   Affichez le nom et l’année de naissance des étudiants dont le prénom
commence par la lettre I.

```sql
SELECT concat (nom, EXTRACT(YEAR from datenaissance)) from etudiants where  SUBSTR(prenom,1,1) = 'I';
```

```bash
CONCAT(NOM,EXTRACT(YEARFROMDATENAISSANCE))
-----------------------------------------------------------------
Saitout1969
Serafoub2013
```

*   Proposez d'autres requêtes répondant à d'autres questions. Soyez créatif !

Afficher la liste des étudiants qui sont plus lourd que 70 kgs

```sql
select nom, prenom from etudiants where poids > 70;
```

```bash
NOM       PRENOM    POIDS
--------- --------- ------
Traifor   Benoit    77
Génial    Clément   72
Paris     Adam      72
Parees    Clémence  72
Serafoub  Izouaf    81
```

Afficher la liste des étudiants par ordre croissant de leur nom

```sql
select nom, prenom from etudiants order by nom asc;
```

```bash
NOM         PRENOM
----------- -------------------------
Génial      Clément
Parees      Clémence
Paris       Adam
Saitout     Inès
Serafoub    Izouaf
Traifor     Benoit

6 lignes s�lectionn�es.

```

<div class="page-break"></div>

## TP 4 : SQL Simple, tri et regroupements ##

### A. Table employés ###


1.  Creation de la table

```sql
drop table employees cascade constraint;

create table employees (
  NumEmp  NUMBER(4),
  NomEmp  VARCHAR(25),
  DateEmb DATE,
  DateSortie DATE,
  CONSTRAINT pk_employes PRIMARY KEY (NumEmp),
  CONSTRAINT nn_emp_nomEMP CHECK (NomEmp IS NOT NULL),
  CONSTRAINT nn_emp_DateEmb CHECK (DateEmb IS NOT NULL)
);
```

On ajoute des valeurs dans la table employees.

```sql
INSERT INTO EMPLOYEES VALUES (9007,'CHEVALIER','01/01/96',NULL);
INSERT INTO EMPLOYEES VALUES (9701,'LEROY','17/09/97',NULL);
INSERT INTO EMPLOYEES VALUES (9703,'LAMI','17/09/97',NULL);
INSERT INTO EMPLOYEES VALUES (9801,'SULTAN','20/03/98',NULL);
INSERT INTO EMPLOYEES VALUES (9802,'CLEMENCE','16/10/98',NULL);
INSERT INTO EMPLOYEES VALUES (9803,'CAVALIER','22/11/98',NULL);
INSERT INTO EMPLOYEES VALUES (9901,'ALEXANDRE','21/02/99',NULL);
```

2.  Formulation et test des requêtes.

*   Liste des employés

```sql
SELECT * FROM employees;
```

```bash
NUMEMP     NOMEMP               DATEEMB  DATESORT
---------- ------------------- -------- --------
  9007     CHEVALIER            01/01/96
  9701     LEROY                17/09/97
  9703     LAMI                 17/09/97
  9801     SULTAN               20/03/98
  9802     CLEMENCE             16/10/98
  9803     CAVALIER             22/11/98
  9901     ALEXANDRE            21/02/99
```

*   Liste des noms de tous les employés

```sql
SELECT NomEmp "Nom des employés" FROM employees;
```

```bash
Nom des employés
-------------------------
CHEVALIER
LEROY
LAMI
SULTAN
CLEMENCE
CAVALIER
ALEXANDRE
```
*   Liste des noms des employés embauchés à patir du 1er Janvier 1999

```sql
SELECT NomEmp "Nom des employés" FROM employees where DateEmb > '01/01/99';
```

```bash
Nom des employés
-------------------------
ALEXANDRE
```
*   Liste des employés (Num et Nom) dont le nom commence par la lettre C

```sql
SELECT concat(NumEmp,concat(' ', NomEmp))"Nom et Num des employees" from employees where SUBSTR(NomEmp,1,1) = 'C';
--
select numemp, nomemp from employes where nomemp like 'C%';
```

```bash
Nom et Num des employees
------------------------------------------------------------------
9007 CHEVALIER
9802 CLEMENCE
9803 CAVALIER
```
*   Liste des employés triés par ordre décroissant sur les noms

```sql
SELECT *  FROM employees ORDER BY NomEmp DESC;
```

```bash
NUMEMP      NOMEMP                   DATEEMB  DATESORT
---------- ------------------------- -------- --------
  9801     SULTAN                    20/03/98
  9701     LEROY                     17/09/97
  9703     LAMI                      17/09/97
  9802     CLEMENCE                  16/10/98
  9007     CHEVALIER                 01/01/96
  9803     CAVALIER                  22/11/98
  9901     ALEXANDRE                 21/02/99
```

*   Nombre d’employés embauchés chaque année

```sql
SELECT EXTRACT(YEAR FROM DateEmb)"Année", COUNT(*)"Nombre d'employés embauché" FROM employees GROUP BY EXTRACT(YEAR FROM DateEmb);
```

```bash
Année      Nombre d\'employés embauchés
---------- ----------------------------
  1997      2
  1999      1
  1996      1
  1998      3
```

*   Nombre d’employés embauchés chaque année ayant un nom de plus de 5
lettres

```sql
SELECT EXTRACT(YEAR FROM DateEmb)"Année", COUNT(*)"Nombre d'employés embauché" FROM employees WHERE LENGTH(NomEmp) > 5 GROUP BY EXTRACT(YEAR FROM DateEmb) ;
```

```bash
Année Nombre d\'employés embauché
---------- ----------------------------
  1999      1
  1996      1
  1998      3
```

*   Nombre d’employés embauchés chaque année ayant un nom commençant
par L ou C, en ne gardant que les années avec au moins deux employés

```sql
SELECT EXTRACT(YEAR FROM DateEmb)"Année", COUNT(*) "Nombre d'employés embauché" FROM employees WHERE SUBSTR(NomEmp,1,1) = 'L'OR SUBSTR(NomEmp,1,1)  = 'C'  GROUP BY EXTRACT(YEAR FROM DateEmb) HAVING COUNT(*)>1 ;

--

select extract(year from dateemb) "Annee" , count(*) from employes where (nomemp like 'L%' or nomemp like 'C%') group by extract(year from dateemb) having count(*) > 1 ;
```

```bash
Année       Nombre d\'employés embauché
---------- ----------------------------
  1997	      2
  1998	      2
```

### B. Tables postes ###

1.  Création de la table

```sql
create table Salaire (
  NumEmp  NUMBER(4),
  Poste  VARCHAR(25),
  Salaire NUMBER(4),
  NumServ VARCHAR(2),
  DateDeb DATE,
  DateFin DATE,
  CONSTRAINT nn_emp_Salaire CHECK (Salaire IS NOT NULL),
  CONSTRAINT nn_emp_poste CHECK (Poste IS NOT NULL),
  CONSTRAINT nn_emp_NumServ CHECK (SUBSTR(NumServ,1,1) = 'S' AND SUBSTR(NumServ,2,1) > 0),
  CONSTRAINT nn_emp_DateDeb CHECK (DateDeb IS NOT NULL)
);
```

2.  Ajout des valeurs dans la table

```sql
INSERT INTO SALAIRE VALUES (9701,'PRESIDENT',5800,'S2','17/09/97',NULL);
INSERT INTO SALAIRE VALUES (9703,'SECRETAIRE',950,'S1','17/09/97','31/12/98');
INSERT INTO SALAIRE VALUES (9703,'SECRETAIRE',1200,'S1','01/01/99',NULL);
INSERT INTO SALAIRE VALUES (9801,'DIRECTEUR',5300,'S1','07/07/97','31/12/98');
INSERT INTO SALAIRE VALUES (9801,'DIRECTEUR',3200,'S5','20/03/98',NULL);
INSERT INTO SALAIRE VALUES (9802,'DIRECTEUR',3500,'S2','16/10/98',NULL);
INSERT INTO SALAIRE VALUES (9803,'INGENIEUR',2600,'S4','22/11/98',NULL);
INSERT INTO SALAIRE VALUES (9901,'DIRECTEUR',3000,'S3','21/02/99',NULL);
```

*   Liste de tous les noms de postes

```sql
SELECT Poste FROM salaire;
```

```bash
POSTE
-------------------------
PRESIDENT
SECRETAIRE
SECRETAIRE
DIRECTEUR
DIRECTEUR
DIRECTEUR
INGENIEUR
DIRECTEUR
```

*   Postes occupés dont le salaire de l’employé est supérieur ou égal à 3000
SELECT poste FROM salaire WHERE Salaire >= 3000 AND DateFin IS NULL;


```bash
POSTE
-------------------------
PRESIDENT
DIRECTEUR
DIRECTEUR
DIRECTEUR
DIRECTEUR
```

*   Postes occupés, triés par ordre décroissant et salaires par ordre
croissant

```sql
SELECT Poste , Salaire FROM salaire WHERE DateFin IS NULL ORDER BY Poste DESC,Salaire ASC;
```

*   Resultat de la commande

```bash
POSTE			                SALAIRE
------------------------- ----------
SECRETAIRE                 1200
PRESIDENT                  5800
INGENIEUR                  2600
DIRECTEUR                  3000
DIRECTEUR                  3200
DIRECTEUR                  3500
```

*   Salaire le plus bas

```sql
SELECT MIN(Salaire) FROM salaire;
```

```bash
MIN(SALAIRE)
------------
 950
```

*   Moyenne des salaires

```sql
SELECT AVG(Salaire) FROM salaire;
```

```bash
AVG(SALAIRE)
------------
   3193,75
```

*   Moyenne des salaires pour les postes actuellement occupés

```sql
SELECT AVG(Salaire) FROM salaire WHERE DateFin IS NULL;
```

```bash
AVG(SALAIRE)
------------
3216,66667

```

*   Nombre de salariés avec un salaire > 3000

```sql
SELECT COUNT(* ) From salaire WHERE Salaire > 3000;
```



```bash
COUNT(* )
----------
4
```

*   Moyenne des salaires actuels pour chaque service

```sql
SELECT NumServ,AVG(Salaire)  FROM salaire WHERE DateFin IS NULL GROUP BY (NumServ);

--

select numserv, avg(salaire) from salaire group by numserv;

```



```bash
NU AVG(SALAIRE)
-- ------------
S4   2600
S3   3000
S5   3200
S2   4650
S1   1200
```

*   Moyenne des salaires pour chaque poste avec au moins 2 employés

```sql
SELECT Poste,AVG(Salaire)  FROM salaire GROUP BY (Poste) HAVING COUNT(Poste)>1 ;
```



```bash
POSTE                   AVG(SALAIRE)
----------------------- ------------
DIRECTEUR                    3750
SECRETAIRE                   1075
```

### C. Table etudiants ###

*   Création de la table etudiants

```sql
create table etudiants (
NUMERO  NUMBER(4)     NOT NULL,
NOM     VARCHAR2(25)  NOT NULL,
PRENOM  VARCHAR2(25)  NOT NULL,
SEXE    CHAR(1) CHECK(SEXE IN ('F', 'M')),
DATENAISSANCE   DATE    NOT NULL,
POIDS   NUMBER,
ANNEE   NUMBER,
CONSTRAINT PK_ETUDIANTS PRIMARY KEY (NUMERO)
);
```
Ajout des valeurs dans la table

```sql

INSERT INTO ETUDIANTS VALUES (71, 'Traifor', 'Benoit', 'M', '10/12/1978', 77, 1);
INSERT INTO ETUDIANTS VALUES (72, 'Génial', 'Clément', 'M', '10/04/1978', 72, 1);
INSERT INTO ETUDIANTS VALUES (73, 'Paris', 'Adam', 'M', '28/06/1974', 72, 2);
INSERT INTO ETUDIANTS VALUES (74, 'Parees', 'Clémence', 'F', '20/09/1977', 72, NULL);
INSERT INTO ETUDIANTS VALUES (69, 'Saitout', 'Inès', 'F', '22/11/1969', 69, 2);
INSERT INTO ETUDIANTS VALUES (55, 'Serafoub', 'Izouaf', 'M', '19/09/2013', 81, 1);
```

*   Moyenne des poids par sexe

```sql
SELECT SEXE,AVG(POIDS) FROM etudiants GROUP BY (SEXE);
```

```bash
S AVG(POIDS)
- ----------
M 75,5
F 70,5
```

*   Moyenne des poids par sexe et par tranche d’âge

```sql
SELECT EXTRACT(YEAR FROM DATENAISSANCE),SEXE,AVG(POIDS) FROM etudiants GROUP BY (SEXE,EXTRACT(YEAR FROM DATENAISSANCE));
```

```bash
Année de naissance  S AVG(POIDS)
------------------- - ----------
       1974       M   72
       1978       M   74,5
       1977       F   72
       1969       F   69
       2013       M   81
```

*   Moyenne des poids par année, par sexe et par tranche d’âge

```sql
SELECT ANNEE,SEXE,EXTRACT(YEAR FROM DATENAISSANCE),AVG(POIDS) FROM etudiants GROUP BY (ANNEE,SEXE,EXTRACT(YEAR FROM DATENAISSANCE));
```

```bash
ANNEE S EXTRACT(YEARFROMDATENAISSANCE) AVG(POIDS)
---------- - ------------------------ ----------
1 M            2013                     81
1 M            1978                     74,5
2 F            1969                     69
2 M            1974                     72
  F            1977                     72
```

*   Moyenne des poids par sexe, par année et par tranche d’âge

```sql
SELECT SEXE,ANNEE,EXTRACT(YEAR FROM DATENAISSANCE),AVG(POIDS) FROM etudiants GROUP BY (SEXE,ANNEE,EXTRACT(YEAR FROM DATENAISSANCE));
```

```bash
AS  ANNEE    EXTRACT(YEARFROMDATENAISSANCE) AVG(POIDS)
- ---------- ------------------------------ ----------
M    2             1974                        72
M    1             2013                        81
F                  1977                        72
M    1             1978                      74,5
F    2             1969                        69
```

<div class="page-break"></div>

## TP 5 : Jointures ##

### A. Gestion d'un café ###

Création des tables par l'intermédiaire d'un script sql déjà rédigé.

```sql
@creatcafe.sql
```
Insertion des valeurs.

```sql
@insertcafe.sql
```
##### 3. Formulation et tests d'une série de requêtes en SQL #####

1.  Liste du contenu de chaque table de la base.

```sql
SELECT * FROM LESTABLES;
select * from consommation;
select * from serveur;
select * from facture;
select * from comprend;
```

```bash
NUMTABLE  NOMTABLE	     NBPLACE
--------- -------------- ----------
 1        entree-gche    6
 2        entree-dte     10
 3        fenetre1       3
 4        fenetre2       8
 5        fenetre3       4
 6        fond-gche      4
 7        fond-dte       2

 NUMCONS    LIBCONS               PRIXCONS
----------- --------------------- ----------
    100     Cafe                       ,9
    101     Cafe double               1,3
    102     Cafe creme                1
    105     Chocolat                  1,5
    106     Biere pression            1,8
    107     Biere 25cl                2
    108     Biere 33cl                2,2
    110     Biere 50cl                2,5
    120     Jus de fruits             1,7
    121     Jus de fruits presse      2,6
    122     Perrier                   1,6
    124     Orangina                  1,4
    130     Coca Cola                 1,7

NUMSERVEUR NOMSERVEUR RUESERVEUR        CPSERVEUR VILLESERVEUR DATENSER
---------- ---------- ----------------  --------- ------------ --------
  50       Pizzi      3 rue des lilas     90000   BELFORT      12/01/76
  51       Cathy      25 av Roosevelt     90100   DELLE        05/04/78
  52       Totof      46 grande rue       90500   BAVILLIERS   30/09/84
  53       Pilou      5 impasse Martin    90000   BELFORT      17/08/86
  54       Alice      15 rue de la barre  95880   ENGHIEN      13/03/86

NUMFACTURE   NUMTABLE NUMSERVEUR DATEFACT
----------- --------- ---------- --------
      1200    1       53 01/02/10
      1201    5       53 01/02/10
      1202    3       52 01/02/10
      1203    5       50 01/02/10
      1204    4       52 02/02/10
      1205    1       53 02/02/10
      1206    3       52 02/02/10
      1207    5       53 02/02/10
      1208    7       54 02/02/10

NUMFACTURE    NUMCONS	     QTE
---------- ---------- ----------
      1200   101       3
      1200   106       1
      1200   120       1
      1201   105       2
      1201   106       2
      1202   100       2
      1202   122       1
      1203   102       1
      1203   108       1
      1203   121       1
      1203   130       1
      1204   122       4
      1204   124       2
      1205   100       2
      1206   108       3
      1207   108       1
      1207   110       2
      1208   108       2


```

2.  Nombre de places de la table N°4 (Nbplace).

```sql
SELECT NBPLACE FROM LESTABLES WHERE NUMTABLE = 4;
```

```bash
   NBPLACE
----------
	 8
```

3.  Liste des consommations dont le prix unitaire est supérieur à 1 euro
(Numcons, Libcons, Prixcons).

```sql
SELECT * FROM CONSOMMATION WHERE PRIXCONS >1;
```

```bash
NUMCONS LIBCONS             PRIXCONS
------- ------------------- ----------
    101 Cafe double         1,3
    105 Chocolat            1,5
    106 Biere pressio       1,8
    107 Biere 25cl          2
    108 Biere 33cl          2,2
    110 Biere 50cl          2,5
    120 Jus de fruit        1,7
    121 Jus de fruits press 2,6
    122 Perrier             1,6
    124 Orangina            1,4
    130 Coca Cola           1,7
```

4.  Liste des serveurs de Belfort et de Delle (Numserv, Nomserv, Villeserveur).

```sql
SELECT NUMSERVEUR, NOMSERVEUR, VILLESERVEUR FROM SERVEUR WHERE VILLESERVEUR = 'BELFORT' OR VILLESERVEUR = 'DELLE';
```

```bash
NUMSERVEUR NOMSERVEUR  VILLESERVEUR
---------- ----------- --------------
  50       Pizzi       BELFORT
  51       Cathy       DELLE
  53       Pilou       BELFORT
```

5.  Liste des factures du 2 février servies par le serveur 52 (Numfact,
Numtable).

```sql
SELECT NUMFACTURE,NUMTABLE FROM FACTURE WHERE DATEFACTURE = '02/02/10' AND NUMSERVEUR = 52;
```

```bash
NUMFACTURE   NUMTABLE
------------ ----------
      1204	    4
      1206	    3
```

6.  Liste des consommations de la facture 1203 (Numcons, Qte).

```sql
SELECT NUMCONS, QTE FROM COMPREND WHERE  NUMFACTURE = 1203 AND NUMFACTURE = 1203;
```

```bash
NUMCONS  QTE
------- -------
    102   1
    108   1
    121   1
    130   1
```

7.  Liste des consommations des factures 1200 et 1201 (sans lignes en double)
(Numcons).

```sql
SELECT NUMCONS FROM COMPREND WHERE NUMFACTURE = 1200 OR NUMFACTURE = 1201 GROUP BY (NUMCONS);
```

```bash
   NUMCONS
----------
       120
       101
       105
       106
```

8.  Liste des serveurs qui sont nés en 1976 (Nomserv, Datenserveur).

```sql
SELECT NOMSERVEUR,DATENSERVEUR FROM SERVEUR WHERE EXTRACT(YEAR FROM DATENSERVEUR) = 1976;
```

```bash
NOMSERVEUR DATENSER
---------- --------
Pizzi      12/01/76
```

9.  Liste des consommations de type bière (Numcons, Libcons, Prixcons).

```sql
SELECT * FROM CONSOMMATION WHERE SUBSTR(LIBCONS,1,5)='Biere';
-- ou
SELECT * FROM CONSOMMATION WHERE LIBCONS LIKE 'Biere%';
```

```bash
NUMCONS LIBCONS         PRIXCONS
------- --------------- --------
    106 Biere pression    1,8
    107 Biere 25cl        2
    108 Biere 33cl        2,2
    110 Biere 50cl        2,5
```

10. Liste des tables servies après le 1 février.

```sql
SELECT * FROM FACTURE WHERE DATEFACTURE > '01/02/2010';
```

```bash
NUMFACTURE NUMTABLE NUMSERVEUR DATEFACT
---------- -------- ---------- --------
      1204    4      52        02/02/10
      1205    1      53        02/02/10
      1206    3      52        02/02/10
      1207    5      53        02/02/10
      1208    7      54        02/02/10
```

11. Liste des serveurs dont le nom contient i en deuxième position (Nomserv).

```sql
SELECT NOMSERVEUR FROM SERVEUR WHERE SUBSTR(NOMSERVEUR,2,1) = 'i';
-- ou
SELECT NOMSERVEUR FROM SERVEUR WHERE NOMSERVEUR LIKE '_i%';
```

```bash
NOMSERVEUR
--------------------
Pizzi
Pilou
```

12. Liste des serveurs dont le nom commence par un P (Nomserv).

```sql
SELECT NOMSERVEUR FROM SERVEUR WHERE SUBSTR(NOMSERVEUR,1,1) = 'P';
-- ou
SELECT NOMSERVEUR FROM SERVEUR WHERE NOMSERVEUR LIKE 'P%';
```

```bash
NOMSERVEUR
--------------------
Pizzi
Pilou
```

13. Liste des serveurs par ville (Nomserv, Villeserveur).

```sql
SELECT NOMSERVEUR,VILLESERVEUR FROM SERVEUR ORDER BY VILLESERVEUR ASC;
```

```bash
NOMSERVEUR  VILLESERVEUR
----------- --------------
Totof        BAVILLIERS
Pilou        BELFORT
Pizzi        BELFORT
Cathy        DELLE
Alice        ENGHIEN
```

14. Affichage d'une liste de consommation classé par ordre alphabétique

```sql
SQL> select libcons, numcons, prixcons from consommation order by libcons asc;
```

```bash
LIBCONS         NUMCONS   PRIXCONS
-------------- ---------  ---------
Biere pression        106    1,8
Biere 25cl            107    2
Biere 33cl            108    2,2
Biere 50cl            110    2,5
Cafe                  100     ,9
Cafe creme            102    1
Cafe double           101    1,3
Chocolat              105    1,5
Coca Cola             130    1,7
Jus de fruits         120    1,7
Jus de fruits presse  121    2,6
Orangina              124    1,4
Perrier               122    1,6
```

15. Liste des villes où habitent des serveurs (sans lignes en double)
(Villeserveur).

```sql
SELECT VILLESERVEUR FROM SERVEUR GROUP BY (VILLESERVEUR);
```

```bash
VILLESERVEUR
--------------------
DELLE
BAVILLIERS
ENGHIEN
BELFORT
```

16. Le nombre de tables du restaurant.

```sql
SELECT COUNT(*) FROM LESTABLES;
```

```bash
COUNT(*)
----------
 7
```

17. Le nombre de places disponibles sur l'ensemble des tables.

```sql
SELECT SUM(NBPLACE) FROM LESTABLES;
```

```bash
SUM(NBPLACE)
------------
	  37
```

18. Nombre de factures établies par chaque serveur (Numserv, Nbfacture).

```sql
SELECT NUMSERVEUR, COUNT(*) FROM FACTURE GROUP BY (NUMSERVEUR);
-- ou
select numserveur, count(numserveur) as nb_factures from serveur natural join facture group by numserveur;
```

```bash
NUMSERVEUR   COUNT(*)
---------- ----------
  54         1
  53         4
  52         3
  50         1
```

19. Nombre de factures établies chaque jour (Datefacture, Nbfacture).

```sql
SELECT DATEFACTURE, COUNT(*) FROM FACTURE GROUP BY(DATEFACTURE) ORDER BY DATEFACTURE ASC;
```

```bash
DATEFACT   COUNT(*)
-------- ----------
01/02/10    4
02/02/10    5
```

20. Liste des serveurs qui ont établi plus de 3 factures (Numserv, Nbfacture).

```sql
SELECT NUMSERVEUR, COUNT(*) FROM FACTURE GROUP BY(NUMSERVEUR) HAVING COUNT(*)>3;
```

```bash
NUMSERVEUR   COUNT(*)
---------- ----------
  53        4
```
21. Prix moyen des consommation

```sql
select avg(prixcons) from consommation;
```

```bash
AVG(PRIXCONS)
-------------
   1,70769231
```

22. Prix moyen du café

```sql
select avg(prixcons) from consommation where substr(libcons,1,4) = 'Cafe';
--ou
select avg(prixcons) from consommation where libcons like 'Cafe%';
```

```bash
AVG(PRIXCONS)
-------------
   1,06666667
```

23. Quantité moyenne consommée pour chaque consommation.

```sql
select numcons, avg(qte) from comprend group by (numcons);
```

```bash
NUMCONS   AVG(QTE)
-------- ----------
    100       2
    120       1
    121       1
    108    1,75
    102       1
    110       2
    101       3
    130       1
    105       2
    124       2
    106     1,5
    122     2,5
```

24. Nombre de serveurs par villes

```sql
select villeserveur, count(numserveur) from serveur group by (villeserveur);
```

```bash
-------------- ------
DELLE          1
BAVILLIERS     1
ENGHIEN        1
BELFORT        2
```

25. Liste des villes dans lesquelles habitent plus d'un serveur.

```sql
SELECT villeserveur, count(*) as nb_serveur FROM serveur GROUP BY(villeserveur) HAVING COUNT(*)>1;
```

```bash
VILLESERVEUR      NB_SERVEUR
---------------- ----------
BELFORT           2
```

26. Nombre de type de consommation par factures.

```sql
select numfacture, count(numcons) from comprend group by (numfacture);
```

```bash
NUMFACTURE COUNT(NUMCONS)
---------- --------------
      1200   3
      1201   2
      1202   2
      1203   4
      1204   2
      1205   1
      1206   1
      1207   2
      1208   1
```

27. Nombre total de consommation en comptant la Quantité par facture

```sql
select numfacture, count(qte) from comprend group by (numfacture);
```

```bash
NUMFACTURE COUNT(QTE)
---------- ----------
      1207  2
      1208  1
      1200  3
      1205  1
      1201  2
      1204  2
      1203  4
      1202  2
      1206  1
```
28. Nombre de facture par consommation.

```sql
select numcons, count(numfacture) from comprend group by (numcons);
```

```bash
NUMCONS COUNT(NUMFACTURE)
------- -----------------
    100     2
    120     1
    121     1
    108     4
    102     1
    110     1
    101     1
    130     1
    105     1
    124     1
    106     2
    122     2
```

29. Consommation qui interviennent dans plus de 2 factures.

```sql
select numcons, count(numfacture) from comprend group by (numcons) having count(numfacture) > 2;
```

```bash
NUMCONS COUNT(NUMFACTURE)
------- -----------------
    108   4
```
30. Liste des serveur triés par nom de ville croissant, puis nom de serveur croissant

```sql
select * from serveur order by villeserveur, nomserveur;
```

```bash
NUMSERVEUR NOMSERVEUR  RUESERVEUR	        CPSERVEUR VILLESERVEUR  DATENSER
---------- ----------  -----------------  --------- ------------- --------
  52       Totof       46 grande rue      90500     BAVILLIERS    30/09/84
  53       Pilou       5 impasse Martin   90000     BELFORT       17/08/86
  50       Pizzi       3 rue des lilas    90000     BELFORT       12/01/76
  51       Cathy       25 av Roosevelt    90100     DELLE         05/04/78
  54       Alice       15 rue de la barre 95880     ENGHIEN       13/03/86
```

31. Liste des serveurs, triés par nom de ville décroissant, puis nom de serveur croissant.
```sql

select * from serveur order by villeserveur desc, nomserveur asc;
```

```bash
NUMSERVEUR NOMSERVEUR RUESERVEUR CPSERVEUR VILLESERVEUR DATENSER
---------- --------------------- ------ -------------- --------
	54 Alice  15 rue de la barre    95880 ENGHIEN        13/03/86
	51 Cathy  25 av Roosevelt       90100 DELLE          05/04/78
	53 Pilou  5 impasse Martin      90000 BELFORT        17/08/86
	50 Pizzi  3 rue des lilas       90000 BELFORT        12/01/76
	52 Totof  46 grande rue         90500 BAVILLIERS     30/09/84
```


32. Liste des factures avec leur numéro de table et le nom du serveur

```sql
select numfacture, numtable, nomserveur from facture natural join serveur;
```

```bash
NUMFACTURE NUMTABLE NOMSERVEUR
---------- -------- -----------
      1200   1        Pilou
      1201   5        Pilou
      1202   3        Totof
      1203   5        Pizzi
      1204   4        Totof
      1205   1        Pilou
      1206   3        Totof
      1207   5        Pilou
      1208   7        Alice
```

33. Liste des factures de la table 5 avec le nom du serveur.

```sql
select numfacture, numtable, nomserveur from facture natural join serveur where numtable = 5;
```

```bash
NUMFACTURE  NUMTABLE  NOMSERVEUR
----------  --------- --------------------
      1201    5       Pilou
      1203    5       Pizzi
      1207    5       Pilou
```

34. Liste des factures avec leur nom de table et le nom du serveur

```sql
select numfacture, nomtable, nomserveur from facture natural join lestables natural join serveur;
```

```bash

NUMFACTURE NOMTABLE   NOMSERVEUR
---------- ---------- --------------------
     1206 fenetre1       Totof
     1201 fenetre3       Pilou
     1207 fenetre3       Pilou
     1208 fond-dte       Alice
     1200 entree-gche    Pilou
     1205 entree-gche    Pilou
     1204 fenetre2       Totof
     1203 fenetre3       Pizzi
     1202 fenetre1       Totof
```

35. Listes des serveurs et des tables qu'ils ont servies ordonnés selon le nom du

```sql
select distinct nomserveur, nomtable from facture natural join serveur natural join lestables ORDER BY nomserveur;
```

```bash
NOMSERVEUR NOMTABLE
---------- ---------------
Alice      fond-dte
Pilou      entree-gche
Pilou      fenetre3
Pizzi      fenetre3
Totof      fenetre1
Totof      fenetre2
```

36. Liste des consommation de la facture 1203 avec leur nom, leur prix et leur Quantité

```sql
select numcons, libcons, prixcons, qte from comprend natural join consommation where numfacture = 1203;
```

```bash
NUMCONS LIBCONS                PRIXCONS   QTE
------- ---------------------  ---------- ----------
    102 Cafe creme               1      1
    108 Biere 33cl             2,2      1
    121 Jus de fruits presse    ,6      1
    130 Coca Cola              1,7      1
```

37. Liste des consommations du premier février de la table 5 avec leur nom, leur prix, et leur Quantité

```sql
select libcons, qte from facture natural join comprend natural join consommation where datefacture = '01/02/10' and numtable = 5;
```

```bash
LIBCONS               QTE
--------------------- ----
Cafe creme            1
Chocolat              2
Biere pression				2
Biere 33cl            1
Jus de fruits presse  1
Coca Cola             1

```

38. Liste des tables et des numéros de factures qui leur sont associées.

```sql
select nomtable, numfacture from lestables natural left join facture;
```

```bash
NOMTABLE	NUMFACTURE
--------- ----------
entree-gche   1200
entree-gche   1205
entree-dte
fenetre1      1206
fenetre1      1202
fenetre2      1204
fenetre3      1203
fenetre3      1201
fenetre3      1207
fond-gch
fond-dte      1208
```

39. Pareil

```SQL
select nomtable, numfacture from facture natural right join lestables;
```

```bash
NOMTABLE  NUMFACTURE
--------- ----------
entree-gche      1200
entree-gche      1205
entree-dte
fenetre1         1206
fenetre1         1202
fenetre2         1204
fenetre3         1203
fenetre3         1201
fenetre3         1207
fond-gche
fond-dte         1208
```

40. Liste des tables qui n'ont aucune factures

```sql
select numtable, nomtable from lestables natural left join facture where numfacture is null;
```

```bash
NUMTABLE    NOMTABLE
---------- ---------------
 2          entree-dte
 6          fond-gche
```

41. Liste des consommation qui ont déjà été servies par le serveur 52

```SQL
select numcons, libcons from comprend natural join consommation natural left join facture where numserveur = 52;
```

```bash
NUMCONS LIBCONS
------- ------------------------------
    100 Cafe
    108 Biere 33cl
    122 Perrier
    122 Perrier
    124 Orangina
```

42. Liste des consommation qui n'ont jamais été servies

```sql
select numcons, libcons from comprend natural right join consommation where qte is null;
```

```bash
NUMCONS LIBCONS
------- ------------------------------
    107 Biere 25cl
```

43. La liste des facture avec leur date et leur nombre de consommation.

```sql
select numfacture, datefacture, sum(qte) from comprend natural join facture group by numfacture, datefacture;
```

```bash
NUMFACTURE DATEFACT   SUM(QTE)
---------- -------- ----------
      1202 01/02/10      3
      1204 02/02/10      6
      1203 01/02/10      4
      1205 02/02/10      2
      1200 01/02/10      5
      1207 02/02/10      3
      1201 01/02/10      4
      1206 02/02/10      3
      1208 02/02/10      2
```

44. La liste des factures et le montant de leur addition

```SQL
select numfacture, sum(qte*prixcons) from comprend natural join consommation group by numfacture;
```

```bash
NUMFACTURE SUM(QTE*PRIXCONS)
---------- -----------------
      1207   7,2
      1208   4,4
      1200   7,4
      1205   1,8
      1201   6,6
      1204   9,2
      1203   7,5
      1202   3,4
      1206   6,6
```

45. Nombre de consommation servies par jour

```sql
select datefacture, sum(qte) from facture natural join comprend group by datefacture;
```

```bash
DATEFACT   SUM(QTE)
---------- ---------
01/02/10   16
02/02/10   16
```

46. Montant global du chiffre d'affaire par jour

```sql
select datefacture, sum(qte*prixcons) from facture natural join comprend natural join consommation group by datefacture;
```

```bash
DATEFACT SUM(QTE*PRIXCONS)
-------- -----------------
01/02/10      24,9
02/02/10      29,2
```

47. La liste des serveurs par nom et leur nombre de factures.

```sql
select nomserveur, count(numfacture) from serveur natural left join facture group by nomserveur;
```

```bash
NOMSERVEUR  COUNT(NUMFACTURE)
----------- -----------------
Totof       3
Pizzi       1
Alice       1
Cathy       0
Pilou       4

```
48. La liste des serveurs par nom et le nombre de consommation qu'ils ont servies.

```SQL
select nomserveur, count(qte) from serveur natural join facture natural join comprend group by nomserveur;
```

```bash
NOMSERVEUR    SUM(QTE)
------------ ----------
Totof        12
Alice         2
Pizzi         4
Pilou        14
```
49. LA liste des serveurs par nom et leur chiffre d'affaire

```sql
select nomserveur, sum(qte*prixcons) as ca from comprend natural join consommation natural join facture natural join serveur group by (nomserveur);
```

```bash
NOMSERVEUR     CA
------------- ----------
Totof         19,2
Pizzi          7,5
Alice          4,4
Pilou           23
```

50. Nom des table qui ont au moins deux factures

```sql
select numtable, count(numfacture) from facture natural join lestables group by numtable having count(numfacture) >= 2;
```

```bash
NUMTABLE COUNT(NUMFACTURE)
-------- -----------------
 1         2
 5         3
 3         2
```

51. La liste complète des consommations et le nombre de factures dans lesquelles elles apparaissent

```sql
select libcons, count(*) as nb_facture from consommation natural join comprend group by libcons;
```

```bash
LIBCONS             NB_FACTURE
------------------ ----------
Coca Cola             1
Biere 50cl            1
Biere pression        2
Biere 33cl            4
Jus de fruits presse  1
Cafe                  2
Cafe double           1
Jus de fruits         1
Perrier               2
Cafe creme            1
Chocolat              1

LIBCONS               NB_FACTURE
-------------------- ----------
Orangina              1
```

52. La liste complète des tables et leur chiffre d'affaire

```sql
SELECT NOMTABLE, SUM(QTE*PRIXCONS) AS ca
FROM COMPREND
JOIN FACTURE ON FACTURE.NUMFACTURE = COMPREND.NUMFACTURE
JOIN CONSOMMATION ON CONSOMMATION.NUMCONS = COMPREND.NUMCONS
RIGHT JOIN LESTABLES ON FACTURE.NUMTABLE = LESTABLES.NUMTABLE
GROUP BY NOMTABLE;
```

```bash
NOMTABLE      CA
------------- ----------
fenetre1      10
entree-dte
fenetre3      21,3
fond-dte      4,4
fond-gche
entree-gche   9,2
fenetre2      9,2
```

### B. Généalogie royale ###

#### 3. Formulation et test de requêtes ####

Création de la table et insertion des valeurs

```sql
@genealogie.sql
```
1.  La liste des enfants d'Elizabeth II

```sql
SELECT g1.NOM, g1.DATENAISSANCE FROM genealogie
g1 JOIN genealogie g2 ON g1.MERE = g2.NUMPER WHERE g2.NOM = 'Elizabeth II' ;
```

```bash
NOM               DATENAIS
----------------- --------
Prince Charles    14/11/48
Princesse Anne    15/08/50
Prince Andrew     19/02/60
Prince Edward     10/03/64
```

2.  La mère du prince William

```SQL
SELECT g1.NOM,g1.DATENAISSANCE FROM genealogie
g1 JOIN genealogie g2 ON g1.NUMPER = g2.MERE where g2.NOM = 'Prince Charles' ;
```

```bash
NOM               DATENAIS
----------------- --------
Elizabeth II      21/04/26
```

3.  Les parents d'Elizabeth II

```sql
SELECT g1.NOM,g1.DATENAISSANCE FROM genealogie
g1 JOIN genealogie g2 ON g1.NUMPER = g2.MERE OR g1.NUMPER = g2.PERE where g2.NOM = 'Elizabeth II' ;
```

```bash
NOM                   DATENAIS
--------------------- --------
George VI             14/12/95
Elizabeth Bowes-Lyon  04/08/00
```

4.  Les frères et sœurs du Prince Charles (Nom, DateNaissance).

```sql
SELECT g1.NOM,g1.DATENAISSANCE FROM genealogie
g1 JOIN genealogie g2 ON g1.MERE = g2.MERE AND g1.PERE = g2.PERE where g2.NOM = 'Prince Charles' AND g1.NUMPER != g2.NUMPER;
```

```bash
NOM                   DATENAIS
--------------------- --------
Princesse Anne        15/08/50
Prince Andrew         19/02/60
Prince Edward         10/03/64
```

5.  Le nom des individus, le nom de leur père (ou NULL) et le nom de leur mère
(ou NULL) (Nom, Nompere, Nommere).

```sql
SELECT g1.NOM, g2.NOM "Nom Père",g3.NOM "Nom Mère"  FROM genealogie
g1 LEFT JOIN genealogie g2 ON g1.PERE = g2.NUMPER
LEFT JOIN genealogie g3 ON g1.MERE = g3.NUMPER;
```

```bash
NOM                     Nom Père                 Nom Mère
----------------------- ----------------------- -----------------------------------
Elizabeth II            George VI               Elizabeth Bowes-Lyon
Margaret du Royaume-Uni George VI               Elizabeth Bowes-Lyon
Prince Charles          Philip Mountbatten      Elizabeth II
Princesse Anne          Philip Mountbatten      Elizabeth II
Prince Andrew           Philip Mountbatten      Elizabeth II
Prince Edward           Philip Mountbatten      Elizabeth II
Prince William          Prince Charles          Diana Spencer
Prince Henry            Prince Charles          Diana Spencer
George VI
Elizabeth Bowes-Lyon
Philip Mountbatten
Diana Spencer
```

6.  La liste des individus et le nombre de leurs enfants étant dans la base de
données (Nom, NbEnfants).

```sql
SELECT g1.NOM, COUNT(g2.NUMPER) FROM genealogie
g1 LEFT JOIN genealogie g2 ON g2.MERE = g1.NUMPER OR g2.PERE = g1.NUMPER
GROUP BY g1.NOM;
```

```bash
NOM                       COUNT(G2.NUMPER)
------------------------ ----------------
Philip Mountbatten        4
George VI                 2
Elizabeth II              4
Margaret du Royaume-Uni   0
Prince Andrew             0
Elizabeth Bowes-Lyon      2
Prince Charles            2
Princesse Anne            0
Diana Spencer             2
Prince Henry              0
Prince Edward             0
Prince William            0
```

<div class="page-break"></div>

## TP 6 : SQL requetes avances ##

### **B** Formulez et testez les requêtes suivantes en SQL en proposant au moins deux solutions différentes pour chaque question **si possible :** ###

1.  Donner la liste des noms, des prénoms et des dates de naissance de tous
les élèves.

```sql
select nom, prenom, date_naissance from eleves;
--ou
select concat(nom,concat(' ',concat(prenom,concat(' ', date_naissance)))) "Nom Prenom date de naissance" from eleves;
```

```bash
NOM        PRENOM      DATE_NAISS
---------- ----------- ----------
Brisefer   Benoit      10-12-1978
Génial     Olivier     10-04-1978
Jourdan    Gil         28-06-1974
Spring     Jerry       16-02-1974
Tsuno      Yoko        29-10-1977
Lebut      Marc        29-04-1974
Lagaffe    Gaston      08-04-1975
Dubois     Robin       20-04-1976
Walthéry   Natacha     07-09-1977
Danny      Buck        15-02-1973

```

2.  Donner tous les renseingements sur toutes les activités

```sql
select * from activites;
-- ou
select niveau, nom, equipe from activites;
```

```bash
NIVEAU     NOM                  EQUIPE
---------- -------------------- ---------------------
1          Mini foot            Amc Indus
1          Surf                 Les planchistes ...
2          Tennis               Ace Club
3          Tennis               Ace Club
1          Volley ball          Avs80
2          Mini foot            Les as du ballon
2          Volley ball          smash
```
3.  Lister les spécialités des professeurs

```sql
select nom, specialite from professeurs;
--ou
select nom, specialite from professeurs order by nom desc;
```

```bash
NOM           SPECIALITE
------------- --------------------
Bottle        poésie
Bolenov       réseau
Tonilaclasse  poo
Pastecnov     sql
Selector      sql
Vilplusplus   poo
Francesca
Pucette       sql
```

4.  Obtenir le nom et prénom des élèves pesant moins de 45 kilos et inscrits
en 1 ère année ou des élèves inscrits en 2 ème année.

```sql
select nom, prenom from eleves where poids < 45 AND annee = 1 OR annee = 2;
--ou
select nom, prenom from eleves where (poids between 0 AND 45 and annee = 1 ) OR annee = 2;
```

```bash
NOM                       PRENOM
------------------------- -------------------------
Brisefer                  Benoit
Génial                    Olivier
Jourdan                   Gil
Spring                    Jerry
Lebut                     Marc
Dubois                    Robin
Danny                     Buck
```   

5.  Obtenir le nom des élèves dont le poids est compris entre 60 et 80 kilos.

```sql
select nom, poids from eleves where poids <= 80 and poids >= 60;
--ou
select nom, poids from eleves where poids between 60 and 80;
```

```bash
NOM                       POIDS
------------------------- ----------
Jourdan                   72
Spring                    78
Lebut                     75
Lagaffe                   61
Dubois                    60
```

6.  Obtenir le nom des professeurs dont la spécialité est ‘poésie’ ou SQL.

```sql
select nom from professeurs where specialite in ('poésie','sql');
--ou
select nom from professeurs where specialite = 'poésie' or specialite = 'sql';

```

```bash
NOM
-------------------------
Bottle
Pastecnov
Selector
Pucette
```

7.  Obtenir le nom des élèves dont le nom commence par ‘L’.

```sql
select nom from eleves where nom like 'L%';
--ou
select nom from eleves where substr(nom, 1, 1) = 'L';
```

```bash
NOM
-------------------------
Lebut
Lagaffe
```

8.  Obtenir le nom des professeurs dont la spécialité est inconnue.

```sql
select nom from professeurs where specialite is NULL;
--ou
select nom from professeurs having count(specialite) < 1 group by nom;
```

```bash
NOM
-------------------------
Francesca
```

9.  Obtenir le nom et prénom des élèves pesant moins de 45 kilos et inscrits
en 1 ère année.

```sql
select nom, prenom from eleves where poids < 45 AND annee = 1;
-- on suppose les valeurs de comparaison non inclusive.
select nom, prenom from eleves where (poids between 0 and 44) AND annee = 1;
```

```bash
NOM                       PRENOM
------------------------- -------------------------
Brisefer                  Benoit
Génial                    Olivier
```

10. Obtenir, pour chaque professeur, son nom et sa spécialité. Si cette der-
nière est inconnue, on souhaite afficher la chaîne de caractères : ‘****’.

```sql
select nom, coalesce(specialite, '****') AS specialite from professeurs;
--ou
SELECT nom, NVL(specialite, '****') AS specialite FROM professeurs;

```

```bash
NOM            SPECIALITE
-------------- --------------------
Bottle         poésie
Bolenov        réseau
Tonilaclasse   poo
Pastecnov      sql
Selector       sql
Vilplusplus    poo
Francesca      ****
Pucette        sql
```

11. Quels sont les noms et prénoms des élèves qui pratiquent du surf au ni-
veau 1. Rédigez cette requête de cinq façons différentes.

```sql
select t1.nom, t1.prenom from eleves t1 join ACTIVITES_PRATIQUEES t2 on t2.num_eleve = t1.num_eleve where t2.nom = 'Surf' and t2.niveau = 1;
--ou
select nom, prenom from eleves where num_eleve in (select num_eleve from activites_pratiquees where nom = 'Surf' and niveau = 1);
--ou
select t1.nom, t1.prenom from eleves t1 where exists (select * from activites_pratiquees t2 where t1.num_eleve = t2.num_eleve and t2.nom = 'Surf' and t2.niveau = 1);
--ou
SELECT NOM,PRENOM FROM ELEVES WHERE NUM_ELEVE IN (SELECT NUM_ELEVE
FROM ACTIVITES_PRATIQUEES WHERE NOM != ALL(SELECT DISTINCT NOM FROM
ACTIVITES_PRATIQUEES WHERE NOM != 'Surf') AND NIVEAU = 1);
--ou
SELECT NOM, PRENOM FROM ELEVES WHERE NUM_ELEVE IN
(SELECT NUM_ELEVE FROM ACTIVITES_PRATIQUEES WHERE NOM = 'Surf'
INTERSECT SELECT NUM_ELEVE FROM ACTIVITES_PRATIQUEES WHERE NIVEAU = 1);
```

```bash
NOM        PRENOM
---------- -------------------------
Brisefer   Benoit
Spring     Jerry
Tsuno      Yoko
```

12. Obtenir le nom des élèves de l’équipe AMC INDUS.

```sql
select t1.nom from eleves t1 join ACTIVITES_PRATIQUEES t2 on t1.num_eleve = t2.num_eleve join activites t3 on t3.niveau = t2.niveau and t3.nom = t2.nom where equipe = 'Amc Indus';
--ou
select nom from eleves where num_eleve in (select num_eleve from activites_pratiquees where concat(niveau,nom) in (select concat(niveau, nom) from activites where equipe = 'Amc Indus'));
```

```bash
NOM
-------------------------
Brisefer
Génial
Tsuno
Dubois
Walthéry
Danny
```

13. Obtenir les pairs de noms de professeurs qui ont la même spécialité.

```sql
select t1.nom,t2.nom,t1.specialite from professeurs t1, professeurs t2 where t1.specialite = t2.specialite and t1.nom > t2.nom;
--ou
select t1.nom,t2.nom,t1.specialite from professeurs t1 join professeurs t2 on t1.specialite = t2.specialite where t1.nom > t2.nom;
```

```bash
NOM         NOM          SPECIALITE
----------  ------------ --------------------
Vilplusplus Tonilaclasse poo
Pucette     Pastecnov    sql
Selector    Pastecnov    sql
Selector    Pucette      sql
```

14. Pour chaque spécialité sql/SQL, on demande d’obtenir son nom son salaire mensuel actuel et son augmentation mensuelle depuis son salaire
de base.

```sql
select nom, specialite, ((Salaire_actuel - Salaire_base) / MONTHS_BETWEEN(Der_prom, Date_entree) ) "augmentation mensuelle" from professeurs where specialite = 'sql';
```

```bash
NOM        SPECIALITE     augmentation mensuelle
---------  -------------  --------------------
Pastecnov   sql
Selector    sql           0
Pucette     sql           5764,22462
```

15. Obtenir le nom des professeurs dont l’augmentation relative au salaire de
base dépasse 25%.

```sql
select nom from professeurs where (Salaire_actuel - Salaire_base) > (Salaire_base * 25 / 100);
```

```bash
NOM
-------------------------
Bottle
Bolenov
Francesca
```

16. Afficher les points de Tsuno obtenus dans chaque cours sur 100 plutôt
que sur 20.

```sql
select (points * 5) "Points sur 100" from resultats where Num_eleve in(select Num_eleve from eleves where nom = 'Tsuno');
--ou
select (points * 5) "Points sur 100" from resultats where Num_eleve =(select Num_eleve from eleves where nom = 'Tsuno');
--Attention le = ne marche que lorsqu'on obtient que 1 num_select dans le 2eme select

```

```bash
Points sur 100
--------------
25
32,5
65
65
```

17. Obtenir le poids moyen des élèves de 1 ère année.

```sql
select avg(poids) "poids moyen" from eleves where annee = 1;
--ou
select sum(poids)/count(*) "poids moyen" from eleves where annee = 1;
```

```bash
poids moyen
-----------
       48,4
```

18. Obtenir le total des points de l’élève numéro 3.

```sql
select sum(points) from RESULTATS where Num_eleve = 3;
```

```bash
SUM(POINTS)
-----------
   65
```

19. Obtenir la plus petite et la plus grande cote de l’élève Brisefer.

```sql
select min(points),max(points) from RESULTATS where Num_eleve in (select Num_eleve from eleves where nom = 'Brisefer');
--ou
select min(points),max(points) from RESULTATS natural join eleves where nom = 'Brisefer';
```

```bash
MIN(POINTS) MAX(POINTS)
----------- -----------
    8       20
```

20. Obtenir le nombre d’élèves inscrits en deuxième année.

```sql
select count(*) from eleves where annee = 2;
```

```bash
COUNT(*)
----------
 5
```

21. Quel est l'augmentation mensuelle moyenne des salaires des professeurs de SQL?

```sql
select avg((Salaire_actuel - Salaire_base)/months_between(der_prom, date_entree)) "Augmentation mensuelle moyenne" from professeurs where specialite = 'sql';
```

```bash
Augmentation mensuelle moyenne
------------------------------
2882,11231
```

22. Obtenir l'année de la dernière promotion du professeur pucette.

```sql
select EXTRACT(YEAR from der_prom) "Année" from professeurs where nom like 'Pucette';
--ou
select substr(der_prom,7,4) "Année" from professeurs where nom like 'Pucette';
```

```bash
Année
----------
1996
```

23. Pour chaque professuer, afficher sa date d'embauche, sa date de dernière promotion ansi que le nombre d'années écoulé entre ces deux dates.

```sql
select nom, date_entree, der_prom, months_between(der_prom, date_entree)/12 AS DateDiff from professeurs;
```

```bash
NOM                 DATE_ENT DER_PROM DATEDIFF
------------------- -------- -------- ----------
Bottle              01/10/70 01/10/88 18
Bolenov             15/11/68 01/10/98 29,8790323
Tonilaclasse        01/10/79 01/01/89 9,25
Pastecnov           01/10/75
Selector            15/10/82 01/10/88 5,96236559
Vilplusplus         25/04/90 05/06/94 4,11290323
Francesca           01/10/75 11/01/98 22,2768817
Pucette             06/12/88 29/02/96 7,22849462

```

24. Afficher l'age moyen des élèves. Cet âge sera exprimée en année.

```SQL
select avg(months_between(sysdate, date_naissance)/12) "age moyen" from eleves;
--ou
select sum(months_between(sysdate, date_naissance)/12)/count(*) "age moyen" from eleves;
```

```bash
age moyen
----------
43,8373711
```

25. Affiche le nom des professeurs pour lesquels il s'est écoulé plus de 50 mois entre l'embauche et la première promotion

```sql
select nom from professeurs where months_between(der_prom, date_entree) > 50;
```

```bash
NOM
-------------------------
Bottle
Bolenov
Tonilaclasse
Selector
Francesca
Pucette

6 lignes s�lectionn�es.

```
26. Obtenir la liste des élèves qui auront au moins 24 dans moins de 4 mois.

```sql
select nom from eleves where months_between(sysdate, date_naissance)+4/12 > 24;
```

```bash
NOM
-------------------------
Brisefer
Génial
Jourdan
Spring
Tsuno
Lebut
Lagaffe
Dubois
Walthéry
Danny

10 lignes s�lectionn�es.
```

27. Obtenir une liste des élèves classé par année et par ordre alphabétique

```sql
select nom, annee from eleves order by annee, nom ;
```

```bash
NOM             ANNEE
--------------- ----------
Brisefer        1
Génial          1
Lagaffe         1
Tsuno           1
Walthéry        1
Danny           2
Dubois          2
Jourdan         2
Lebut           2
Spring          2

10 lignes s�lectionn�es.
```

28. Afficher en ordre decroissant les points de Tsuno obtenus dans chaque cours sur 100 plutot que sur 20.

```sql
select num_cours, points*5 from resultats where num_eleve = (select num_eleve from eleves where nom like('Tsuno')) order by 2 desc;
--ou
select num_cours, points*5 from resultats natural join eleves where nom = 'Tsuno' order by 2 desc;
```

```bash
NUM_COURS  POINTS*5
---------- ----------
4          65
5          65
2          32,5
1          25
```

29. Obtenir pour chaque élève de 1 ère année son nom et sa moyenne.

```sql
select nom, avg(points) from resultats natural join eleves group by nom;
```

```bash
NOM        AVG(POINTS)
---------- -----------
Jourdan    16,25
Spring     12,375
Dubois     8,375
Walthéry   15,875
Danny      7,875
Tsuno      9,375
Génial     9,5
Lebut      9,875
Brisefer   13,375
Lagaffe    10,375
```

30. Obtenir la moyenne des points de chaque élève de 1 ère année dont le to-
tal des points est supérieur à 40.

```sql
select num_eleve, avg(points) from resultats having sum(points) > 40 group by num_eleve;
```

```bash
NUM_ELEVE  AVG(POINTS)
---------- -----------
  1        13,375
  4        12,375
  3        16,25
  7        10,375
  9        15,875
```

31. Obtenir le maximum parmi les totaux de chaque élève.

```sql
select max(sum(points)) "Max des totaux" from resultats group by num_eleve;
```

```bash
Max des totaux
--------------
     65
```

32. Obtenir le nom des élèves qui jouent dans l'équipe AMC INDUS.

Voir question 12

33. Quels sont les élèves de 1 ère année dont la moyenne est supérieure à la
moyenne de la 1 ère année ?

```sql
select num_eleve, avg(points) from resultats natural join eleves where annee = 1 having avg(points) > (select avg(points) from resultats natural join eleves where annee = 1) group by num_eleve;
--ou
select num_eleve, avg(points) from resultats where num_eleve in (select num_eleve from eleves where annee = 1) having avg(points) > (select avg(points) from resultats where num_eleve in (select num_eleve from eleves where annee = 1)) group by num_eleve;
```

```bash
NUM_ELEVE  AVG(POINTS)
---------- -----------
  1        13,375
  9        15,875
```

34. Obtenir le nom et le poids des élèves de 1 ère année plus lourds que
n’importe quel élève de 2 ème année.

```sql
select nom, poids from eleves where annee = 1 and poids > all(select poids from eleves where annee = 2);
--ou
select nom,poids from eleves where annee = 1 and poids > (select MAX(poids) from eleves where annee = 2);
```

```bash
aucune ligne s�lectionn�e
```

35. Obtenir le nom et le poids des élèves de 1 ère année plus lourds qu’un
élève quelconque de 2 ème année.

```sql
select nom, poids from eleves where annee = 1 and poids > any(select poids from eleves where annee = 2);
--ou
select nom,poids from eleves where annee = 1 and poids > (select MIN(poids) from eleves where annee = 2);
```

```bash
NOM                        POIDS
-------------------------- ----------
Lagaffe                    61
```

36. Obtenir le nom, le poids et l’année des élèves dont le poids est supérieur au poids moyen des élèves étant dans la même année d’études.

```sql
select nom, poids, annee from eleves where annee = 1 and poids > (select avg(poids) from eleves where annee = 1) Union select nom, poids, annee from eleves where annee = 2 and poids > (select avg(poids) from eleves where annee = 2);
--ou
select nom, poids, annee from eleves where annee = 1 and poids > (select avg(poids) from eleves where annee = 1) Or annee = 2 and poids > (select avg(poids) from eleves where annee = 2);
```

```bash
NOM            POIDS     ANNEE
-------------- ---------- ----------
Danny          82        2
Lagaffe        61        1
Lebut          75        2
Spring         78        2
Walthéry       59        1
```

37. Obtenir le nom des professeurs qui ne donnent pas le cours numéro 1.

```sql
select nom from professeurs where num_prof not in (select num_prof from charge where num_cours = 1);
--ou
select nom from professeurs p where not exists (select num_prof from charge c where num_cours = 1 and p.num_prof = c.num_prof);
```

```bash
NOM
-------------------------
Tonilaclasse
Pastecnov
Selector
Vilplusplus
Francesca
```

38. Obtenir le nom des élèves de 1 ère année qui ont obtenu plus de 60 % et
qui jouent au tennis.

```sql
select nom from eleves where annee = 1 and num_eleve in (select num_eleve from resultats having (avg(points)*5)>60 group by num_eleve) and num_eleve in (select num_eleve from activites_pratiquees where nom = 'Tennis' );
--ou
select t1.nom from eleves t1
join activites_pratiquees t3 on t1.num_eleve = t3.num_eleve
join resultats t2 on t1.num_eleve = t2.num_eleve where t3.nom = 'Tennis' and t1.annee = 1
having(avg(t2.points)*5)>60  group by t1.nom;
```

```bash
NOM
-------------------------
Brisefer
```

39. Professeurs qui prennent en charge tous les cours de deuxième année ;
on demande le Numéro et le nom.

```sql
select num_prof, nom from Professeurs where num_prof in (select num_prof from charge where num_cours in (select num_cours from cours where annee = 2) having count(num_prof)=(select count(num_cours) from cours where annee = 2) group by num_prof);
--ou
select t1.num_prof, t1.nom from professeurs t1 join charge t2 on t1.num_prof = t2.num_prof join cours t3 on t2.num_cours = t3.num_cours where annee = 2 group by t1.num_prof,t1.nom having count(t1.num_prof) = (select count(num_cours) from cours where annee = 2);
```

```bash
NUM_PROF   NOM
---------- -------------------------
 8         Pucette
 3         Tonilaclasse

```

40. Élèves qui pratiquent toutes les activités ; on demande le Numéro et le
nom.

```sql
select t1.num_eleve,t1.nom from eleves t1 where not exists (select * from activites t2 where not exists (select * from activites_pratiquees t3 where t1.num_eleve=t3.num_eleve and t2.nom = t3.nom));
--ou
select num_eleve,nom from eleves E where (select count(distinct nom) from activites) = (select count(distinct nom) from activites_pratiquees A where A.num_eleve = E.num_eleve)  ;
```

```bash
NUM_ELEVE  NOM
---------- -------------------------
1        Brisefer
```

<div class="page-break"></div>

## TP 7 : SQL Vues et Arbres ##

1.  Lancement du script famille.sql

```sql
@famille.sql
```

2.  Schémas E/A de la table.

               0,n              PERSONNES             0,n          
           Est mere de          -NOM               Est pere de       
MERE                            -PRENOM                              PERE
                                -DATENAISSANCE  
              0,1               -SEXE                 0,1
          Est enfant de                           Est enfant de  


3.  Création d'une vue  permettant d’afficher les ancêtres d’une personne avec la commande CONNECT BY

```sql
CREATE VIEW ancetres AS select * from personnes;
--
CREATE VIEW vue1 AS (SELECT * FROM PERSONNES WHERE (NUMERO != 1) START WITH NUMERO = &num CONNECT BY NUMERO = PRIOR PERE OR NUMERO = PRIOR MERE);
```

4.  Affichez le père, le grand père et l’arrière grand père d’une personne (&num) en n’utilisant pas CONNECT BY mais en utilisant plusieurs vues intermédiaires

```sql
CREATE VIEW pere AS (SELECT * FROM PERSONNES WHERE NUMERO = 3);

CREATE VIEW grandpere AS (SELECT * FROM PERSONNES WHERE NUMERO = (select pere from pere));

CREATE VIEW arrieregrandpere AS (SELECT * FROM PERSONNES WHERE NUMERO = (select pere from grandpere));

CREATE VIEW ancetres2 AS (SELECT * FROM pere union select * from grandpere union select * from arrieregrandpere );

select * from ancetres2;
```

5.  Affichez tous les descendants d’une personne.

```sql
SELECT * FROM personnes start with  numero = 99 CONNECT BY pere = prior numero or mere = prior numero;
```

6.  Affichez les frères et sœurs d’une personne.

```sql
CREATE VIEW parents AS (select PERE,MERE FROM PERSONNES WHERE NUMERO =
10);

CREATE VIEW FS As (select * from personnes where numero != 10 and pere in(select pere from parents) and mere in(select mere from parents));

select * from FS;
```

7.  Affichez seulement les sœurs d’une personne.

```sql
select * from FS where sexe = 'F';
```

8.  Affichez tous les enfants des femmes de plus de 40 ans

```sql
select * from personnes where level = 2 start with  sexe = 'F' and ((SYSDATE - DATENAISSANCE)/365 > 40) CONNECT BY mere = prior numero;
```

9.  Affichez les cousins et cousines d’une personne.

10. Affichez les cousins issus de germain d’une personne.

11. Affichez les petits enfants de la personne la plus âgée ayant des petits enfants.

12. Pour chaque personne, affichez les nom, prénom et âge de son descendant le plus jeune.
