PROMPT *************** Destruction des tables de la bd ecole *************
PROMPT

DROP TABLE ETUDIANTS;

PROMPT Création de la table ETUDIANTS
PROMPT

CREATE TABLE etudiants (
  numero          NUMBER(4)     PRIMARY KEY,
  nom             VARCHAR2(25)  NOT NULL,
  prenom          VARCHAR2(25)  NOT NULL,
  sexe            CHAR(1)       CHECK (sexe IN ('F', 'M')),
  dateDeNaissance DATE          NOT NULL,
  Poids           NUMBER,
  Annee           NUMBER
);


PROMPT
pause Tapez sur Enter...
PROMPT


desc etudiants;

PROMPT
pause Tapez sur Enter...
PROMPT

select * from etudiants;

PROMPT
pause Tapez sur Enter...
PROMPT

INSERT INTO etudiants VALUES (71, 'Nervac', 'Feguir', 'M', '10/12/1978', 77, 1);
INSERT INTO etudiants VALUES (72, 'Bayard', 'Emeric', 'M', '21/11/1995', 80, 5);
INSERT INTO etudiants VALUES (73, 'Saddik', 'Nady', 'M', '24/05/1999', 15, 2);
INSERT INTO etudiants VALUES (74, 'Sy', 'Jeff', 'M', '01/10/1995', 70, 4);
INSERT INTO etudiants VALUES (69, 'Saguez', 'Virginie', 'F', '10/06/1998', 70, 1);
INSERT INTO etudiants VALUES (55, 'Kashem', 'Sadman', 'M', '10/03/1997', 90, 1);

select * from etudiants;

PROMPT
pause Tapez sur Enter...
PROMPT

INSERT INTO etudiants VALUES (71, 'Akito', 'Diamando', 'M', '06/07/1995', 77, 1);



PROMPT
pause Tapez sur Enter...
PROMPT

INSERT INTO etudiants VALUES (21, 'Akito', 'Diamando', 'N', '06/07/1995', 77, 1);


PROMPT
pause Tapez sur Enter...
PROMPT

INSERT INTO etudiants VALUES (21, 'Akito', 'Diamando', 'M', NULL, 77, 1);

PROMPT
pause Tapez sur Enter...
PROMPT

