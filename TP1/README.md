## TP1 ##

### B. Premiere création de table ###

1.  Création de la table etudiants

```sql
@etudiants.sql
```

2.  Vérification de la table avec

```sql
desc etudiants;
```

```bash
Nom             NULL ?   Type
--------------- -------- -------------
NUMERO          NOT NULL NUMBER(4)
NOM             NOT NULL VARCHAR2(25)
PRENOM          NOT NULL VARCHAR2(25)
SEXE                     CHAR(1)
DATEDENAISSANCE NOT NULL DATE
POIDS                    NUMBER
ANNEE                    NUMBER
```

```sql
select * from etudiants;
```

```bash
aucune ligne sélectionnée
```

desc nom_table affiche les attributs d'une table ainsi que leurs contraintes c'est-à-dire le type, la taille max et si l'attribut peut être NULL ou non(elle ne montre pas les CHECK).
select * from nom_table affiche tout le contenue d'une table.

3.  Définition de l'attribue de la clé primaire.

```sql
numero        NUMBER(4)     PRIMARY KEY,
```

L'attribut numero est la clé primaire.

4.  Les contraintes définit sont :

*   numéro non null && unique (car clée primaire) && 4 carac max
*   Nom non null && 25 carac max
*   Prenom non null && 25 carac max
*   Sexe soit F, soit M && 1 carac max
*   dateDeNaissance non null.

5.  Ajout des lignes suivantes:

```sql
INSERT INTO etudiants VALUES (71, 'Nervac', 'Feguir', 'M', '10/12/1978', 77, 1);
INSERT INTO etudiants VALUES (72, 'Bayard', 'Emeric', 'M', '21/11/1995', 80, 5);
INSERT INTO etudiants VALUES (73, 'Saddik', 'Nady', 'M', '24/05/1999', 15, 2);
INSERT INTO etudiants VALUES (74, 'Sy', 'Jeff', 'M', '01/10/1995', 70, 4);
INSERT INTO etudiants VALUES (69, 'Saguez', 'Virginie', 'F', '10/06/1998', 70, 1);
INSERT INTO etudiants VALUES (55, 'Kashem', 'Sadman', 'M', '10/03/1997', 90, 1);

select * from etudiants;
```

```bash
NUMERO  NOM      PRENOM   S DATEDENA POIDS  ANNEE
------- -------- -------- - -------- ------ ------
71      Nervac   Feguir   M 10/12/78 77     1
72      Bayard   Emeric   M 21/11/95 80     5
73      Saddik   Nady     M 24/05/99 15     2
74      Sy       Jeff     M 01/10/95 70     4
69      Saguez   Virginie F 10/06/98 70     1
55      Kashem   Sadman   M 10/03/97 90     1
```

6.  Ajout de lignes violant les contraintes définies:

```sql
INSERT INTO etudiants VALUES (71, 'Akito', 'Diamando', 'M', '06/07/1995', 77, 1);
```

```bash
ORA-00001: violation de contrainte unique (L3INFO_131.SYS_C001018742)
```

```sql
INSERT INTO etudiants VALUES (21, 'Akito', 'Diamando', 'N', '06/07/1995', 77, 1);
```

```bash
ORA-02290: violation de contraintes (L3INFO_131.SYS_C001018741) de v�rification
```

```sql
INSERT INTO etudiants VALUES (21, 'Akito', 'Diamando', 'M', NULL, 77, 1);
```

```bash
ORA-01400: impossible dinsérer NULL dans ("L3INFO_131"."ETUDIANTS"."DATEDENAISSANCE")
```

On ne peut donc pas insérer une ligne si les contraintes ne sont pas respectés.
Toutes les contraintes sont bien vérifiées.


### C. Base de données avec plusieurs tables ###

```sql
@ecole.sql
```
