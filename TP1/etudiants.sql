CREATE TABLE etudiants (
  numero          NUMBER(4)     PRIMARY KEY,
  nom             VARCHAR2(25)  NOT NULL,
  prenom          VARCHAR2(25)  NOT NULL,
  sexe            CHAR(1)       CHECK (sexe IN ('F', 'M')),
  dateDeNaissance DATE          NOT NULL,
  Poids           NUMBER,
  Annee           NUMBER
);
