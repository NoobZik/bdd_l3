DROP TABLE eleves CASCADE CONSTRAINTS;
DROP TABLE activites_pratiquees CASCADE CONSTRAINTS;
DROP TABLE charge CASCADE CONSTRAINTS;
DROP TABLE resultats CASCADE CONSTRAINTS;
DROP TABLE professeurs CASCADE CONSTRAINTS;
DROP TABLE activites CASCADE CONSTRAINTS;
DROP TABLE cours CASCADE CONSTRAINTS;

CREATE TABLE eleves (
  num_eleve        NUMBER(4),
  nom              VARCHAR2(25)  NOT NULL,
  prenom           VARCHAR2(25)  NOT NULL,
  dateDeNaissance  DATE          NOT NULL,
  Poids            NUMBER        NOT NULL,
  Annee            NUMBER(4)     NOT NULL,
  CONSTRAINT eleves_PK PRIMARY KEY(num_eleve)
);


CREATE TABLE cours (
  num_cours     NUMBER(4),
  nom           VARCHAR2(25)    NOT NULL,
  nbheures      NUMBER          NOT NULL,
  annee         NUMBER(4)       NOT NULL,
  CONSTRAINT cours_PK PRIMARY KEY(num_cours)
);


CREATE TABLE activites (
  nom     VARCHAR(25),
  niveau  VARCHAR(25),
  equipe  VARCHAR(25) NOT NULL,
  CONSTRAINT activites_PK PRIMARY KEY(nom, niveau)
);


CREATE TABLE professeurs (
  num_prof        NUMBER,
  nom             VARCHAR(25) NOT NULL,
  specialite      VARCHAR(25) NOT NULL,
  date_entree     DATE        NOT NULL,
  der_prom        VARCHAR(25) NOT NULL,
  salaire_base    NUMBER      NOT NULL,
  salaire_actuel  NUMBER      NOT NULL,
  CONSTRAINT professeurs_PK PRIMARY KEY(num_prof)
);


CREATE TABLE resultats (
  num_eleve NUMBER,
  num_cours NUMBER,
  points    NUMBER NOT NULL,
  CONSTRAINT resultats_eleve_FK FOREIGN KEY (num_eleve)  REFERENCES eleves(num_eleve),
  CONSTRAINT resultats_cours_FK FOREIGN KEY (num_cours)  REFERENCES cours(num_cours)
);


CREATE TABLE charge (
  num_prof  NUMBER,
  num_cours NUMBER,
  CONSTRAINT charge_prof_FK FOREIGN KEY (num_prof) REFERENCES professeurs(num_prof),
  CONSTRAINT charge_cour_FK FOREIGN KEY (num_cours) REFERENCES cours(num_cours)
);


CREATE TABLE activites_pratiquees (
  num_eleve NUMBER,
  nom       VARCHAR(25),
  niveau    VARCHAR(25),
  CONSTRAINT activites_pratiquees_eleves_FK FOREIGN KEY (num_eleve) REFERENCES eleves(num_eleve),
  CONSTRAINT activites_pratiquees_FK FOREIGN KEY (nom, niveau) REFERENCES activites(nom, niveau)
);
