PROMPT ## TP 5 : Jointures ##

PROMPT ### A. Gestion d un café ###

PROMPT Création des tables par l intermédiaire d un script sql déjà rédigé.

@creatcafe.sql

PROMPT Insertion des valeurs.
PROMPT

@insertcafe.sql

PROMPT ##### 3. Formulation et tests d une série de requêtes en SQL #####

PROMPT 1.  Liste du contenu de chaque table de la base.

SELECT * FROM LESTABLES;

PROMPT
pause Tapez sur Enter...
PROMPT

select * from consommation;

PROMPT
pause Tapez sur Enter...
PROMPT

select * from serveur;

PROMPT
pause Tapez sur Enter...
PROMPT

select * from facture;

PROMPT
pause Tapez sur Enter...
PROMPT

select * from comprend;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 2.  Nombre de places de la table N°4 (Nbplace).
PROMPT

SELECT NBPLACE FROM LESTABLES WHERE NUMTABLE = 4;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 3.  Liste des consommations dont le prix unitaire est supérieur à 1 euro (Numcons, Libcons, Prixcons).
PROMPT

SELECT * FROM CONSOMMATION WHERE PRIXCONS >1;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 4.  Liste des serveurs de Belfort et de Delle (Numserv, Nomserv, Villeserveur).
PROMPT

SELECT NUMSERVEUR, NOMSERVEUR, VILLESERVEUR FROM SERVEUR WHERE VILLESERVEUR = 'BELFORT' OR VILLESERVEUR = 'DELLE';

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 5.  Liste des factures du 2 février servies par le serveur 52 (Numfact, Numtable).
PROMPT

SELECT NUMFACTURE,NUMTABLE FROM FACTURE WHERE DATEFACTURE = '02/02/10' AND NUMSERVEUR = 52;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 6.  Liste des consommations de la facture 1203 (Numcons, Qte).
PROMPT

SELECT NUMCONS, QTE FROM COMPREND WHERE  NUMFACTURE = 1203 AND NUMFACTURE = 1203;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 7.  Liste des consommations des factures 1200 et 1201 (sans lignes en double) (Numcons).
PROMPT

SELECT NUMCONS FROM COMPREND WHERE NUMFACTURE = 1200 OR NUMFACTURE = 1201 GROUP BY (NUMCONS);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 8.  Liste des serveurs qui sont nés en 1976 (Nomserv, Datenserveur).
PROMPT

SELECT NOMSERVEUR,DATENSERVEUR FROM SERVEUR WHERE EXTRACT(YEAR FROM DATENSERVEUR) = 1976;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 9.  Liste des consommations de type bière (Numcons, Libcons, Prixcons).
PROMPT

SELECT * FROM CONSOMMATION WHERE SUBSTR(LIBCONS,1,5)='Biere';
PROMPT
pause Tapez sur Enter...
PROMPT
SELECT * FROM CONSOMMATION WHERE LIBCONS LIKE 'Biere%';

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 10. Liste des tables servies après le 1 février.
PROMPT

SELECT * FROM FACTURE WHERE DATEFACTURE > '01/02/2010';

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 11. Liste des serveurs dont le nom contient i en deuxième position (Nomserv).
PROMPT

SELECT NOMSERVEUR FROM SERVEUR WHERE SUBSTR(NOMSERVEUR,2,1) = 'i';

PROMPT
pause Tapez sur Enter...
PROMPT

SELECT NOMSERVEUR FROM SERVEUR WHERE NOMSERVEUR LIKE '_i%';

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 12. Liste des serveurs dont le nom commence par un P (Nomserv).
PROMPT

SELECT NOMSERVEUR FROM SERVEUR WHERE SUBSTR(NOMSERVEUR,1,1) = 'P';

PROMPT
pause Tapez sur Enter...
PROMPT

SELECT NOMSERVEUR FROM SERVEUR WHERE NOMSERVEUR LIKE 'P%';


PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 13. Liste des serveurs par ville (Nomserv, Villeserveur).
PROMPT

SELECT NOMSERVEUR,VILLESERVEUR FROM SERVEUR ORDER BY VILLESERVEUR ASC;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 14. Affichage d une liste de consommation classé par ordre alphabétique
PROMPT

select libcons, numcons, prixcons from consommation order by libcons asc;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 15. Liste des villes où habitent des serveurs (sans lignes en double) (Villeserveur).
PROMPT

SELECT VILLESERVEUR FROM SERVEUR GROUP BY (VILLESERVEUR);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 16. Le nombre de tables du restaurant.
PROMPT

SELECT COUNT(*) FROM LESTABLES;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 17. Le nombre de places disponibles sur l ensemble des tables.
PROMPT

SELECT SUM(NBPLACE) FROM LESTABLES;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 18. Nombre de factures établies par chaque serveur (Numserv, Nbfacture).
PROMPT

SELECT NUMSERVEUR, COUNT(*) FROM FACTURE GROUP BY (NUMSERVEUR);

PROMPT
pause Tapez sur Enter...
PROMPT

select numserveur, count(numserveur) as nb_factures from serveur natural join facture group by numserveur;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 19. Nombre de factures établies chaque jour (Datefacture, Nbfacture).
PROMPT

SELECT DATEFACTURE, COUNT(*) FROM FACTURE GROUP BY(DATEFACTURE) ORDER BY DATEFACTURE ASC;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 20. Liste des serveurs qui ont établi plus de 3 factures (Numserv, Nbfacture).
PROMPT

SELECT NUMSERVEUR, COUNT(*) FROM FACTURE GROUP BY(NUMSERVEUR) HAVING COUNT(*)>3;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 21. Prix moyen des consommation
PROMPT

select avg(prixcons) from consommation;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 22. Prix moyen du café
PROMPT

select avg(prixcons) from consommation where substr(libcons,1,4) = 'Cafe';

PROMPT
pause Tapez sur Enter...
PROMPT

select avg(prixcons) from consommation where libcons like 'Cafe%';

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 23. Quantité moyenne consommée pour chaque consommation.
PROMPT

select numcons, avg(qte) from comprend group by (numcons);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 24. Nombre de serveurs par villes
PROMPT

select villeserveur, count(numserveur) from serveur group by (villeserveur);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 25. Liste des villes dans lesquelles habitent plus d un serveur.
PROMPT

SELECT villeserveur, count(*) as nb_serveur FROM serveur GROUP BY(villeserveur) HAVING COUNT(*)>1;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 26. Nombre de type de consommation par factures.
PROMPT

select numfacture, count(numcons) from comprend group by (numfacture);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 27. Nombre total de consommation en comptant la Quantité par facture
PROMPT

select numfacture, count(qte) from comprend group by (numfacture);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 28. Nombre de facture par consommation.
PROMPT

select numcons, count(numfacture) from comprend group by (numcons);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 29. Consommation qui interviennent dans plus de 2 factures.
PROMPT

select numcons, count(numfacture) from comprend group by (numcons) having count(numfacture) > 2;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 30. Liste des serveur triés par nom de ville croissant, puis nom de serveur croissant
PROMPT

select * from serveur order by villeserveur, nomserveur;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 31. Liste des serveurs, triés par nom de ville décroissant, puis nom de serveur croissant.
PROMPT

select * from serveur order by villeserveur desc, nomserveur asc;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 32. Liste des factures avec leur numéro de table et le nom du serveur
PROMPT

select numfacture, numtable, nomserveur from facture natural join serveur;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 33. Liste des factures de la table 5 avec le nom du serveur.
PROMPT

select numfacture, numtable, nomserveur from facture natural join serveur where numtable = 5;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 34. Liste des factures avec leur nom de table et le nom du serveur
PROMPT

select numfacture, nomtable, nomserveur from facture natural join lestables natural join serveur;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 35. Listes des serveurs et des tables qu ils ont servies ordonnés selon le nom du
PROMPT

select distinct nomserveur, nomtable from facture natural join serveur natural join lestables ORDER BY nomserveur;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 36. Liste des consommation de la facture 1203 avec leur nom, leur prix et leur Quantité
PROMPT

select numcons, libcons, prixcons, qte from comprend natural join consommation where numfacture = 1203;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 37. Liste des consommations du premier février de la table 5 avec leur nom, leur prix, et leur Quantité
PROMPT

select numcons, libcons, prixcons, qte from comprend natural join consommation natural join facture where datefacture = '01/02/10';

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 38. Liste des tables et des numéros de factures qui leur sont associées.
PROMPT

select nomtable, numfacture from lestables natural left join facture;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 39. Pareil
PROMPT

select nomtable, numfacture from facture natural right join lestables;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 40. Liste des tables qui n ont aucune factures
PROMPT

select numtable, nomtable from lestables natural left join facture where numfacture is null;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 41. Liste des consommation qui ont déjà été servies par le serveur 52
PROMPT

select numcons, libcons from comprend natural join consommation natural left join facture where numserveur = 52;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 42. Liste des consommation qui n ont jamais été servies
PROMPT

select numcons, libcons from comprend natural right join consommation where qte is null;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 43. La liste des facture avec leur date et leur nombre de consommation.
PROMPT

select numfacture, datefacture, sum(qte) from comprend natural join facture group by numfacture, datefacture;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 44. La liste des factures et le montant de leur addition
PROMPT

select numfacture, sum(qte*prixcons) from comprend natural join consommation group by numfacture;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 45. Nombre de consommation servies par jour
PROMPT

select datefacture, sum(qte) from facture natural join comprend group by datefacture;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 46. Montant global du chiffre d affaire par jour
PROMPT

select datefacture, sum(qte*prixcons) from facture natural join comprend natural join consommation group by datefacture;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 47. La liste des serveurs par nom et leur nombre de factures.
PROMPT

select nomserveur, count(numfacture) from serveur natural left join facture group by nomserveur;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 48. La liste des serveurs par nom et le nombre de consommation qu ils ont servies.
PROMPT

select nomserveur, count(qte) from serveur natural join facture natural join comprend group by nomserveur;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 49. LA liste des serveurs par nom et leur chiffre d affaire
PROMPT

select nomserveur, sum(qte*prixcons) as ca from comprend natural join consommation natural join facture natural join serveur group by (nomserveur);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 50. Nom des table qui ont au moins deux factures

select numtable, count(numfacture) from facture natural join lestables group by numtable having count(numfacture) >= 2;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 51. La liste complète des consommations et le nombre de factures dans lesquelles elles apparaissent

select libcons, count(*) as nb_facture from consommation natural join comprend group by libcons;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 52. La liste complète des tables et leur chiffre d affaire
PROMPT

SELECT NOMTABLE, SUM(QTE*PRIXCONS) AS ca
FROM COMPREND
JOIN FACTURE ON FACTURE.NUMFACTURE = COMPREND.NUMFACTURE
JOIN CONSOMMATION ON CONSOMMATION.NUMCONS = COMPREND.NUMCONS
RIGHT JOIN LESTABLES ON FACTURE.NUMTABLE = LESTABLES.NUMTABLE
GROUP BY NOMTABLE;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT ### B. Généalogie royale ###

PROMPT #### 3. Formulation et test de requêtes ####

PROMPT Création de la table et insertion des valeurs
PROMPT

@genealogie.sql

PROMPT 1.  La liste des enfants d Elizabeth II
PROMPT

SELECT g1.NOM, g1.DATENAISSANCE FROM genealogie
g1 JOIN genealogie g2 ON g1.MERE = g2.NUMPER WHERE g2.NOM = 'Elizabeth II' ;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 2.  La mère du prince William

SELECT g1.NOM,g1.DATENAISSANCE FROM genealogie
g1 JOIN genealogie g2 ON g1.NUMPER = g2.MERE where g2.NOM = 'Prince Charles' ;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 3.  Les parents dElizabeth II
PROMPT

SELECT g1.NOM,g1.DATENAISSANCE FROM genealogie
g1 JOIN genealogie g2 ON g1.NUMPER = g2.MERE OR g1.NUMPER = g2.PERE where g2.NOM = 'Elizabeth II' ;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 4.  Les frères et sœurs du Prince Charles (Nom, DateNaissance).
PROMPT

SELECT g1.NOM,g1.DATENAISSANCE FROM genealogie
g1 JOIN genealogie g2 ON g1.MERE = g2.MERE AND g1.PERE = g2.PERE where g2.NOM = 'Prince Charles' AND g1.NUMPER != g2.NUMPER;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 5.  Le nom des individus, le nom de leur père (ou NULL) et le nom de leur mère (ou NULL) (Nom, Nompere, Nommere).

SELECT g1.NOM, g2.NOM "Nom Père",g3.NOM "Nom Mère"  FROM genealogie
g1 LEFT JOIN genealogie g2 ON g1.PERE = g2.NUMPER
LEFT JOIN genealogie g3 ON g1.MERE = g3.NUMPER;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 6.  La liste des individus et le nombre de leurs enfants étant dans la base de données (Nom, NbEnfants).
PROMPT

SELECT g1.NOM, COUNT(g2.NUMPER) FROM genealogie
g1 LEFT JOIN genealogie g2 ON g2.MERE = g1.NUMPER OR g2.PERE = g1.NUMPER
GROUP BY g1.NOM;
