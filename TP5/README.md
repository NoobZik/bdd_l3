## TP 5 : Jointures ##

### A. Gestion d'un café ###

Création des tables par l'intermédiaire d'un script sql déjà rédigé.

```sql
@creatcafe.sql
```
Insertion des valeurs.

```sql
@insertcafe.sql
```
##### 3. Formulation et tests d'une série de requêtes en SQL #####

1.  Liste du contenu de chaque table de la base.

```sql
SELECT * FROM LESTABLES;
select * from consommation;
select * from serveur;
select * from facture;
select * from comprend;
```

```bash
NUMTABLE  NOMTABLE	     NBPLACE
--------- -------------- ----------
 1        entree-gche    6
 2        entree-dte     10
 3        fenetre1       3
 4        fenetre2       8
 5        fenetre3       4
 6        fond-gche      4
 7        fond-dte       2

 NUMCONS    LIBCONS               PRIXCONS
----------- --------------------- ----------
    100     Cafe                       ,9
    101     Cafe double               1,3
    102     Cafe creme                1
    105     Chocolat                  1,5
    106     Biere pression            1,8
    107     Biere 25cl                2
    108     Biere 33cl                2,2
    110     Biere 50cl                2,5
    120     Jus de fruits             1,7
    121     Jus de fruits presse      2,6
    122     Perrier                   1,6
    124     Orangina                  1,4
    130     Coca Cola                 1,7

NUMSERVEUR NOMSERVEUR RUESERVEUR        CPSERVEUR VILLESERVEUR DATENSER
---------- ---------- ----------------  --------- ------------ --------
  50       Pizzi      3 rue des lilas     90000   BELFORT      12/01/76
  51       Cathy      25 av Roosevelt     90100   DELLE        05/04/78
  52       Totof      46 grande rue       90500   BAVILLIERS   30/09/84
  53       Pilou      5 impasse Martin    90000   BELFORT      17/08/86
  54       Alice      15 rue de la barre  95880   ENGHIEN      13/03/86

NUMFACTURE   NUMTABLE NUMSERVEUR DATEFACT
----------- --------- ---------- --------
      1200    1       53 01/02/10
      1201    5       53 01/02/10
      1202    3       52 01/02/10
      1203    5       50 01/02/10
      1204    4       52 02/02/10
      1205    1       53 02/02/10
      1206    3       52 02/02/10
      1207    5       53 02/02/10
      1208    7       54 02/02/10

NUMFACTURE    NUMCONS	     QTE
---------- ---------- ----------
      1200   101       3
      1200   106       1
      1200   120       1
      1201   105       2
      1201   106       2
      1202   100       2
      1202   122       1
      1203   102       1
      1203   108       1
      1203   121       1
      1203   130       1
      1204   122       4
      1204   124       2
      1205   100       2
      1206   108       3
      1207   108       1
      1207   110       2
      1208   108       2


```

2.  Nombre de places de la table N°4 (Nbplace).

```sql
SELECT NBPLACE FROM LESTABLES WHERE NUMTABLE = 4;
```

```bash
   NBPLACE
----------
	 8
```

3.  Liste des consommations dont le prix unitaire est supérieur à 1 euro
(Numcons, Libcons, Prixcons).

```sql
SELECT * FROM CONSOMMATION WHERE PRIXCONS >1;
```

```bash
NUMCONS LIBCONS             PRIXCONS
------- ------------------- ----------
    101 Cafe double         1,3
    105 Chocolat            1,5
    106 Biere pressio       1,8
    107 Biere 25cl          2
    108 Biere 33cl          2,2
    110 Biere 50cl          2,5
    120 Jus de fruit        1,7
    121 Jus de fruits press 2,6
    122 Perrier             1,6
    124 Orangina            1,4
    130 Coca Cola           1,7
```

4.  Liste des serveurs de Belfort et de Delle (Numserv, Nomserv, Villeserveur).

```sql
SELECT NUMSERVEUR, NOMSERVEUR, VILLESERVEUR FROM SERVEUR WHERE VILLESERVEUR = 'BELFORT' OR VILLESERVEUR = 'DELLE';
```

```bash
NUMSERVEUR NOMSERVEUR  VILLESERVEUR
---------- ----------- --------------
  50       Pizzi       BELFORT
  51       Cathy       DELLE
  53       Pilou       BELFORT
```

5.  Liste des factures du 2 février servies par le serveur 52 (Numfact,
Numtable).

```sql
SELECT NUMFACTURE,NUMTABLE FROM FACTURE WHERE DATEFACTURE = '02/02/10' AND NUMSERVEUR = 52;
```

```bash
NUMFACTURE   NUMTABLE
------------ ----------
      1204	    4
      1206	    3
```

6.  Liste des consommations de la facture 1203 (Numcons, Qte).

```sql
SELECT NUMCONS, QTE FROM COMPREND WHERE  NUMFACTURE = 1203 AND NUMFACTURE = 1203;
```

```bash
NUMCONS  QTE
------- -------
    102   1
    108   1
    121   1
    130   1
```

7.  Liste des consommations des factures 1200 et 1201 (sans lignes en double)
(Numcons).

```sql
SELECT NUMCONS FROM COMPREND WHERE NUMFACTURE = 1200 OR NUMFACTURE = 1201 GROUP BY (NUMCONS);
```

```bash
   NUMCONS
----------
       120
       101
       105
       106
```

8.  Liste des serveurs qui sont nés en 1976 (Nomserv, Datenserveur).

```sql
SELECT NOMSERVEUR,DATENSERVEUR FROM SERVEUR WHERE EXTRACT(YEAR FROM DATENSERVEUR) = 1976;
```

```bash
NOMSERVEUR DATENSER
---------- --------
Pizzi      12/01/76
```

9.  Liste des consommations de type bière (Numcons, Libcons, Prixcons).

```sql
SELECT * FROM CONSOMMATION WHERE SUBSTR(LIBCONS,1,5)='Biere';
-- ou
SELECT * FROM CONSOMMATION WHERE LIBCONS LIKE 'Biere%';
```

```bash
NUMCONS LIBCONS         PRIXCONS
------- --------------- --------
    106 Biere pression    1,8
    107 Biere 25cl        2
    108 Biere 33cl        2,2
    110 Biere 50cl        2,5
```

10. Liste des tables servies après le 1 février.

```sql
SELECT * FROM FACTURE WHERE DATEFACTURE > '01/02/2010';
```

```bash
NUMFACTURE NUMTABLE NUMSERVEUR DATEFACT
---------- -------- ---------- --------
      1204    4      52        02/02/10
      1205    1      53        02/02/10
      1206    3      52        02/02/10
      1207    5      53        02/02/10
      1208    7      54        02/02/10
```

11. Liste des serveurs dont le nom contient i en deuxième position (Nomserv).

```sql
SELECT NOMSERVEUR FROM SERVEUR WHERE SUBSTR(NOMSERVEUR,2,1) = 'i';
-- ou
SELECT NOMSERVEUR FROM SERVEUR WHERE NOMSERVEUR LIKE '_i%';
```

```bash
NOMSERVEUR
--------------------
Pizzi
Pilou
```

12. Liste des serveurs dont le nom commence par un P (Nomserv).

```sql
SELECT NOMSERVEUR FROM SERVEUR WHERE SUBSTR(NOMSERVEUR,1,1) = 'P';
-- ou
SELECT NOMSERVEUR FROM SERVEUR WHERE NOMSERVEUR LIKE 'P%';
```

```bash
NOMSERVEUR
--------------------
Pizzi
Pilou
```

13. Liste des serveurs par ville (Nomserv, Villeserveur).

```sql
SELECT NOMSERVEUR,VILLESERVEUR FROM SERVEUR ORDER BY VILLESERVEUR ASC;
```

```bash
NOMSERVEUR  VILLESERVEUR
----------- --------------
Totof        BAVILLIERS
Pilou        BELFORT
Pizzi        BELFORT
Cathy        DELLE
Alice        ENGHIEN
```

14. Affichage d'une liste de consommation classé par ordre alphabétique

```sql
SQL> select libcons, numcons, prixcons from consommation order by libcons asc;
```

```bash
LIBCONS         NUMCONS   PRIXCONS
-------------- ---------  ---------
Biere pression        106    1,8
Biere 25cl            107    2
Biere 33cl            108    2,2
Biere 50cl            110    2,5
Cafe                  100     ,9
Cafe creme            102    1
Cafe double           101    1,3
Chocolat              105    1,5
Coca Cola             130    1,7
Jus de fruits         120    1,7
Jus de fruits presse  121    2,6
Orangina              124    1,4
Perrier               122    1,6
```

15. Liste des villes où habitent des serveurs (sans lignes en double)
(Villeserveur).

```sql
SELECT VILLESERVEUR FROM SERVEUR GROUP BY (VILLESERVEUR);
```

```bash
VILLESERVEUR
--------------------
DELLE
BAVILLIERS
ENGHIEN
BELFORT
```

16. Le nombre de tables du restaurant.

```sql
SELECT COUNT(*) FROM LESTABLES;
```

```bash
COUNT(*)
----------
 7
```

17. Le nombre de places disponibles sur l'ensemble des tables.

```sql
SELECT SUM(NBPLACE) FROM LESTABLES;
```

```bash
SUM(NBPLACE)
------------
	  37
```

18. Nombre de factures établies par chaque serveur (Numserv, Nbfacture).

```sql
SELECT NUMSERVEUR, COUNT(*) FROM FACTURE GROUP BY (NUMSERVEUR);
-- ou
select numserveur, count(numserveur) as nb_factures from serveur natural join facture group by numserveur;
```

```bash
NUMSERVEUR   COUNT(*)
---------- ----------
  54         1
  53         4
  52         3
  50         1
```

19. Nombre de factures établies chaque jour (Datefacture, Nbfacture).

```sql
SELECT DATEFACTURE, COUNT(*) FROM FACTURE GROUP BY(DATEFACTURE) ORDER BY DATEFACTURE ASC;
```

```bash
DATEFACT   COUNT(*)
-------- ----------
01/02/10    4
02/02/10    5
```

20. Liste des serveurs qui ont établi plus de 3 factures (Numserv, Nbfacture).

```sql
SELECT NUMSERVEUR, COUNT(*) FROM FACTURE GROUP BY(NUMSERVEUR) HAVING COUNT(*)>3;
```

```bash
NUMSERVEUR   COUNT(*)
---------- ----------
  53        4
```
21. Prix moyen des consommation

```sql
select avg(prixcons) from consommation;
```

```bash
AVG(PRIXCONS)
-------------
   1,70769231
```

22. Prix moyen du café

```sql
select avg(prixcons) from consommation where substr(libcons,1,4) = 'Cafe';
--ou
select avg(prixcons) from consommation where libcons like 'Cafe%';
```

```bash
AVG(PRIXCONS)
-------------
   1,06666667
```

23. Quantité moyenne consommée pour chaque consommation.

```sql
select numcons, avg(qte) from comprend group by (numcons);
```

```bash
NUMCONS   AVG(QTE)
-------- ----------
    100       2
    120       1
    121       1
    108    1,75
    102       1
    110       2
    101       3
    130       1
    105       2
    124       2
    106     1,5
    122     2,5
```

24. Nombre de serveurs par villes

```sql
select villeserveur, count(numserveur) from serveur group by (villeserveur);
```

```bash
-------------- ------
DELLE          1
BAVILLIERS     1
ENGHIEN        1
BELFORT        2
```

25. Liste des villes dans lesquelles habitent plus d'un serveur.

```sql
SELECT villeserveur, count(*) as nb_serveur FROM serveur GROUP BY(villeserveur) HAVING COUNT(*)>1;
```

```bash
VILLESERVEUR      NB_SERVEUR
---------------- ----------
BELFORT           2
```

26. Nombre de type de consommation par factures.

```sql
select numfacture, count(numcons) from comprend group by (numfacture);
```

```bash
NUMFACTURE COUNT(NUMCONS)
---------- --------------
      1200   3
      1201   2
      1202   2
      1203   4
      1204   2
      1205   1
      1206   1
      1207   2
      1208   1
```

27. Nombre total de consommation en comptant la Quantité par facture

```sql
select numfacture, count(qte) from comprend group by (numfacture);
```

```bash
NUMFACTURE COUNT(QTE)
---------- ----------
      1207  2
      1208  1
      1200  3
      1205  1
      1201  2
      1204  2
      1203  4
      1202  2
      1206  1
```
28. Nombre de facture par consommation.

```sql
select numcons, count(numfacture) from comprend group by (numcons);
```

```bash
NUMCONS COUNT(NUMFACTURE)
------- -----------------
    100     2
    120     1
    121     1
    108     4
    102     1
    110     1
    101     1
    130     1
    105     1
    124     1
    106     2
    122     2
```

29. Consommation qui interviennent dans plus de 2 factures.

```sql
select numcons, count(numfacture) from comprend group by (numcons) having count(numfacture) > 2;
```

```bash
NUMCONS COUNT(NUMFACTURE)
------- -----------------
    108   4
```
30. Liste des serveur triés par nom de ville croissant, puis nom de serveur croissant

```sql
select * from serveur order by villeserveur, nomserveur;
```

```bash
NUMSERVEUR NOMSERVEUR  RUESERVEUR	        CPSERVEUR VILLESERVEUR  DATENSER
---------- ----------  -----------------  --------- ------------- --------
  52       Totof       46 grande rue      90500     BAVILLIERS    30/09/84
  53       Pilou       5 impasse Martin   90000     BELFORT       17/08/86
  50       Pizzi       3 rue des lilas    90000     BELFORT       12/01/76
  51       Cathy       25 av Roosevelt    90100     DELLE         05/04/78
  54       Alice       15 rue de la barre 95880     ENGHIEN       13/03/86
```

31. Liste des serveurs, triés par nom de ville décroissant, puis nom de serveur croissant.
```sql

select * from serveur order by villeserveur desc, nomserveur asc;
```

```bash
NUMSERVEUR NOMSERVEUR RUESERVEUR CPSERVEUR VILLESERVEUR DATENSER
---------- --------------------- ------ -------------- --------
	54 Alice  15 rue de la barre    95880 ENGHIEN        13/03/86
	51 Cathy  25 av Roosevelt       90100 DELLE          05/04/78
	53 Pilou  5 impasse Martin      90000 BELFORT        17/08/86
	50 Pizzi  3 rue des lilas       90000 BELFORT        12/01/76
	52 Totof  46 grande rue         90500 BAVILLIERS     30/09/84
```


32. Liste des factures avec leur numéro de table et le nom du serveur

```sql
select numfacture, numtable, nomserveur from facture natural join serveur;
```

```bash
NUMFACTURE NUMTABLE NOMSERVEUR
---------- -------- -----------
      1200   1        Pilou
      1201   5        Pilou
      1202   3        Totof
      1203   5        Pizzi
      1204   4        Totof
      1205   1        Pilou
      1206   3        Totof
      1207   5        Pilou
      1208   7        Alice
```

33. Liste des factures de la table 5 avec le nom du serveur.

```sql
select numfacture, numtable, nomserveur from facture natural join serveur where numtable = 5;
```

```bash
NUMFACTURE  NUMTABLE  NOMSERVEUR
----------  --------- --------------------
      1201    5       Pilou
      1203    5       Pizzi
      1207    5       Pilou
```

34. Liste des factures avec leur nom de table et le nom du serveur

```sql
select numfacture, nomtable, nomserveur from facture natural join lestables natural join serveur;
```

```bash

NUMFACTURE NOMTABLE   NOMSERVEUR
---------- ---------- --------------------
     1206 fenetre1       Totof
     1201 fenetre3       Pilou
     1207 fenetre3       Pilou
     1208 fond-dte       Alice
     1200 entree-gche    Pilou
     1205 entree-gche    Pilou
     1204 fenetre2       Totof
     1203 fenetre3       Pizzi
     1202 fenetre1       Totof
```

35. Listes des serveurs et des tables qu'ils ont servies ordonnés selon le nom du

```sql
select distinct nomserveur, nomtable from facture natural join serveur natural join lestables ORDER BY nomserveur;
```

```bash
NOMSERVEUR NOMTABLE
---------- ---------------
Alice      fond-dte
Pilou      entree-gche
Pilou      fenetre3
Pizzi      fenetre3
Totof      fenetre1
Totof      fenetre2
```

36. Liste des consommation de la facture 1203 avec leur nom, leur prix et leur Quantité

```sql
select numcons, libcons, prixcons, qte from comprend natural join consommation where numfacture = 1203;
```

```bash
NUMCONS LIBCONS                PRIXCONS   QTE
------- ---------------------  ---------- ----------
    102 Cafe creme               1      1
    108 Biere 33cl             2,2      1
    121 Jus de fruits presse    ,6      1
    130 Coca Cola              1,7      1
```

37. Liste des consommations du premier février de la table 5 avec leur nom, leur prix, et leur Quantité

```sql
select libcons, qte from facture natural join comprend natural join consommation where datefacture = '01/02/10' and numtable = 5;
```

```bash
LIBCONS               QTE
--------------------- ----
Cafe creme            1
Chocolat              2
Biere pression				2
Biere 33cl            1
Jus de fruits presse  1
Coca Cola             1

```

38. Liste des tables et des numéros de factures qui leur sont associées.

```sql
select nomtable, numfacture from lestables natural left join facture;
```

```bash
NOMTABLE	NUMFACTURE
--------- ----------
entree-gche   1200
entree-gche   1205
entree-dte
fenetre1      1206
fenetre1      1202
fenetre2      1204
fenetre3      1203
fenetre3      1201
fenetre3      1207
fond-gch
fond-dte      1208
```

39. Pareil

```SQL
select nomtable, numfacture from facture natural right join lestables;
```

```bash
NOMTABLE  NUMFACTURE
--------- ----------
entree-gche      1200
entree-gche      1205
entree-dte
fenetre1         1206
fenetre1         1202
fenetre2         1204
fenetre3         1203
fenetre3         1201
fenetre3         1207
fond-gche
fond-dte         1208
```

40. Liste des tables qui n'ont aucune factures

```sql
select numtable, nomtable from lestables natural left join facture where numfacture is null;
```

```bash
NUMTABLE    NOMTABLE
---------- ---------------
 2          entree-dte
 6          fond-gche
```

41. Liste des consommation qui ont déjà été servies par le serveur 52

```SQL
select numcons, libcons from comprend natural join consommation natural left join facture where numserveur = 52;
```

```bash
NUMCONS LIBCONS
------- ------------------------------
    100 Cafe
    108 Biere 33cl
    122 Perrier
    122 Perrier
    124 Orangina
```

42. Liste des consommation qui n'ont jamais été servies

```sql
select numcons, libcons from comprend natural right join consommation where qte is null;
```

```bash
NUMCONS LIBCONS
------- ------------------------------
    107 Biere 25cl
```

43. La liste des facture avec leur date et leur nombre de consommation.

```sql
select numfacture, datefacture, sum(qte) from comprend natural join facture group by numfacture, datefacture;
```

```bash
NUMFACTURE DATEFACT   SUM(QTE)
---------- -------- ----------
      1202 01/02/10      3
      1204 02/02/10      6
      1203 01/02/10      4
      1205 02/02/10      2
      1200 01/02/10      5
      1207 02/02/10      3
      1201 01/02/10      4
      1206 02/02/10      3
      1208 02/02/10      2
```

44. La liste des factures et le montant de leur addition

```SQL
select numfacture, sum(qte*prixcons) from comprend natural join consommation group by numfacture;
```

```bash
NUMFACTURE SUM(QTE*PRIXCONS)
---------- -----------------
      1207   7,2
      1208   4,4
      1200   7,4
      1205   1,8
      1201   6,6
      1204   9,2
      1203   7,5
      1202   3,4
      1206   6,6
```

45. Nombre de consommation servies par jour

```sql
select datefacture, sum(qte) from facture natural join comprend group by datefacture;
```

```bash
DATEFACT   SUM(QTE)
---------- ---------
01/02/10   16
02/02/10   16
```

46. Montant global du chiffre d'affaire par jour

```sql
select datefacture, sum(qte*prixcons) from facture natural join comprend natural join consommation group by datefacture;
```

```bash
DATEFACT SUM(QTE*PRIXCONS)
-------- -----------------
01/02/10      24,9
02/02/10      29,2
```

47. La liste des serveurs par nom et leur nombre de factures.

```sql
select nomserveur, count(numfacture) from serveur natural left join facture group by nomserveur;
```

```bash
NOMSERVEUR  COUNT(NUMFACTURE)
----------- -----------------
Totof       3
Pizzi       1
Alice       1
Cathy       0
Pilou       4

```
48. La liste des serveurs par nom et le nombre de consommation qu'ils ont servies.

```SQL
select nomserveur, count(qte) from serveur natural join facture natural join comprend group by nomserveur;
```

```bash
NOMSERVEUR    SUM(QTE)
------------ ----------
Totof        12
Alice         2
Pizzi         4
Pilou        14
```
49. LA liste des serveurs par nom et leur chiffre d'affaire

```sql
select nomserveur, sum(qte*prixcons) as ca from comprend natural join consommation natural join facture natural join serveur group by (nomserveur);
```

```bash
NOMSERVEUR     CA
------------- ----------
Totof         19,2
Pizzi          7,5
Alice          4,4
Pilou           23
```

50. Nom des table qui ont au moins deux factures

```sql
select numtable, count(numfacture) from facture natural join lestables group by numtable having count(numfacture) >= 2;
```

```bash
NUMTABLE COUNT(NUMFACTURE)
-------- -----------------
 1         2
 5         3
 3         2
```

51. La liste complète des consommations et le nombre de factures dans lesquelles elles apparaissent

```sql
select libcons, count(*) as nb_facture from consommation natural join comprend group by libcons;
```

```bash
LIBCONS             NB_FACTURE
------------------ ----------
Coca Cola             1
Biere 50cl            1
Biere pression        2
Biere 33cl            4
Jus de fruits presse  1
Cafe                  2
Cafe double           1
Jus de fruits         1
Perrier               2
Cafe creme            1
Chocolat              1

LIBCONS               NB_FACTURE
-------------------- ----------
Orangina              1
```

52. La liste complète des tables et leur chiffre d'affaire

```sql
SELECT NOMTABLE, SUM(QTE*PRIXCONS) AS ca
FROM COMPREND
JOIN FACTURE ON FACTURE.NUMFACTURE = COMPREND.NUMFACTURE
JOIN CONSOMMATION ON CONSOMMATION.NUMCONS = COMPREND.NUMCONS
RIGHT JOIN LESTABLES ON FACTURE.NUMTABLE = LESTABLES.NUMTABLE
GROUP BY NOMTABLE;
```

```bash
NOMTABLE      CA
------------- ----------
fenetre1      10
entree-dte
fenetre3      21,3
fond-dte      4,4
fond-gche
entree-gche   9,2
fenetre2      9,2
```

### B. Généalogie royale ###

#### 3. Formulation et test de requêtes ####

Création de la table et insertion des valeurs

```sql
@genealogie.sql
```
1.  La liste des enfants d'Elizabeth II

```sql
SELECT g1.NOM, g1.DATENAISSANCE FROM genealogie
g1 JOIN genealogie g2 ON g1.MERE = g2.NUMPER WHERE g2.NOM = 'Elizabeth II' ;
```

```bash
NOM               DATENAIS
----------------- --------
Prince Charles    14/11/48
Princesse Anne    15/08/50
Prince Andrew     19/02/60
Prince Edward     10/03/64
```

2.  La mère du prince William

```SQL
SELECT g1.NOM,g1.DATENAISSANCE FROM genealogie
g1 JOIN genealogie g2 ON g1.NUMPER = g2.MERE where g2.NOM = 'Prince Charles' ;
```

```bash
NOM               DATENAIS
----------------- --------
Elizabeth II      21/04/26
```

3.  Les parents d'Elizabeth II

```sql
SELECT g1.NOM,g1.DATENAISSANCE FROM genealogie
g1 JOIN genealogie g2 ON g1.NUMPER = g2.MERE OR g1.NUMPER = g2.PERE where g2.NOM = 'Elizabeth II' ;
```

```bash
NOM                   DATENAIS
--------------------- --------
George VI             14/12/95
Elizabeth Bowes-Lyon  04/08/00
```

4.  Les frères et sœurs du Prince Charles (Nom, DateNaissance).

```sql
SELECT g1.NOM,g1.DATENAISSANCE FROM genealogie
g1 JOIN genealogie g2 ON g1.MERE = g2.MERE AND g1.PERE = g2.PERE where g2.NOM = 'Prince Charles' AND g1.NUMPER != g2.NUMPER;
```

```bash
NOM                   DATENAIS
--------------------- --------
Princesse Anne        15/08/50
Prince Andrew         19/02/60
Prince Edward         10/03/64
```

5.  Le nom des individus, le nom de leur père (ou NULL) et le nom de leur mère
(ou NULL) (Nom, Nompere, Nommere).

```sql
SELECT g1.NOM, g2.NOM "Nom Père",g3.NOM "Nom Mère"  FROM genealogie
g1 LEFT JOIN genealogie g2 ON g1.PERE = g2.NUMPER
LEFT JOIN genealogie g3 ON g1.MERE = g3.NUMPER;
```

```bash
NOM                     Nom Père                 Nom Mère
----------------------- ----------------------- -----------------------------------
Elizabeth II            George VI               Elizabeth Bowes-Lyon
Margaret du Royaume-Uni George VI               Elizabeth Bowes-Lyon
Prince Charles          Philip Mountbatten      Elizabeth II
Princesse Anne          Philip Mountbatten      Elizabeth II
Prince Andrew           Philip Mountbatten      Elizabeth II
Prince Edward           Philip Mountbatten      Elizabeth II
Prince William          Prince Charles          Diana Spencer
Prince Henry            Prince Charles          Diana Spencer
George VI
Elizabeth Bowes-Lyon
Philip Mountbatten
Diana Spencer
```

6.  La liste des individus et le nombre de leurs enfants étant dans la base de
données (Nom, NbEnfants).

```sql
SELECT g1.NOM, COUNT(g2.NUMPER) FROM genealogie
g1 LEFT JOIN genealogie g2 ON g2.MERE = g1.NUMPER OR g2.PERE = g1.NUMPER
GROUP BY g1.NOM;
```

```bash
NOM                       COUNT(G2.NUMPER)
------------------------ ----------------
Philip Mountbatten        4
George VI                 2
Elizabeth II              4
Margaret du Royaume-Uni   0
Prince Andrew             0
Elizabeth Bowes-Lyon      2
Prince Charles            2
Princesse Anne            0
Diana Spencer             2
Prince Henry              0
Prince Edward             0
Prince William            0
```
