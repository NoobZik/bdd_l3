drop table employees cascade constraint;

create table employees (
  NumEmp  NUMBER(4),
  NomEmp  VARCHAR(25),
  DateEmb DATE,
  DateSortie DATE,
  CONSTRAINT pk_employees PRIMARY KEY (NumEmp),
  CONSTRAINT nn_emp_nomEMP CHECK (NomEmp IS NOT NULL),
  CONSTRAINT nn_emp_DateEmb CHECK (DateEmb IS NOT NULL)
);

-- Affichage des employes

select * from employees;

-- Liste des noms de tous les employés

col NomEmp "Nom des employers" from employees;

-- Afficher a partir d'une date

select NomEmp from employees where DateEmb > '01/01/99';

-- Liste des eployers num et nom dont le nom commence par la lettre C

select NumEmp, NomEmp from employees where substr(NomEmp, 1, 1) = 'C';

-- Liste des employés triés par ordre decroissant sur les noms

select NomEmp from employees order by NomEmp desc;

-- Nombre d'employés embauché par année

select EXTRACT(YEAR from DateEmb), count(*) from employees group by extract(YEAR from DateEmb);

-- Nombre d'employés embauché chaque année ayant un nom de plus de 5 lettre

select EXTRACT(YEAR from DateEmb), count(*) from employees where length(NomEmp) > 5 group by extract(YEAR from DateEmb);

-- Nombre d'employés embauché chaque année ayant un nom commençant par L ou C, en ne gardant que les années avec au moins deux employés

select EXTRACT(YEAR from DateEmb), count(*) from employees where  group by extract(YEAR from DateEmb);
