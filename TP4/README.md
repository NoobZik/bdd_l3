## TP 4 : SQL Simple, tri et regroupements ##

### A. Table employés ###


1.  Creation de la table

```sql
drop table employees cascade constraint;

create table employees (
  NumEmp  NUMBER(4),
  NomEmp  VARCHAR(25),
  DateEmb DATE,
  DateSortie DATE,
  CONSTRAINT pk_employes PRIMARY KEY (NumEmp),
  CONSTRAINT nn_emp_nomEMP CHECK (NomEmp IS NOT NULL),
  CONSTRAINT nn_emp_DateEmb CHECK (DateEmb IS NOT NULL)
);
```

On ajoute des valeurs dans la table employees.

```sql
INSERT INTO EMPLOYEES VALUES (9007,'CHEVALIER','01/01/96',NULL);
INSERT INTO EMPLOYEES VALUES (9701,'LEROY','17/09/97',NULL);
INSERT INTO EMPLOYEES VALUES (9703,'LAMI','17/09/97',NULL);
INSERT INTO EMPLOYEES VALUES (9801,'SULTAN','20/03/98',NULL);
INSERT INTO EMPLOYEES VALUES (9802,'CLEMENCE','16/10/98',NULL);
INSERT INTO EMPLOYEES VALUES (9803,'CAVALIER','22/11/98',NULL);
INSERT INTO EMPLOYEES VALUES (9901,'ALEXANDRE','21/02/99',NULL);
```

2.  Formulation et test des requêtes.

*   Liste des employés

```sql
SELECT * FROM employees;
```

```bash
NUMEMP     NOMEMP               DATEEMB  DATESORT
---------- ------------------- -------- --------
  9007     CHEVALIER            01/01/96
  9701     LEROY                17/09/97
  9703     LAMI                 17/09/97
  9801     SULTAN               20/03/98
  9802     CLEMENCE             16/10/98
  9803     CAVALIER             22/11/98
  9901     ALEXANDRE            21/02/99
```

*   Liste des noms de tous les employés

```sql
SELECT NomEmp "Nom des employés" FROM employees;
```

```bash
Nom des employés
-------------------------
CHEVALIER
LEROY
LAMI
SULTAN
CLEMENCE
CAVALIER
ALEXANDRE
```
*   Liste des noms des employés embauchés à patir du 1er Janvier 1999

```sql
SELECT NomEmp "Nom des employés" FROM employees where DateEmb > '01/01/99';
```

```bash
Nom des employés
-------------------------
ALEXANDRE
```
*   Liste des employés (Num et Nom) dont le nom commence par la lettre C

```sql
SELECT concat(NumEmp,concat(' ', NomEmp))"Nom et Num des employees" from employees where SUBSTR(NomEmp,1,1) = 'C';
--
select numemp, nomemp from employes where nomemp like 'C%';
```

```bash
Nom et Num des employees
------------------------------------------------------------------
9007 CHEVALIER
9802 CLEMENCE
9803 CAVALIER
```
*   Liste des employés triés par ordre décroissant sur les noms

```sql
SELECT *  FROM employees ORDER BY NomEmp DESC;
```

```bash
NUMEMP      NOMEMP                   DATEEMB  DATESORT
---------- ------------------------- -------- --------
  9801     SULTAN                    20/03/98
  9701     LEROY                     17/09/97
  9703     LAMI                      17/09/97
  9802     CLEMENCE                  16/10/98
  9007     CHEVALIER                 01/01/96
  9803     CAVALIER                  22/11/98
  9901     ALEXANDRE                 21/02/99
```

*   Nombre d’employés embauchés chaque année

```sql
SELECT EXTRACT(YEAR FROM DateEmb)"Année", COUNT(*)"Nombre d'employés embauché" FROM employees GROUP BY EXTRACT(YEAR FROM DateEmb);
```

```bash
Année      Nombre d\'employés embauchés
---------- ----------------------------
  1997      2
  1999      1
  1996      1
  1998      3
```

*   Nombre d’employés embauchés chaque année ayant un nom de plus de 5
lettres

```sql
SELECT EXTRACT(YEAR FROM DateEmb)"Année", COUNT(*)"Nombre d'employés embauché" FROM employees WHERE LENGTH(NomEmp) > 5 GROUP BY EXTRACT(YEAR FROM DateEmb) ;
```

```bash
Année Nombre d\'employés embauché
---------- ----------------------------
  1999      1
  1996      1
  1998      3
```

*   Nombre d’employés embauchés chaque année ayant un nom commençant
par L ou C, en ne gardant que les années avec au moins deux employés

```sql
SELECT EXTRACT(YEAR FROM DateEmb)"Année", COUNT(*) "Nombre d'employés embauché" FROM employees WHERE SUBSTR(NomEmp,1,1) = 'L'OR SUBSTR(NomEmp,1,1)  = 'C'  GROUP BY EXTRACT(YEAR FROM DateEmb) HAVING COUNT(*)>1 ;

--

select extract(year from dateemb) "Annee" , count(*) from employes where (nomemp like 'L%' or nomemp like 'C%') group by extract(year from dateemb) having count(*) > 1 ;
```

```bash
Année       Nombre d\'employés embauché
---------- ----------------------------
  1997	      2
  1998	      2
```

### B. Tables postes ###

1.  Création de la table

```sql
create table Salaire (
  NumEmp  NUMBER(4),
  Poste  VARCHAR(25),
  Salaire NUMBER(4),
  NumServ VARCHAR(2),
  DateDeb DATE,
  DateFin DATE,
  CONSTRAINT nn_emp_Salaire CHECK (Salaire IS NOT NULL),
  CONSTRAINT nn_emp_poste CHECK (Poste IS NOT NULL),
  CONSTRAINT nn_emp_NumServ CHECK (SUBSTR(NumServ,1,1) = 'S' AND SUBSTR(NumServ,2,1) > 0),
  CONSTRAINT nn_emp_DateDeb CHECK (DateDeb IS NOT NULL)
);
```

2.  Ajout des valeurs dans la table

```sql
INSERT INTO SALAIRE VALUES (9701,'PRESIDENT',5800,'S2','17/09/97',NULL);
INSERT INTO SALAIRE VALUES (9703,'SECRETAIRE',950,'S1','17/09/97','31/12/98');
INSERT INTO SALAIRE VALUES (9703,'SECRETAIRE',1200,'S1','01/01/99',NULL);
INSERT INTO SALAIRE VALUES (9801,'DIRECTEUR',5300,'S1','07/07/97','31/12/98');
INSERT INTO SALAIRE VALUES (9801,'DIRECTEUR',3200,'S5','20/03/98',NULL);
INSERT INTO SALAIRE VALUES (9802,'DIRECTEUR',3500,'S2','16/10/98',NULL);
INSERT INTO SALAIRE VALUES (9803,'INGENIEUR',2600,'S4','22/11/98',NULL);
INSERT INTO SALAIRE VALUES (9901,'DIRECTEUR',3000,'S3','21/02/99',NULL);
```

*   Liste de tous les noms de postes

```sql
SELECT Poste FROM salaire;
```

```bash
POSTE
-------------------------
PRESIDENT
SECRETAIRE
SECRETAIRE
DIRECTEUR
DIRECTEUR
DIRECTEUR
INGENIEUR
DIRECTEUR
```

*   Postes occupés dont le salaire de l’employé est supérieur ou égal à 3000
SELECT poste FROM salaire WHERE Salaire >= 3000 AND DateFin IS NULL;


```bash
POSTE
-------------------------
PRESIDENT
DIRECTEUR
DIRECTEUR
DIRECTEUR
DIRECTEUR
```

*   Postes occupés, triés par ordre décroissant et salaires par ordre
croissant

```sql
SELECT Poste , Salaire FROM salaire WHERE DateFin IS NULL ORDER BY Poste DESC,Salaire ASC;
```

*   Resultat de la commande

```bash
POSTE			                SALAIRE
------------------------- ----------
SECRETAIRE                 1200
PRESIDENT                  5800
INGENIEUR                  2600
DIRECTEUR                  3000
DIRECTEUR                  3200
DIRECTEUR                  3500
```

*   Salaire le plus bas

```sql
SELECT MIN(Salaire) FROM salaire;
```

```bash
MIN(SALAIRE)
------------
 950
```

*   Moyenne des salaires

```sql
SELECT AVG(Salaire) FROM salaire;
```

```bash
AVG(SALAIRE)
------------
   3193,75
```

*   Moyenne des salaires pour les postes actuellement occupés

```sql
SELECT AVG(Salaire) FROM salaire WHERE DateFin IS NULL;
```

```bash
AVG(SALAIRE)
------------
3216,66667

```

*   Nombre de salariés avec un salaire > 3000

```sql
SELECT COUNT(* ) From salaire WHERE Salaire > 3000;
```



```bash
COUNT(* )
----------
4
```

*   Moyenne des salaires actuels pour chaque service

```sql
SELECT NumServ,AVG(Salaire)  FROM salaire WHERE DateFin IS NULL GROUP BY (NumServ);

--

select numserv, avg(salaire) from salaire group by numserv;

```



```bash
NU AVG(SALAIRE)
-- ------------
S4   2600
S3   3000
S5   3200
S2   4650
S1   1200
```

*   Moyenne des salaires pour chaque poste avec au moins 2 employés

```sql
SELECT Poste,AVG(Salaire)  FROM salaire GROUP BY (Poste) HAVING COUNT(Poste)>1 ;
```



```bash
POSTE                   AVG(SALAIRE)
----------------------- ------------
DIRECTEUR                    3750
SECRETAIRE                   1075
```

### C. Table etudiants ###

*   Création de la table etudiants

```sql
create table etudiants (
NUMERO  NUMBER(4)     NOT NULL,
NOM     VARCHAR2(25)  NOT NULL,
PRENOM  VARCHAR2(25)  NOT NULL,
SEXE    CHAR(1) CHECK(SEXE IN ('F', 'M')),
DATENAISSANCE   DATE    NOT NULL,
POIDS   NUMBER,
ANNEE   NUMBER,
CONSTRAINT PK_ETUDIANTS PRIMARY KEY (NUMERO)
);
```
Ajout des valeurs dans la table

```sql

INSERT INTO ETUDIANTS VALUES (71, 'Traifor', 'Benoit', 'M', '10/12/1978', 77, 1);
INSERT INTO ETUDIANTS VALUES (72, 'Génial', 'Clément', 'M', '10/04/1978', 72, 1);
INSERT INTO ETUDIANTS VALUES (73, 'Paris', 'Adam', 'M', '28/06/1974', 72, 2);
INSERT INTO ETUDIANTS VALUES (74, 'Parees', 'Clémence', 'F', '20/09/1977', 72, NULL);
INSERT INTO ETUDIANTS VALUES (69, 'Saitout', 'Inès', 'F', '22/11/1969', 69, 2);
INSERT INTO ETUDIANTS VALUES (55, 'Serafoub', 'Izouaf', 'M', '19/09/2013', 81, 1);
```

*   Moyenne des poids par sexe

```sql
SELECT SEXE,AVG(POIDS) FROM etudiants GROUP BY (SEXE);
```

```bash
S AVG(POIDS)
- ----------
M 75,5
F 70,5
```

*   Moyenne des poids par sexe et par tranche d’âge

```sql
SELECT EXTRACT(YEAR FROM DATENAISSANCE),SEXE,AVG(POIDS) FROM etudiants GROUP BY (SEXE,EXTRACT(YEAR FROM DATENAISSANCE));
```

```bash
Année de naissance  S AVG(POIDS)
------------------- - ----------
       1974       M   72
       1978       M   74,5
       1977       F   72
       1969       F   69
       2013       M   81
```

*   Moyenne des poids par année, par sexe et par tranche d’âge

```sql
SELECT ANNEE,SEXE,EXTRACT(YEAR FROM DATENAISSANCE),AVG(POIDS) FROM etudiants GROUP BY (ANNEE,SEXE,EXTRACT(YEAR FROM DATENAISSANCE));
```

```bash
ANNEE S EXTRACT(YEARFROMDATENAISSANCE) AVG(POIDS)
---------- - ------------------------ ----------
1 M            2013                     81
1 M            1978                     74,5
2 F            1969                     69
2 M            1974                     72
  F            1977                     72
```

*   Moyenne des poids par sexe, par année et par tranche d’âge

```sql
SELECT SEXE,ANNEE,EXTRACT(YEAR FROM DATENAISSANCE),AVG(POIDS) FROM etudiants GROUP BY (SEXE,ANNEE,EXTRACT(YEAR FROM DATENAISSANCE));
```

```bash
AS  ANNEE    EXTRACT(YEARFROMDATENAISSANCE) AVG(POIDS)
- ---------- ------------------------------ ----------
M    2             1974                        72
M    1             2013                        81
F                  1977                        72
M    1             1978                      74,5
F    2             1969                        69
```
