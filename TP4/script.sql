PROMPT ## TP 4 : SQL Simple, tri et regroupements ##
PROMPT ### A. Table employés ###

PROMPT 1.  Creation de la table

drop table employees cascade constraint;

create table employees (
  NumEmp  NUMBER(4),
  NomEmp  VARCHAR(25),
  DateEmb DATE,
  DateSortie DATE,
  CONSTRAINT pk_employes PRIMARY KEY (NumEmp),
  CONSTRAINT nn_emp_nomEMP CHECK (NomEmp IS NOT NULL),
  CONSTRAINT nn_emp_DateEmb CHECK (DateEmb IS NOT NULL)
);

PROMPT
pause press enter
PROMPT

PROMPT On ajoute des valeurs dans la table employees.
PROMPT

INSERT INTO EMPLOYEES VALUES (9007,'CHEVALIER','01/01/96',NULL);
INSERT INTO EMPLOYEES VALUES (9701,'LEROY','17/09/97',NULL);
INSERT INTO EMPLOYEES VALUES (9703,'LAMI','17/09/97',NULL);
INSERT INTO EMPLOYEES VALUES (9801,'SULTAN','20/03/98',NULL);
INSERT INTO EMPLOYEES VALUES (9802,'CLEMENCE','16/10/98',NULL);
INSERT INTO EMPLOYEES VALUES (9803,'CAVALIER','22/11/98',NULL);
INSERT INTO EMPLOYEES VALUES (9901,'ALEXANDRE','21/02/99',NULL);

PROMPT
pause press enter
PROMPT

PROMPT 2.  Formulation et test des requêtes.
PROMPT

PROMPT   Liste des employés
PROMPT
SELECT * FROM employees;

PROMPT
pause press enter
PROMPT

PROMPT  Liste des noms de tous les employés
PROMPT

SELECT NomEmp "Nom des employés" FROM employees;

PROMPT
pause press enter
PROMPT

PROMPT   Liste des noms des employés embauchés à patir du 1er Janvier 1999
PROMPT

SELECT NomEmp "Nom des employés" FROM employees where DateEmb > '01/01/99';

PROMPT
pause press enter
PROMPT

PROMPT Liste des employés (Num et Nom) dont le nom commence par la lettre C
PROMPT

SELECT concat(NumEmp,concat(' ', NomEmp))"Nom et Num des employees" from employees where SUBSTR(NomEmp,1,1) = 'C';

PROMPT
pause press enter
PROMPT

PROMPT   Liste des employés triés par ordre décroissant sur les noms
PROMPT

SELECT *  FROM employees ORDER BY NomEmp DESC;

PROMPT
pause press enter
PROMPT

PROMPT Nombre d’employés embauchés chaque année
PROMPT

SELECT EXTRACT(YEAR FROM DateEmb)"Année", COUNT(*)"Nombre d'employés embauché" FROM employees GROUP BY EXTRACT(YEAR FROM DateEmb);

PROMPT
pause press enter
PROMPT

PROMPT  Nombre d’employés embauchés chaque année ayant un nom de plus de 5 lettres
PROMPT

SELECT EXTRACT(YEAR FROM DateEmb)"Année", COUNT(*)"Nombre d'employés embauché" FROM employees WHERE LENGTH(NomEmp) > 5 GROUP BY EXTRACT(YEAR FROM DateEmb) ;

PROMPT
pause press enter
PROMPT

PROMPT   Nombre d’employés embauchés chaque année ayant un nom commençant par L ou C, en ne gardant que les années avec au moins deux employés
PROMPT

SELECT EXTRACT(YEAR FROM DateEmb)"Année", COUNT(*) "Nombre d'employés embauché" FROM employees WHERE SUBSTR(NomEmp,1,1) = 'L'OR SUBSTR(NomEmp,1,1)  = 'C'  GROUP BY EXTRACT(YEAR FROM DateEmb) HAVING COUNT(*)>1 ;

PROMPT
pause press enter
PROMPT

PROMPT ### B. Tables postes ###

PROMPT 1.  Création de la table
PROMPT

DROP Table SALAIRE CASCADE constraint;

create table Salaire (
  NumEmp  NUMBER(4),
  Poste  VARCHAR(25),
  Salaire NUMBER(4),
  NumServ VARCHAR(2),
  DateDeb DATE,
  DateFin DATE,
  CONSTRAINT nn_emp_Salaire CHECK (Salaire IS NOT NULL),
  CONSTRAINT nn_emp_poste CHECK (Poste IS NOT NULL),
  CONSTRAINT nn_emp_NumServ CHECK (SUBSTR(NumServ,1,1) = 'S' AND SUBSTR(NumServ,2,1) > 0),
  CONSTRAINT nn_emp_DateDeb CHECK (DateDeb IS NOT NULL)
);

PROMPT
pause press enter
PROMPT

PROMPT 2.  Ajout des valeurs dans la table
PROMPT

INSERT INTO SALAIRE VALUES (9701,'PRESIDENT',5800,'S2','17/09/97',NULL);
INSERT INTO SALAIRE VALUES (9703,'SECRETAIRE',950,'S1','17/09/97','31/12/98');
INSERT INTO SALAIRE VALUES (9703,'SECRETAIRE',1200,'S1','01/01/99',NULL);
INSERT INTO SALAIRE VALUES (9801,'DIRECTEUR',5300,'S1','07/07/97','31/12/98');
INSERT INTO SALAIRE VALUES (9801,'DIRECTEUR',3200,'S5','20/03/98',NULL);
INSERT INTO SALAIRE VALUES (9802,'DIRECTEUR',3500,'S2','16/10/98',NULL);
INSERT INTO SALAIRE VALUES (9803,'INGENIEUR',2600,'S4','22/11/98',NULL);
INSERT INTO SALAIRE VALUES (9901,'DIRECTEUR',3000,'S3','21/02/99',NULL);

PROMPT
pause press enter
PROMPT

PROMPT *  Liste de tous les noms de postes
PROMPT

SELECT Poste FROM salaire;

PROMPT
pause press enter
PROMPT

PROMPT   Postes occupés dont le salaire de l’employé est supérieur ou égal à 3000
PROMPT

SELECT poste FROM salaire WHERE Salaire >= 3000 AND DateFin IS NULL;

PROMPT
pause press enter
PROMPT

PROMPT Postes occupés, triés par ordre décroissant et salaires par ordre
croissant
PROMPT

SELECT Poste , Salaire FROM salaire WHERE DateFin IS NULL ORDER BY Poste DESC,Salaire ASC;

PROMPT
pause press enter
PROMPT

PROMPT Salaire le plus bas
PROMPT

SELECT MIN(Salaire) FROM salaire;

PROMPT
pause press enter
PROMPT

PROMPT   Moyenne des salaires
PROMPT
SELECT AVG(Salaire) FROM salaire;

PROMPT
pause press enter
PROMPT

PROMPT   Moyenne des salaires pour les postes actuellement occupés
PROMPT

SELECT AVG(Salaire) FROM salaire WHERE DateFin IS NULL;

PROMPT
pause press enter
PROMPT

PROMPT   Nombre de salariés avec un salaire > 3000
PROMPT

SELECT COUNT(* ) From salaire WHERE Salaire > 3000;

PROMPT
pause press enter
PROMPT

PROMPT Moyenne des salaires actuels pour chaque service
PROMPT

SELECT NumServ,AVG(Salaire)  FROM salaire WHERE DateFin IS NULL GROUP BY (NumServ);

PROMPT
pause press enter
PROMPT

PROMPT   Moyenne des salaires pour chaque poste avec au moins 2 employés
PROMPT

SELECT Poste,AVG(Salaire)  FROM salaire GROUP BY (Poste) HAVING COUNT(Poste)>1 ;

PROMPT
pause press enter
PROMPT

PROMPT ### C. Table etudiants ###
PROMPT
PROMPT   Création de la table etudiants
PROMPT

DROP TABLE etudiants CASCADE CONSTRAINT;

create table etudiants (
  NUMERO  NUMBER(4)     NOT NULL,
  NOM     VARCHAR2(25)  NOT NULL,
  PRENOM  VARCHAR2(25)  NOT NULL,
  SEXE    CHAR(1) CHECK(SEXE IN ('F', 'M')),
  DATENAISSANCE   DATE    NOT NULL,
  POIDS   NUMBER,
  ANNEE   NUMBER,
  CONSTRAINT PK_ETUDIANTS PRIMARY KEY (NUMERO)
);

PROMPT
pause press enter
PROMPT

PROMPT Ajout des valeurs dans la table
PROMPT

INSERT INTO ETUDIANTS VALUES (71, 'Traifor', 'Benoit', 'M', '10/12/1978', 77, 1);
INSERT INTO ETUDIANTS VALUES (72, 'Génial', 'Clément', 'M', '10/04/1978', 72, 1);
INSERT INTO ETUDIANTS VALUES (73, 'Paris', 'Adam', 'M', '28/06/1974', 72, 2);
INSERT INTO ETUDIANTS VALUES (74, 'Parees', 'Clémence', 'F', '20/09/1977', 72, NULL);
INSERT INTO ETUDIANTS VALUES (69, 'Saitout', 'Inès', 'F', '22/11/1969', 69, 2);
INSERT INTO ETUDIANTS VALUES (55, 'Serafoub', 'Izouaf', 'M', '19/09/2013', 81, 1);

PROMPT
pause press enter
PROMPT

PROMPT   Moyenne des poids par sexe
PROMPT

SELECT SEXE,AVG(POIDS) FROM etudiants GROUP BY (SEXE);

PROMPT
pause press enter
PROMPT

PROMPT   Moyenne des poids par sexe et par tranche d’âge
PROMPT
SELECT EXTRACT(YEAR FROM DATENAISSANCE),SEXE,AVG(POIDS) FROM etudiants GROUP BY (SEXE,EXTRACT(YEAR FROM DATENAISSANCE));

PROMPT
pause press enter
PROMPT

PROMPT  Moyenne des poids par année, par sexe et par tranche  d'age
PROMPT

SELECT ANNEE,SEXE,EXTRACT(YEAR FROM DATENAISSANCE),AVG(POIDS) FROM etudiants GROUP BY (ANNEE,SEXE,EXTRACT(YEAR FROM DATENAISSANCE));


PROMPT
pause press enter
PROMPT

PROMPT  Moyenne des poids par sexe, par année et par tranche d’âge
PROMPT
SELECT SEXE,ANNEE,EXTRACT(YEAR FROM DATENAISSANCE),AVG(POIDS) FROM etudiants GROUP BY (SEXE,ANNEE,EXTRACT(YEAR FROM DATENAISSANCE));
