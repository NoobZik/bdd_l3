-- Noms des clients

select NomCli from Clients;

-- Noms des clients sans les doublons

select distinct NomCli from client;

-- Les clients de paris

select CiviCli, NomCli, PrenCli, VillCli from clients where VillCli = 'Paris';

-- Noms et prénoms des clients féminins

select NomCli, PrenCli from clients where CiviCli != 'Monsieur';

-- Nom des articles dont le prix de vente est supérieur ou égale au double du prix d'achat

select nomArt from articles where (PVArt / 2) >= PAArt;

-- Les numéro de commandes du mois de septembre 2011

select NumeCom from Commandes where (DateCom >= '01/09/2011' and DateCom <= '30/09/2011');
--ou
select NumeCom from Commandes where (Extract(MONTH from Datecom) = 09 AND Extract(YEAR from Datecom) = 2011);

-- Les références et noms des articles du fournisseur WD

select RefeArt, nomArt from articles where RefeArt like ('WD%');

-- Les noms des articles dont le prix de vente est soit 4, soit 27 soit 35

select nomArt from articles where PVArt = 27 or PVArt = 4 or PVArt = 35;

-- Les noms des articles dont le prix de vente est compris entre 30 et 50

select nomArt from article where PVArt >= 30 and PVArt <= 50;

-- Classez les clients par ville, par ordre croissant

select VillCli, NomCli from clients order by (VillCli) asc;

-- Classez les articles de PV < 20, dans l'ordre décroissants du stock

select nomArt from articles where PVArt < 20 order by (QStkArt) desc;

-- Calculer la marge pour chaque article

select nomArt, (PVArt - PAArt) "Marge" from articles;

-- Calculez la marge pour tout les sieges

select nomArt, (PVArt - PAArt) "Marge" from articles where nomArt like ('SIEGE%');

-- Classer la marge pour tous les sièges dans l'ordre décroissants de la marge

select nomArt, (PVArt - PAArt) "Marge" from articles where nomArt like ('SIEGE%') order by (PVArt - PAArt) desc;

-- Calculer le nombre de client

select count(CodeCli) from clients;

-- Prix de vente le plus élevé parmi les articles

SELECT nomArt,MAX(PVart) FROM articles;

-- Moyenne des prix de vente des articles

select avg(PVArt) from articles;

-- Nombre de client par catégorie

select CateCli, count(*) group by (CateCli);

-- Nombre de cliens parisiens par catégorie

select CateCli, count(*) where VillCli like ('Paris') group by (CateCli);

-- Nombre de cliens parisiens par catégorie seulement > 2

select CateCli, count(*) where (VillCli like ('Paris') and CateCli > 2) group by (CateCli);
--je ne crois pas que ce soit ça qu'il demande je pense plutôt que c'est ça 
select CateCli, count(*) where VillCli like ('Paris') group by (CateCli) HAVING (count(*) > 2);
