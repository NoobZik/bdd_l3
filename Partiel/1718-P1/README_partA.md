## Question 1 ##

Les clés primaires et étranges de la table DETAILCOM sont :
*   NumeCom (PK qui est aussi FK de Commandes)
*   RefeArt (FK)

## Question 2 ##

Rappel des définition de forme normales
On dit qu'il existe une dépendance fonctionnelle entre deux attributs X et Y si la connaissance de la valeur de X détermine celle de Y

### Forme normale 1 ###
*   chaque attribut non clé dépend de la clé
*   Les attribut contiennent des valeurs unitaires

### Forme normale 2 ###
*   Est en 1FN
*   Aucun attribut hors clé ne dépend que d’une partie de la clé

### Forme normale 3 ###
*   Est en 2FN
*   Tout attribut non clé ne dépend pas d'un autre attribut non clé

Analyse :
*   La quantité commandé ne dépent pas de l'article
*   Remise ne dépend de rien
*   PUArt Dépend de la quantité, du prix de l'article PVAArt de la remise appliqué

On en déduit que PUArt est dépendante d'une partie des clées donc FN2.

```sql
create table clients(
  CodeCli varchar(10),
  AdreCli varchar(50),
  CateCli number(1),
  CiviCli varchar(12),
  CPCli varchar(10),
  NomCli varchar(20),
  PrenCli varchar(20),
  VillCli varchar(20),
  constraint pk_client primary key (CodeCli),
  constraint ck_client_catecli check (CateCli > 0 and CateCli < 8),
  constraint ck_client_civilcli check (col in ('Mademoiselle', 'Madame', 'Monsieur')),
  constraint ck_Client_VillCli check (VillCli = upper(VillCli))
);
```
