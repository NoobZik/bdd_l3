create table usine (
  NU number(5),
  NomU varchar(25),
  Ville varchar(25),
  constraint pk_usine primary key (NU),
  constraint nn_usine_NomU check (NomU is not null),
  constraint nn_usineVille check (ville is not null)
);

create table produit (
  NP number(5),
  NomP varchar(25),
  couleur varchar(25),
  poids decimal (5,3),
  constraint PK_produit primary key (NP),
  constraint UQ_produit unique (NomP, couleur),
  constraint ck_produit_couleur check (couleur=lower(couleur) and couleur is not null),
  constraint ck_produit_poids check (poids  >= 0 and length(Substr(poids,INSTR(poids, '.' ) + 1, length(poids) ))<=3);
  constraint ck_produit_dc_np_p check (substr(couleur, 1, 1) != substr(NomP,1,1))
);

create table fournisseur (
  NF number(5),
  NomF varchar(25),
  status varchar(25),
  ville varchar(25),
  constraint pk_fournisseur primary key (NF),
  constraint ck_fournisseur_statu check (status is not null),
  constraint ck_fournisseur_ville check (ville is not null)
);

create table livraison (
  NP number(5),
  NU NUMBER(5),
  NF NUMBER(5),
  quantite number(5),
  constraint fk_livraison_NP FOREIGN KEY (NP) REFERENCES produit(NP),
  constraint fk_livraison_NU FOREIGN KEY (NU) REFERENCES usine(NU),
  constraint fk_livraison_NF FOREIGN KEY (NF) REFERENCES fournisseur(NF)
);
