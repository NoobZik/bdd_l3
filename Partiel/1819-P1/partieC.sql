-- Donner le numéro, le nom et la ville de toutes les usines

select * from usine;

-- Donner le numéro, le nom et la ville de toutes les usines de Lyon.

select * from usine where Ville = 'Lyon';

-- Donnez le nom et la couleur en une colonne, le nom entre parenthèse, ex :"Siège (vert)".

select (concat(NomP, concat('(', concat(couleur,')')))) "Nom (Couleur)" from produit;

-- Donnez les numérosde produit, du moins lourds au plus lourd

select NP, Poids from produit order by poids asc;

-- Donnez le nom et la couleur des produits de plus de 10g, triéspar poids décroissants.

select (concat(NomP, concat(' ', couleur))) "Nom Couleur", Poids from produit where poids > 0.10 order by poids desc;

-- Donnez les noms des fournisseurs, ville par ville.
/* A voir car je sais pas...*/

select distinct ville, nomf from fournisseur;

-- Donnez le nombre d'usine par ville.

select ville, count(*) from usine group by ville;

-- Donnez les villes ayant plus de 5 usines.

select ville, count(*) from usine group by ville having count(*) > 5;

--  Donnez les villesayant plus de 5 producteurs

select ville, count(statut) from fournisseur where statut = 'producteur' group by ville having count(NomU) > 5;

-- Donnez la moyenne des poids par produits et par couleur, sauf les produits verts.

select nomp, couleur, avg(poids) from produit where (couleur != 'vert') group by nomp, couleur;

-- Donnez lenombre de produitslivréspar chaquefournisseur, du plus petit au plus grand

select nf, sum(quantite) from livraison group by nf order by sum(quantite) asc;

-- Donnez le nombre d'usinesdont le nom commencepar "C"pour chaque ville

select ville, count(nomu) from usine where SUBSTR(NOMU,1,1) = 'C' group by ville;
