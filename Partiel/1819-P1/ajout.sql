insert into fournisseur values (1, 'Monroe', 'producteur', 'Lyon');
insert into fournisseur values (2, 'Au bon siege', 'Sous-traitant', 'Limoges');
insert into fournisseur values (3, 'Saint Gobain', 'producteur', 'Paris');

insert into produit values (1, 'Plaquette', 'noir', '0.257');
insert into produit values (2, 'Siège', 'rouge', '15.230');
insert into produit values (3, 'Siège', 'vert', '15.230');

insert into usine values (1, 'Citroen', 'Paris');
insert into usine values (2, 'Monroe', 'Socheaux');
insert into usine values (3, 'Citroen', 'Socheaux');
insert into usine values (4, 'Renault', 'Paris');

insert into livraison values (3, 1, 2, 60);
insert into livraison values (1, 2, 3, 2500);
insert into livraison values (1, 3, 3, 3000);
insert into livraison values (2, 2, 3, 120);
insert into livraison values (3, 1, 1, 49);
insert into livraison values (3, 2, 1, 45);
insert into livraison values (3, 3, 1, 78);
