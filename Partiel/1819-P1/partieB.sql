-- Changer la ville du fournisseur N1

update fournisseur set Ville = 'Lyon' where NF = 1;

-- Ajout d'un fournisseur

insert into fournisseur values (45, 'Bob', 'sous-traitant', 'Lille');
insert into fournisseur values (46, 'Julien', NULL, 'Paris');

-- Supprimer la contrainte clée primaire de fournisseur

alter table fournisseur drop constraint pk_fournisseur;

-- Ajout d'une contrainte Ville non null

alter table fournisseur add constraint ck_fournisseur_ville check (ville is not null);

-- Desactivation de la contrainte

alter table fournisseur enable constraint ck_fournisseur_ville;

-- Suppression

delete from fournisseur where ((ville = 'Lyon' or ville = 'Renne' or ville = 'Lille') and (substr(NomF,1,3) = 'St.' or substr(NomF,1,5) = 'Saint'));
