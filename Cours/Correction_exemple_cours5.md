1.

```sql
select * from centre natural join propose;
```

2.

```sql
Select centre.numc, centre.nomc, centre.vilc
from centre natural join propose;
where propose.sport = 'karting';
```

3.

```sql
select numc, nomc, vilc from centre where numc in (select numc from propose where sport ="karting");

select numc, nomc, vilc from centre where numc any (select numc from propose where sport ="karting");

select numc, nomc, vilc from centre where exists (select * from propose where sport ="karting" and centre.numc = propose.numc);
```
