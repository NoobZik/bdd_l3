## TP 6 : SQL requetes avances ##

### **B** Formulez et testez les requêtes suivantes en SQL en proposant au moins deux solutions différentes pour chaque question **si possible :** ###

1.  Donner la liste des noms, des prénoms et des dates de naissance de tous
les élèves.

```sql
select nom, prenom, date_naissance from eleves;
--ou
select concat(nom,concat(' ',concat(prenom,concat(' ', date_naissance)))) "Nom Prenom date de naissance" from eleves;
```

```bash
NOM        PRENOM      DATE_NAISS
---------- ----------- ----------
Brisefer   Benoit      10-12-1978
Génial     Olivier     10-04-1978
Jourdan    Gil         28-06-1974
Spring     Jerry       16-02-1974
Tsuno      Yoko        29-10-1977
Lebut      Marc        29-04-1974
Lagaffe    Gaston      08-04-1975
Dubois     Robin       20-04-1976
Walthéry   Natacha     07-09-1977
Danny      Buck        15-02-1973

```

2.  Donner tous les renseingements sur toutes les activités

```sql
select * from activites;
-- ou
select niveau, nom, equipe from activites;
```

```bash
NIVEAU     NOM                  EQUIPE
---------- -------------------- ---------------------
1          Mini foot            Amc Indus
1          Surf                 Les planchistes ...
2          Tennis               Ace Club
3          Tennis               Ace Club
1          Volley ball          Avs80
2          Mini foot            Les as du ballon
2          Volley ball          smash
```
3.  Lister les spécialités des professeurs

```sql
select nom, specialite from professeurs;
--ou
select nom, specialite from professeurs order by nom desc;
```

```bash
NOM           SPECIALITE
------------- --------------------
Bottle        poésie
Bolenov       réseau
Tonilaclasse  poo
Pastecnov     sql
Selector      sql
Vilplusplus   poo
Francesca
Pucette       sql
```

4.  Obtenir le nom et prénom des élèves pesant moins de 45 kilos et inscrits
en 1 ère année ou des élèves inscrits en 2 ème année.

```sql
select nom, prenom from eleves where poids < 45 AND annee = 1 OR annee = 2;
--ou
select nom, prenom from eleves where (poids between 0 AND 45 and annee = 1 ) OR annee = 2;
```

```bash
NOM                       PRENOM
------------------------- -------------------------
Brisefer                  Benoit
Génial                    Olivier
Jourdan                   Gil
Spring                    Jerry
Lebut                     Marc
Dubois                    Robin
Danny                     Buck
```   

5.  Obtenir le nom des élèves dont le poids est compris entre 60 et 80 kilos.

```sql
select nom, poids from eleves where poids <= 80 and poids >= 60;
--ou
select nom, poids from eleves where poids between 60 and 80;
```

```bash
NOM                       POIDS
------------------------- ----------
Jourdan                   72
Spring                    78
Lebut                     75
Lagaffe                   61
Dubois                    60
```

6.  Obtenir le nom des professeurs dont la spécialité est ‘poésie’ ou SQL.

```sql
select nom from professeurs where specialite in ('poésie','sql');
--ou
select nom from professeurs where specialite = 'poésie' or specialite = 'sql';

```

```bash
NOM
-------------------------
Bottle
Pastecnov
Selector
Pucette
```

7.  Obtenir le nom des élèves dont le nom commence par ‘L’.

```sql
select nom from eleves where nom like 'L%';
--ou
select nom from eleves where substr(nom, 1, 1) = 'L';
```

```bash
NOM
-------------------------
Lebut
Lagaffe
```

8.  Obtenir le nom des professeurs dont la spécialité est inconnue.

```sql
select nom from professeurs where specialite is NULL;
--ou
select nom from professeurs having count(specialite) < 1 group by nom;
```

```bash
NOM
-------------------------
Francesca
```

9.  Obtenir le nom et prénom des élèves pesant moins de 45 kilos et inscrits
en 1 ère année.

```sql
select nom, prenom from eleves where poids < 45 AND annee = 1;
-- on suppose les valeurs de comparaison non inclusive.
select nom, prenom from eleves where (poids between 0 and 44) AND annee = 1;
```

```bash
NOM                       PRENOM
------------------------- -------------------------
Brisefer                  Benoit
Génial                    Olivier
```

10. Obtenir, pour chaque professeur, son nom et sa spécialité. Si cette der-
nière est inconnue, on souhaite afficher la chaîne de caractères : ‘****’.

```sql
select nom, coalesce(specialite, '****') AS specialite from professeurs;
--ou
SELECT nom, NVL(specialite, '****') AS specialite FROM professeurs;

```

```bash
NOM            SPECIALITE
-------------- --------------------
Bottle         poésie
Bolenov        réseau
Tonilaclasse   poo
Pastecnov      sql
Selector       sql
Vilplusplus    poo
Francesca      ****
Pucette        sql
```

11. Quels sont les noms et prénoms des élèves qui pratiquent du surf au ni-
veau 1. Rédigez cette requête de cinq façons différentes.

```sql
select t1.nom, t1.prenom from eleves t1 join ACTIVITES_PRATIQUEES t2 on t2.num_eleve = t1.num_eleve where t2.nom = 'Surf' and t2.niveau = 1;
--ou
select nom, prenom from eleves where num_eleve in (select num_eleve from activites_pratiquees where nom = 'Surf' and niveau = 1);
--ou
select t1.nom, t1.prenom from eleves t1 where exists (select * from activites_pratiquees t2 where t1.num_eleve = t2.num_eleve and t2.nom = 'Surf' and t2.niveau = 1);
--ou
SELECT NOM,PRENOM FROM ELEVES WHERE NUM_ELEVE IN (SELECT NUM_ELEVE
FROM ACTIVITES_PRATIQUEES WHERE NOM != ALL(SELECT DISTINCT NOM FROM
ACTIVITES_PRATIQUEES WHERE NOM != 'Surf') AND NIVEAU = 1);
--ou
SELECT NOM, PRENOM FROM ELEVES WHERE NUM_ELEVE IN
(SELECT NUM_ELEVE FROM ACTIVITES_PRATIQUEES WHERE NOM = 'Surf'
INTERSECT SELECT NUM_ELEVE FROM ACTIVITES_PRATIQUEES WHERE NIVEAU = 1);
```

```bash
NOM        PRENOM
---------- -------------------------
Brisefer   Benoit
Spring     Jerry
Tsuno      Yoko
```

12. Obtenir le nom des élèves de l’équipe AMC INDUS.

```sql
select t1.nom from eleves t1 join ACTIVITES_PRATIQUEES t2 on t1.num_eleve = t2.num_eleve join activites t3 on t3.niveau = t2.niveau and t3.nom = t2.nom where equipe = 'Amc Indus';
--ou
select nom from eleves where num_eleve in (select num_eleve from activites_pratiquees where concat(niveau,nom) in (select concat(niveau, nom) from activites where equipe = 'Amc Indus'));
```

```bash
NOM
-------------------------
Brisefer
Génial
Tsuno
Dubois
Walthéry
Danny
```

13. Obtenir les pairs de noms de professeurs qui ont la même spécialité.

```sql
select t1.nom,t2.nom,t1.specialite from professeurs t1, professeurs t2 where t1.specialite = t2.specialite and t1.nom > t2.nom;
--ou
select t1.nom,t2.nom,t1.specialite from professeurs t1 join professeurs t2 on t1.specialite = t2.specialite where t1.nom > t2.nom;
```

```bash
NOM         NOM          SPECIALITE
----------  ------------ --------------------
Vilplusplus Tonilaclasse poo
Pucette     Pastecnov    sql
Selector    Pastecnov    sql
Selector    Pucette      sql
```

14. Pour chaque spécialité sql/SQL, on demande d’obtenir son nom son salaire mensuel actuel et son augmentation mensuelle depuis son salaire
de base.

```sql
select nom, specialite, ((Salaire_actuel - Salaire_base) / MONTHS_BETWEEN(Der_prom, Date_entree) ) "augmentation mensuelle" from professeurs where specialite = 'sql';
```

```bash
NOM        SPECIALITE     augmentation mensuelle
---------  -------------  --------------------
Pastecnov   sql
Selector    sql           0
Pucette     sql           5764,22462
```

15. Obtenir le nom des professeurs dont l’augmentation relative au salaire de
base dépasse 25%.

```sql
select nom from professeurs where (Salaire_actuel - Salaire_base) > (Salaire_base * 25 / 100);
```

```bash
NOM
-------------------------
Bottle
Bolenov
Francesca
```

16. Afficher les points de Tsuno obtenus dans chaque cours sur 100 plutôt
que sur 20.

```sql
select (points * 5) "Points sur 100" from resultats where Num_eleve in(select Num_eleve from eleves where nom = 'Tsuno');
--ou
select (points * 5) "Points sur 100" from resultats where Num_eleve =(select Num_eleve from eleves where nom = 'Tsuno');
--Attention le = ne marche que lorsqu'on obtient que 1 num_select dans le 2eme select

```

```bash
Points sur 100
--------------
25
32,5
65
65
```

17. Obtenir le poids moyen des élèves de 1 ère année.

```sql
select avg(poids) "poids moyen" from eleves where annee = 1;
--ou
select sum(poids)/count(*) "poids moyen" from eleves where annee = 1;
```

```bash
poids moyen
-----------
       48,4
```

18. Obtenir le total des points de l’élève numéro 3.

```sql
select sum(points) from RESULTATS where Num_eleve = 3;
```

```bash
SUM(POINTS)
-----------
   65
```

19. Obtenir la plus petite et la plus grande cote de l’élève Brisefer.

```sql
select min(points),max(points) from RESULTATS where Num_eleve in (select Num_eleve from eleves where nom = 'Brisefer');
--ou
select min(points),max(points) from RESULTATS natural join eleves where nom = 'Brisefer';
```

```bash
MIN(POINTS) MAX(POINTS)
----------- -----------
    8       20
```

20. Obtenir le nombre d’élèves inscrits en deuxième année.

```sql
select count(*) from eleves where annee = 2;
```

```bash
COUNT(*)
----------
 5
```

21. Quel est l'augmentation mensuelle moyenne des salaires des professeurs de SQL?

```sql
select avg((Salaire_actuel - Salaire_base)/months_between(der_prom, date_entree)) "Augmentation mensuelle moyenne" from professeurs where specialite = 'sql';
```

```bash
Augmentation mensuelle moyenne
------------------------------
2882,11231
```

22. Obtenir l'année de la dernière promotion du professeur pucette.

```sql
select EXTRACT(YEAR from der_prom) "Année" from professeurs where nom like 'Pucette';
--ou
select substr(der_prom,7,4) "Année" from professeurs where nom like 'Pucette';
```

```bash
Année
----------
1996
```

23. Pour chaque professuer, afficher sa date d'embauche, sa date de dernière promotion ansi que le nombre d'années écoulé entre ces deux dates.

```sql
select nom, date_entree, der_prom, months_between(der_prom, date_entree)/12 AS DateDiff from professeurs;
```

```bash
NOM                 DATE_ENT DER_PROM DATEDIFF
------------------- -------- -------- ----------
Bottle              01/10/70 01/10/88 18
Bolenov             15/11/68 01/10/98 29,8790323
Tonilaclasse        01/10/79 01/01/89 9,25
Pastecnov           01/10/75
Selector            15/10/82 01/10/88 5,96236559
Vilplusplus         25/04/90 05/06/94 4,11290323
Francesca           01/10/75 11/01/98 22,2768817
Pucette             06/12/88 29/02/96 7,22849462

```

24. Afficher l'age moyen des élèves. Cet âge sera exprimée en année.

```SQL
select avg(months_between(sysdate, date_naissance)/12) "age moyen" from eleves;
--ou
select sum(months_between(sysdate, date_naissance)/12)/count(*) "age moyen" from eleves;
```

```bash
age moyen
----------
43,8373711
```

25. Affiche le nom des professeurs pour lesquels il s'est écoulé plus de 50 mois entre l'embauche et la première promotion

```sql
select nom from professeurs where months_between(der_prom, date_entree) > 50;
```

```bash
NOM
-------------------------
Bottle
Bolenov
Tonilaclasse
Selector
Francesca
Pucette

6 lignes s�lectionn�es.

```
26. Obtenir la liste des élèves qui auront au moins 24 dans moins de 4 mois.

```sql
select nom from eleves where months_between(sysdate, date_naissance)+4/12 > 24;
```

```bash
NOM
-------------------------
Brisefer
Génial
Jourdan
Spring
Tsuno
Lebut
Lagaffe
Dubois
Walthéry
Danny

10 lignes s�lectionn�es.
```

27. Obtenir une liste des élèves classé par année et par ordre alphabétique

```sql
select nom, annee from eleves order by annee, nom ;
```

```bash
NOM             ANNEE
--------------- ----------
Brisefer        1
Génial          1
Lagaffe         1
Tsuno           1
Walthéry        1
Danny           2
Dubois          2
Jourdan         2
Lebut           2
Spring          2

10 lignes s�lectionn�es.
```

28. Afficher en ordre decroissant les points de Tsuno obtenus dans chaque cours sur 100 plutot que sur 20.

```sql
select num_cours, points*5 from resultats where num_eleve = (select num_eleve from eleves where nom like('Tsuno')) order by 2 desc;
--ou
select num_cours, points*5 from resultats natural join eleves where nom = 'Tsuno' order by 2 desc;
```

```bash
NUM_COURS  POINTS*5
---------- ----------
4          65
5          65
2          32,5
1          25
```

29. Obtenir pour chaque élève de 1 ère année son nom et sa moyenne.

```sql
select nom, avg(points) from resultats natural join eleves group by nom;
```

```bash
NOM        AVG(POINTS)
---------- -----------
Jourdan    16,25
Spring     12,375
Dubois     8,375
Walthéry   15,875
Danny      7,875
Tsuno      9,375
Génial     9,5
Lebut      9,875
Brisefer   13,375
Lagaffe    10,375
```

30. Obtenir la moyenne des points de chaque élève de 1 ère année dont le to-
tal des points est supérieur à 40.

```sql
select num_eleve, avg(points) from resultats having sum(points) > 40 group by num_eleve;
```

```bash
NUM_ELEVE  AVG(POINTS)
---------- -----------
  1        13,375
  4        12,375
  3        16,25
  7        10,375
  9        15,875
```

31. Obtenir le maximum parmi les totaux de chaque élève.

```sql
select max(sum(points)) "Max des totaux" from resultats group by num_eleve;
```

```bash
Max des totaux
--------------
     65
```

32. Obtenir le nom des élèves qui jouent dans l'équipe AMC INDUS.

Voir question 12

33. Quels sont les élèves de 1 ère année dont la moyenne est supérieure à la
moyenne de la 1 ère année ?

```sql
select num_eleve, avg(points) from resultats natural join eleves where annee = 1 having avg(points) > (select avg(points) from resultats natural join eleves where annee = 1) group by num_eleve;
--ou
select num_eleve, avg(points) from resultats where num_eleve in (select num_eleve from eleves where annee = 1) having avg(points) > (select avg(points) from resultats where num_eleve in (select num_eleve from eleves where annee = 1)) group by num_eleve;
```

```bash
NUM_ELEVE  AVG(POINTS)
---------- -----------
  1        13,375
  9        15,875
```

34. Obtenir le nom et le poids des élèves de 1 ère année plus lourds que
n’importe quel élève de 2 ème année.

```sql
select nom, poids from eleves where annee = 1 and poids > all(select poids from eleves where annee = 2);
--ou
select nom,poids from eleves where annee = 1 and poids > (select MAX(poids) from eleves where annee = 2);
```

```bash
aucune ligne s�lectionn�e
```

35. Obtenir le nom et le poids des élèves de 1 ère année plus lourds qu’un
élève quelconque de 2 ème année.

```sql
select nom, poids from eleves where annee = 1 and poids > any(select poids from eleves where annee = 2);
--ou
select nom,poids from eleves where annee = 1 and poids > (select MIN(poids) from eleves where annee = 2);
```

```bash
NOM                        POIDS
-------------------------- ----------
Lagaffe                    61
```

36. Obtenir le nom, le poids et l’année des élèves dont le poids est supérieur au poids moyen des élèves étant dans la même année d’études.

```sql
select nom, poids, annee from eleves where annee = 1 and poids > (select avg(poids) from eleves where annee = 1) Union select nom, poids, annee from eleves where annee = 2 and poids > (select avg(poids) from eleves where annee = 2);
--ou
select nom, poids, annee from eleves where annee = 1 and poids > (select avg(poids) from eleves where annee = 1) Or annee = 2 and poids > (select avg(poids) from eleves where annee = 2);
```

```bash
NOM            POIDS     ANNEE
-------------- ---------- ----------
Danny          82        2
Lagaffe        61        1
Lebut          75        2
Spring         78        2
Walthéry       59        1
```

37. Obtenir le nom des professeurs qui ne donnent pas le cours numéro 1.

```sql
select nom from professeurs where num_prof not in (select num_prof from charge where num_cours = 1);
--ou
select nom from professeurs p where not exists (select num_prof from charge c where num_cours = 1 and p.num_prof = c.num_prof);
```

```bash
NOM
-------------------------
Tonilaclasse
Pastecnov
Selector
Vilplusplus
Francesca
```

38. Obtenir le nom des élèves de 1 ère année qui ont obtenu plus de 60 % et
qui jouent au tennis.

```sql
select nom from eleves where annee = 1 and num_eleve in (select num_eleve from resultats having (avg(points)*5)>60 group by num_eleve) and num_eleve in (select num_eleve from activites_pratiquees where nom = 'Tennis' );
--ou
select t1.nom from eleves t1
join activites_pratiquees t3 on t1.num_eleve = t3.num_eleve
join resultats t2 on t1.num_eleve = t2.num_eleve where t3.nom = 'Tennis' and t1.annee = 1
having(avg(t2.points)*5)>60  group by t1.nom;
```

```bash
NOM
-------------------------
Brisefer
```

39. Professeurs qui prennent en charge tous les cours de deuxième année ;
on demande le Numéro et le nom.

```sql
select num_prof, nom from Professeurs where num_prof in (select num_prof from charge where num_cours in (select num_cours from cours where annee = 2) having count(num_prof)=(select count(num_cours) from cours where annee = 2) group by num_prof);
--ou
select t1.num_prof, t1.nom from professeurs t1 join charge t2 on t1.num_prof = t2.num_prof join cours t3 on t2.num_cours = t3.num_cours where annee = 2 group by t1.num_prof,t1.nom having count(t1.num_prof) = (select count(num_cours) from cours where annee = 2);
```

```bash
NUM_PROF   NOM
---------- -------------------------
 8         Pucette
 3         Tonilaclasse

```

40. Élèves qui pratiquent toutes les activités ; on demande le Numéro et le
nom.

```sql
select t1.num_eleve,t1.nom from eleves t1 where not exists (select * from activites t2 where not exists (select * from activites_pratiquees t3 where t1.num_eleve=t3.num_eleve and t2.nom = t3.nom));
--ou
select num_eleve,nom from eleves E where (select count(distinct nom) from activites) = (select count(distinct nom) from activites_pratiquees A where A.num_eleve = E.num_eleve)  ;
```

```bash
NUM_ELEVE  NOM
---------- -------------------------
1        Brisefer
```
