## TP 6 : SQL requetes avances ##

PROMPT ### **B** Formulez et testez les requêtes suivantes en SQL en proposant au moins deux solutions différentes pour chaque question **si possible :** ###

PROMPT 1.  Donner la liste des noms, des prénoms et des dates de naissance de tous les élèves.

select nom, prenom, date_naissance from eleves;

PROMPT
pause Tapez sur Enter...
PROMPT

select concat(nom,concat(' ',concat(prenom,concat(' ', date_naissance)))) "Nom Prenom date de naissance" from eleves;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 2.  Donner tous les renseingements sur toutes les activités
PROMPT

select * from activites;

PROMPT
pause Tapez sur Enter...
PROMPT

select niveau, nom, equipe from activites;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 3.  Lister les spécialités des professeurs
PROMPT

select nom, specialite from professeurs;

PROMPT
pause Tapez sur Enter...
PROMPT

select nom, specialite from professeurs order by nom desc;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 4.  Obtenir le nom et prénom des élèves pesant moins de 45 kilos et inscrits en 1 ère année ou des élèves inscrits en 2 ème année.

select nom, prenom from eleves where poids < 45 AND annee = 1 OR annee = 2;

PROMPT
pause Tapez sur Enter...
PROMPT

select nom, prenom from eleves where (poids between 0 AND 45 and annee = 1 ) OR annee = 2;


PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 5.  Obtenir le nom des élèves dont le poids est compris entre 60 et 80 kilos.
PROMPT

select nom, poids from eleves where poids <= 80 and poids >= 60;

PROMPT
pause Tapez sur Enter...
PROMPT

select nom, poids from eleves where poids between 60 and 80;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 6.  Obtenir le nom des professeurs dont la spécialité est ‘poésie’ ou SQL.
PROMPT

select nom from professeurs where specialite in ('poésie','sql');

PROMPT
pause Tapez sur Enter...
PROMPT

select nom from professeurs where specialite = 'poésie' or specialite = 'sql';

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 7.  Obtenir le nom des élèves dont le nom commence par ‘L’.
PROMPT
select nom from eleves where nom like 'L%';

PROMPT
pause Tapez sur Enter...
PROMPT

select nom from eleves where substr(nom, 1, 1) = 'L';

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 8.  Obtenir le nom des professeurs dont la spécialité est inconnue.
PROMPT

select nom from professeurs where specialite is NULL;

PROMPT
pause Tapez sur Enter...
PROMPT

select nom from professeurs having count(specialite) < 1 group by nom;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 9.  Obtenir le nom et prénom des élèves pesant moins de 45 kilos et inscrits en 1 ère année.
PROMPT

select nom, prenom from eleves where poids < 45 AND annee = 1;

PROMPT
pause Tapez sur Enter...
PROMPT

select nom, prenom from eleves where (poids between 0 and 44) AND annee = 1;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 10. Obtenir, pour chaque professeur, son nom et sa spécialité. Si cette dernière est inconnue, on souhaite afficher la chaîne de caractères : ‘****’.
PROMPT

select nom, coalesce(specialite, '****') AS specialite from professeurs;

PROMPT
pause Tapez sur Enter...
PROMPT

SELECT nom, NVL(specialite, '****') AS specialite FROM professeurs;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 11. Quels sont les noms et prénoms des élèves qui pratiquent du surf au niveau 1. Rédigez cette requête de cinq façons différentes.
PROMPT

select t1.nom, t1.prenom from eleves t1 join ACTIVITES_PRATIQUEES t2 on t2.num_eleve = t1.num_eleve where t2.nom = 'Surf' and t2.niveau = 1;

PROMPT
pause Tapez sur Enter...
PROMPT

select nom, prenom from eleves where num_eleve in (select num_eleve from activites_pratiquees where nom = 'Surf' and niveau = 1);

PROMPT
pause Tapez sur Enter...
PROMPT

select t1.nom, t1.prenom from eleves t1 where exists (select * from activites_pratiquees t2 where t1.num_eleve = t2.num_eleve and t2.nom = 'Surf' and t2.niveau = 1);

PROMPT
pause Tapez sur Enter...
PROMPT

SELECT NOM,PRENOM FROM ELEVES WHERE NUM_ELEVE IN (SELECT NUM_ELEVE
FROM ACTIVITES_PRATIQUEES WHERE NOM != ALL(SELECT DISTINCT NOM FROM
ACTIVITES_PRATIQUEES WHERE NOM != 'Surf') AND NIVEAU = 1);

PROMPT
pause Tapez sur Enter...
PROMPT

SELECT NOM, PRENOM FROM ELEVES WHERE NUM_ELEVE IN
(SELECT NUM_ELEVE FROM ACTIVITES_PRATIQUEES WHERE NOM = 'Surf'
INTERSECT SELECT NUM_ELEVE FROM ACTIVITES_PRATIQUEES WHERE NIVEAU = 1);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 12. Obtenir le nom des élèves de l’équipe AMC INDUS.
PROMPT

select t1.nom from eleves t1 join ACTIVITES_PRATIQUEES t2 on t1.num_eleve = t2.num_eleve join activites t3 on t3.niveau = t2.niveau and t3.nom = t2.nom where equipe = 'Amc Indus';

PROMPT
pause Tapez sur Enter...
PROMPT

select nom from eleves where num_eleve in (select num_eleve from activites_pratiquees where concat(niveau,nom) in (select concat(niveau, nom) from activites where equipe = 'Amc Indus'));

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 13. Obtenir les pairs de noms de professeurs qui ont la même spécialité.
PROMPT

select t1.nom,t2.nom,t1.specialite from professeurs t1, professeurs t2 where t1.specialite = t2.specialite and t1.nom > t2.nom;

PROMPT
pause Tapez sur Enter...
PROMPT

select t1.nom,t2.nom,t1.specialite from professeurs t1 join professeurs t2 on t1.specialite = t2.specialite where t1.nom > t2.nom;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 14. Pour chaque spécialité sql/SQL, on demande d’obtenir son nom son salaire mensuel actuel et son augmentation mensuelle depuis son salaire de base.
PROMPT

select nom, specialite, ((Salaire_actuel - Salaire_base) / MONTHS_BETWEEN(Der_prom, Date_entree) ) "augmentation mensuelle" from professeurs where specialite = 'sql';

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 15. Obtenir le nom des professeurs dont l’augmentation relative au salaire de base dépasse 25%.

select nom from professeurs where (Salaire_actuel - Salaire_base) > (Salaire_base * 25 / 100);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 16. Afficher les points de Tsuno obtenus dans chaque cours sur 100 plutôt que sur 20.
PROMPT

select (points * 5) "Points sur 100" from resultats where Num_eleve in(select Num_eleve from eleves where nom = 'Tsuno');

PROMPT
pause Tapez sur Enter...
PROMPT

select (points * 5) "Points sur 100" from resultats where Num_eleve =(select Num_eleve from eleves where nom = 'Tsuno');

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 17. Obtenir le poids moyen des élèves de 1 ère année.
PROMPT
select avg(poids) "poids moyen" from eleves where annee = 1;

PROMPT
pause Tapez sur Enter...
PROMPT

select sum(poids)/count(*) "poids moyen" from eleves where annee = 1;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 18. Obtenir le total des points de l’élève numéro 3.
PROMPT

select sum(points) from RESULTATS where Num_eleve = 3;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 19. Obtenir la plus petite et la plus grande cote de l’élève Brisefer.
PROMPT

select min(points),max(points) from RESULTATS where Num_eleve in (select Num_eleve from eleves where nom = 'Brisefer');

PROMPT
pause Tapez sur Enter...
PROMPT

select min(points),max(points) from RESULTATS natural join eleves where nom = 'Brisefer';

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 20. Obtenir le nombre d’élèves inscrits en deuxième année.
PROMPT

select count(*) from eleves where annee = 2;

PROMPT
pause Tapez sur Enter...
PROMPT


PROMPT 21. Quel est l augmentation mensuelle moyenne des salaires des professeurs de SQL?
PROMPT

select avg((Salaire_actuel - Salaire_base)/months_between(der_prom, date_entree)) "Augmentation mensuelle moyenne" from professeurs where specialite = 'sql';

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 22. Obtenir l année de la dernière promotion du professeur pucette.
PROMPT

select EXTRACT(YEAR from der_prom) "Année" from professeurs where nom like 'Pucette';

PROMPT
pause Tapez sur Enter...
PROMPT

select substr(der_prom,7,4) "Année" from professeurs where nom like 'Pucette';

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 23. Pour chaque professuer, afficher sa date d'embauche, sa date de dernière promotion ansi que le nombre d'années écoulé entre ces deux dates.
PROMPT

select nom, date_entree, der_prom, months_between(der_prom, date_entree)/12 AS DateDiff from professeurs;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 24. Afficher lage moyen des élèves. Cet âge sera exprimée en année.
PROMPT

select avg(months_between(sysdate, date_naissance)/12) "age moyen" from eleves;

PROMPT
pause Tapez sur Enter...
PROMPT

select sum(months_between(sysdate, date_naissance)/12)/count(*) "age moyen" from eleves;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 25. Affiche le nom des professeurs pour lesquels il s est écoulé plus de 50 mois entre l embauche et la première promotion
PROMPT

select nom from professeurs where months_between(der_prom, date_entree) > 50;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 26. Obtenir la liste des élèves qui auront au moins 24 dans moins de 4 mois.
PROMPT

select nom from eleves where months_between(sysdate, date_naissance)+4/12 > 24;

PROMPT
pause Tapez sur Enter...
PROMPT


PROMPT 27. Obtenir une liste des élèves classé par année et par ordre alphabétique
PROMPT

select nom, annee from eleves order by annee, nom ;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 28. Afficher en ordre decroissant les points de Tsuno obtenus dans chaque cours sur 100 plutot que sur 20.
PROMPT

select num_cours, points*5 from resultats where num_eleve = (select num_eleve from eleves where nom like('Tsuno')) order by 2 desc;

PROMPT
pause Tapez sur Enter...
PROMPT

select num_cours, points*5 from resultats natural join eleves where nom = 'Tsuno' order by 2 desc;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 29. Obtenir pour chaque élève de 1 ère année son nom et sa moyenne.
PROMPT

select nom, avg(points) from resultats natural join eleves group by nom;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 30. Obtenir la moyenne des points de chaque élève de 1 ère année dont le to- tal des points est supérieur à 40.
PROMPT

select num_eleve, avg(points) from resultats having sum(points) > 40 group by num_eleve;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 31. Obtenir le maximum parmi les totaux de chaque élève.

select max(sum(points)) "Max des totaux" from resultats group by num_eleve;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 33. Quels sont les élèves de 1 ère année dont la moyenne est supérieure à la moyenne de la 1 ère année ?

select num_eleve, avg(points) from resultats natural join eleves where annee = 1 having avg(points) > (select avg(points) from resultats natural join eleves where annee = 1) group by num_eleve;

PROMPT
pause Tapez sur Enter...
PROMPT

select num_eleve, avg(points) from resultats where num_eleve in (select num_eleve from eleves where annee = 1) having avg(points) > (select avg(points) from resultats where num_eleve in (select num_eleve from eleves where annee = 1)) group by num_eleve;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 34. Obtenir le nom et le poids des élèves de 1 ère année plus lourds que n’importe quel élève de 2 ème année.
PROMPT

select nom, poids from eleves where annee = 1 and poids > all(select poids from eleves where annee = 2);

PROMPT
pause Tapez sur Enter...
PROMPT

select nom,poids from eleves where annee = 1 and poids > (select MAX(poids) from eleves where annee = 2);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 35. Obtenir le nom et le poids des élèves de 1 ère année plus lourds qu’un élève quelconque de 2 ème année.

select nom, poids from eleves where annee = 1 and poids > any(select poids from eleves where annee = 2);

PROMPT
pause Tapez sur Enter...
PROMPT

select nom,poids from eleves where annee = 1 and poids > (select MIN(poids) from eleves where annee = 2);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 36. Obtenir le nom, le poids et l’année des élèves dont le poids est supérieur au poids moyen des élèves étant dans la même année d’études.
PROMPT

select nom, poids, annee from eleves where annee = 1 and poids > (select avg(poids) from eleves where annee = 1) Union select nom, poids, annee from eleves where annee = 2 and poids > (select avg(poids) from eleves where annee = 2);

PROMPT
pause Tapez sur Enter...
PROMPT

select nom, poids, annee from eleves where annee = 1 and poids > (select avg(poids) from eleves where annee = 1) Or annee = 2 and poids > (select avg(poids) from eleves where annee = 2);

PROMPT
pause Tapez sur Enter...
PROMPT


PROMPT 37. Obtenir le nom des professeurs qui ne donnent pas le cours numéro 1.
PROMPT

select nom from professeurs where num_prof not in (select num_prof from charge where num_cours = 1);

PROMPT
pause Tapez sur Enter...
PROMPT

select nom from professeurs p where not exists (select num_prof from charge c where num_cours = 1 and p.num_prof = c.num_prof);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 38. Obtenir le nom des élèves de 1 ère année qui ont obtenu plus de 60 % et qui jouent au tennis.
PROMPT

select nom from eleves where annee = 1 and num_eleve in (select num_eleve from resultats having (avg(points)*5)>60 group by num_eleve) and num_eleve in (select num_eleve from activites_pratiquees where nom = 'Tennis' );

PROMPT
pause Tapez sur Enter...
PROMPT

select t1.nom from eleves t1
join activites_pratiquees t3 on t1.num_eleve = t3.num_eleve
join resultats t2 on t1.num_eleve = t2.num_eleve where t3.nom = 'Tennis' and t1.annee = 1
having(avg(t2.points)*5)>60  group by t1.nom;

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 39. Professeurs qui prennent en charge tous les cours de deuxième année ; on demande le Numéro et le nom.
PROMPT

select num_prof, nom from Professeurs where num_prof in (select num_prof from charge where num_cours in (select num_cours from cours where annee = 2) having count(num_prof)=(select count(num_cours) from cours where annee = 2) group by num_prof);

PROMPT
pause Tapez sur Enter...
PROMPT

select t1.num_prof, t1.nom from professeurs t1 join charge t2 on t1.num_prof = t2.num_prof join cours t3 on t2.num_cours = t3.num_cours where annee = 2 group by t1.num_prof,t1.nom having count(t1.num_prof) = (select count(num_cours) from cours where annee = 2);

PROMPT
pause Tapez sur Enter...
PROMPT

PROMPT 40. Élèves qui pratiquent toutes les activités ; on demande le Numéro et le nom.
PROMPT
select t1.num_eleve,t1.nom from eleves t1 where not exists (select * from activites t2 where not exists (select * from activites_pratiquees t3 where t1.num_eleve=t3.num_eleve and t2.nom = t3.nom));

PROMPT
pause Tapez sur Enter...
PROMPT

select num_eleve,nom from eleves E where (select count(distinct nom) from activites) = (select count(distinct nom) from activites_pratiquees A where A.num_eleve = E.num_eleve)  ;
