## TP 3 : Fonctions Oracles ##

### A. Exploration de quelques fonctions ORACLE ###

1.  Test des commandes suivantes et explications:

```sql
SELECT RPAD('Soleil',17,'bla') "RPAD exemple" FROM DUAL;
```

Ecrit soleil, le concatène avec 'bla' jusqu'à 17 caractères. (Right padding)

```sql
SELECT LPAD('Master 2 EID',15,'*.') "LPAD exemple" FROM DUAL;
```

Ecrit le caractère '\*.' devant Master 2 EID par concatenation plusieurs fois dans la limite de 15 caractère. (Left padding)

```sql
SELECT SUBSTR('DESS EID',6,3) "SUBSTR exemple" FROM DUAL;
```

Affiche 3 caractère à partir de la sixième case qui est le .

```sql
SELECT SUBSTR('ABCDEFGHIJ',-5,4) "SUBSTR exemple" FROM DUAL;
```

Affiche les 4 caractère suivantes à partir de la case -5 en partant de la fin (donc la case 5 en partant du début).

```sql
SELECT TO_CHAR (SYSDATE, 'MM-DD-YYYY HH24:MI:SS') "Now" FROM DUAL;
```
Affiche la date actuelle et heure du systeme hôte qui heberge le serveur SQL avec ayant comme étiquette "Now".

```sql
SELECT LENGTH('WEB WAREHOUSE') "Longueur en caractères" FROM DUAL;
```
Calcule et affiche la longueur de la chaine WEB WAREHOUSE.

```sql
SELECT ROUND(17.0958,1) "ROUND exemple" FROM DUAL;
```

Arrondie le nombre 17.0958 à 1 chiffre après la virgule.

```sql
SELECT ROUND(17.58,2) "ROUND exemple" FROM DUAL;
```

Arrondie le nombre 17.58 à 2 chiffre apèrs la virgule. (Aucun changement dans ce cas)

```sql
SELECT TRUNC(1958.0917,1) "TRUNC exemple" FROM DUAL;
```
Troncature : Affiche le nombre à 1 chiffre après la virgule sans arrondir.

```sql
SELECT TRUNC(1958.0917,2) "TRUNC exemple" FROM DUAL;
```
Troncature : Affiche le nombre à 2 chiffre après la virgule.

```sql
SELECT ROUND(TO_DATE('17-SEP-2009'), 'YEAR') "New Year" FROM DUAL;
```

Affiche une erreur à cause du SEP, il faut mettre 09 ou 9 pour que ça marche.
Cette requête SQL arrondi une date en +1 pour les mois et année, et au premier jour du mois pour ce qui concerne du jour.

Version corrigé de la requête.
```sql
SELECT ROUND(TO_DATE('17-9-2009'), 'YEAR') "New Year" FROM DUAL;
```

```sql
SELECT SYSDATE FROM DUAL;
```
Affiche la date d'aujourd'hui.

```sql
SELECT EXTRACT(YEAR FROM SYSDATE) FROM DUAL;
```
Recupère la date de l'hôte, et affiche uniquement l'année par extraction de cette date.

```sql
SELECT ADD_MONTHS(SYSDATE,7) FROM DUAL;
```

Ajoute 7 mois à l'affichage de la date de l'hôte.

```sql
SELECT TRUNC(MONTHS_BETWEEN(SYSDATE, TO_DATE('19-JUN-2001'))) AS AGEBB FROM DUAL;
```
Ne marche pas pour la meme raison plus haut. A remplacer par la requête suivante

```sql
SELECT TRUNC(MONTHS_BETWEEN(SYSDATE, TO_DATE('19-6-2001'))) AS AGEBB FROM DUAL;
```

Affiche la différence de mois entre la date donnée et la date de l'hote, le stock dans la colonne AGEBB.

```sql
SELECT TO_NUMBER(TO_CHAR(SYSDATE, 'YYYY')) FROM DUAL;
```
Converti l'année de l'hôte en char, et le reconvertie en nombre.


2.  Changement de format du type de date.

```sql
ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY';
```

### B. Exemple sur la vrai table ###

2.  Insertion des lignes :

```sql
INSERT INTO ETUDIANTS VALUES (71, 'Traifor', 'Benoit', 'M', '10/12/1978', 77, 1);
INSERT INTO ETUDIANTS VALUES (72, 'Génial', 'Clément', 'M', '10/04/1978', 72, 1);
INSERT INTO ETUDIANTS VALUES (73, 'Paris', 'Adam', 'M', '28/06/1974', 72, 1);
INSERT INTO ETUDIANTS VALUES (74, 'Parees', 'Clémence', 'F', '20/09/1977', 72, NULL);
INSERT INTO ETUDIANTS VALUES (69, 'Saitout', 'Inès', 'F', '22/11/1969', 69, 2);
INSERT INTO ETUDIANTS VALUES (55, 'Serafoub', 'Izouaf', 'M', '19/09/2013', 81, 1);

```


3.  Test de requêtes

```sql
SELECT DECODE(ANNEE, 1, 'Première', 2, 'Seconde', 'Valeur différente de 1 et de 2 !!') AS ANETUDE FROM ETUDIANTS;
```

```bash
ANETUDE
----------------------------------
Première
Première
Première
Valeur différente de
1 et de 2 !!

Seconde
Première

6 lignes s�lectionn�es.
```

Cette commande DECODE permet de remplacer toute occurence de X par Y.
Dans l'exemple de la requête ci-dessus, Toute occurence de 1 sera remplacé par Première, il en est de même pour 2. En revanche, Toute occurence vide sera remplacé par Valeur différente de 1 et de 2.


```sql
SELECT UPPER(NOM) FROM ETUDIANTS;
```

Cette commande UPPER permer de Capitaliser toute les lettre de nom de la table etudiants (Mise en format du résultat).

```bash
UPPER(NOM)
-------------------------
TRAIFOR
GéNIAL
PARIS
PAREES
SAITOUT
SERAFOUB

6 lignes s�lectionn�es.
```

```sql
SELECT LOWER(NOM) FROM ETUDIANTS;
```
La requête lower permet de mettre tous les lettres de nom en minuscule dans le format de sortie

```bash

LOWER(NOM)
-------------------------
traifor
g��nial
paris
parees
saitout
serafoub

6 lignes s�lectionn�es.
```
Quel est le problème dans la fonction NVL ?
Le problème est que le deuxième argument de nvl doit être du même type de que l'attribue de la table. On peut y corriger en mettant -1 pour l'exemple donnée.

```sql
SELECT NVL(ANNEE, '-1') FROM ETUDIANTS;
```
Remplace les null par une autre valeur.

```sql
SELECT NVL(ANNEE, '-1') AS AN_ETUDE FROM ETUDIANTS;
```

Pareille que le précédente en donnant à la colonne le nom AN_ETUDE.

4.  Gestion de l'affichage.

```sql
COL poids FORMAT 999
TTITLE "Etudiants"
set pages 50
set line 50
```

5.  Interrogation de la BD

*   Affichez le nom et le prénom de tous les étudiants en une seule colonne.

```sql
select (concat(nom,concat(' ', prenom)))"NOM PRENOM" from etudiants;
```

```bash
NOM PRENOM
---------------------------------------------------
Traifor Benoit
Génial Clément
Paris Adam
Parees Clémence
Saitout Inès
Serafoub Izouaf
```

*   Affichez la première lettre du prénom en majuscule suivie d'un point et d'un
espace (. ) suivis du nom en majuscule en une seule colonne, pour les
étudiants de sexe masculin.

```sql
select (concat(concat(upper(SUBSTR(prenom,1,1)), '. '), upper(nom))) from etudiants;
```

```bash
----------------------------
B. TRAIFOR
C. GéNIAL
A. PARIS
C. PAREES
I. SAITOUT
I. SERAFOUB
```

*   Affichez le nom et l’année de naissance des étudiants dont le nom se
prononce comme ‘Paris’.

```sql
SELECT concat (nom, EXTRACT(YEAR from datenaissance)) from etudiants where soundex(nom) = soundex('paris');
```

```bash
CONCAT(NOM,EXTRACT(YEARFROMDATENAISSANCE))
-----------------------------------------------------------------
Paris1974
Parees1977
```

*   Affichez le nom et l’année de naissance des étudiants dont le prénom
commence par la lettre I.

```sql
SELECT concat (nom, EXTRACT(YEAR from datenaissance)) from etudiants where  SUBSTR(prenom,1,1) = 'I';
```

```bash
CONCAT(NOM,EXTRACT(YEARFROMDATENAISSANCE))
-----------------------------------------------------------------
Saitout1969
Serafoub2013
```

*   Proposez d'autres requêtes répondant à d'autres questions. Soyez créatif !

Afficher la liste des étudiants qui sont plus lourd que 70 kgs

```sql
select nom, prenom from etudiants where poids > 70;
```

```bash
NOM       PRENOM    POIDS
--------- --------- ------
Traifor   Benoit    77
Génial    Clément   72
Paris     Adam      72
Parees    Clémence  72
Serafoub  Izouaf    81
```

Afficher la liste des étudiants par ordre croissant de leur nom

```sql
select nom, prenom from etudiants order by nom asc;
```

```bash
NOM         PRENOM
----------- -------------------------
Génial      Clément
Parees      Clémence
Paris       Adam
Saitout     Inès
Serafoub    Izouaf
Traifor     Benoit

6 lignes s�lectionn�es.

```
