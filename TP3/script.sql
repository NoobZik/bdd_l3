PROMPT ### A. Exploration de quelques fonctions ORACLE ###
PROMPT
PROMPT 1.  Test des commandes suivantes et explications:
PROMPT

SELECT RPAD('Soleil',17,'bla') "RPAD exemple" FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT LPAD('Master 2 EID',15,'*.') "LPAD exemple" FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT SUBSTR('DESS EID',6,3) "SUBSTR exemple" FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT SUBSTR('ABCDEFGHIJ',-5,4) "SUBSTR exemple" FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT TO_CHAR (SYSDATE, 'MM-DD-YYYY HH24:MI:SS') "Now" FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT LENGTH('WEB WAREHOUSE') "Longueur en caractères" FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT ROUND(17.0958,1) "ROUND exemple" FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT ROUND(17.58,2) "ROUND exemple" FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT TRUNC(1958.0917,1) "TRUNC exemple" FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT TRUNC(1958.0917,2) "TRUNC exemple" FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT ROUND(TO_DATE('17-SEP-2009'), 'YEAR') "New Year" FROM DUAL;


PROMPT
pause press enter
PROMPT

PROMPT Version corrigé
PROMPT
SELECT ROUND(TO_DATE('17-9-2009'), 'YEAR') "New Year" FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT SYSDATE FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT EXTRACT(YEAR FROM SYSDATE) FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT ADD_MONTHS(SYSDATE,7) FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT TRUNC(MONTHS_BETWEEN(SYSDATE, TO_DATE('19-JUN-2001'))) AS AGEBB FROM DUAL;

PROMPT
pause press enter
PROMPT

PROMPT Version corrected
PROMPT
SELECT TRUNC(MONTHS_BETWEEN(SYSDATE, TO_DATE('19-6-2001'))) AS AGEBB FROM DUAL;

PROMPT
pause press enter
PROMPT

SELECT TO_NUMBER(TO_CHAR(SYSDATE, 'YYYY')) FROM DUAL;

PROMPT
pause press enter
PROMPT


PROMPT 2.  Changement de format du type de date.
PROMPT

ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY';

PROMPT
pause press enter
PROMPT

PROMPT ### B. Exemple sur la vrai table ###
PROMPT

PROMPT 2.  Insertion des lignes :
PROMPT

INSERT INTO ETUDIANTS VALUES (71, 'Traifor', 'Benoit', 'M', '10/12/1978', 77, 1);
INSERT INTO ETUDIANTS VALUES (72, 'Génial', 'Clément', 'M', '10/04/1978', 72, 1);
INSERT INTO ETUDIANTS VALUES (73, 'Paris', 'Adam', 'M', '28/06/1974', 72, 1);
INSERT INTO ETUDIANTS VALUES (74, 'Parees', 'Clémence', 'F', '20/09/1977', 72, NULL);
INSERT INTO ETUDIANTS VALUES (69, 'Saitout', 'Inès', 'F', '22/11/1969', 69, 2);
INSERT INTO ETUDIANTS VALUES (55, 'Serafoub', 'Izouaf', 'M', '19/09/2013', 81, 1);

PROMPT
pause press enter
PROMPT

PROMPT 3.  Test de requêtes
PROMPT

SELECT DECODE(ANNEE, 1, 'Première', 2, 'Seconde', 'Valeur différente de 1 et de 2 !!') AS ANETUDE FROM ETUDIANTS;

PROMPT
pause press enter
PROMPT

SELECT UPPER(NOM) FROM ETUDIANTS;

PROMPT
pause press enter
PROMPT

SELECT LOWER(NOM) FROM ETUDIANTS;

PROMPT
pause press enter
PROMPT

SELECT NVL(ANNEE, '-1') FROM ETUDIANTS;

PROMPT
pause press enter
PROMPT

SELECT NVL(ANNEE, '-1') AS AN_ETUDE FROM ETUDIANTS;

PROMPT
pause press enter
PROMPT

COL poids FORMAT 999
TTITLE "Etudiants"
set pages 50
set line 50

PROMPT
pause press enter
PROMPT

PROMPT 5.  Interrogation de la BD
PROMPT

PROMPT Affichez le nom et le prénom de tous les étudiants en une seule colonne.
PROMPT

select (concat(nom,concat(' ', prenom)))"NOM PRENOM" from etudiants;

PROMPT
pause press enter
PROMPT

PROMPT  Affichez la première lettre du prénom en majuscule suivie d'un point et d'un espace (. ) suivis du nom en majuscule en une seule colonne, pour les étudiants de sexe masculin.
PROMPT

select (concat(concat(upper(SUBSTR(prenom,1,1)), '. '), upper(nom))) from etudiants;

PROMPT
pause press enter
PROMPT

PROMPT Affichez le nom et l’année de naissance des étudiants dont le nom se prononce comme ‘Paris’.
PROMPT

SELECT concat (nom, EXTRACT(YEAR from datenaissance)) from etudiants where soundex(nom) = soundex('paris');

PROMPT
pause press enter
PROMPT

PROMPT Affichez le nom et l’année de naissance des étudiants dont le prénom commence par la lettre I.
PROMPT

SELECT concat (nom, EXTRACT(YEAR from datenaissance)) from etudiants where  SUBSTR(prenom,1,1) = 'I';

PROMPT
pause press enter
PROMPT

PROMPT Proposez dautres requêtes répondant à dautres questions. Soyez créatif !
PROMPT

PROMPT Afficher la liste des étudiants qui sont plus lourd que 70 kgs
PROMPT

select nom, prenom from etudiants where poids > 70;

PROMPT
pause press enter
PROMPT

PROMPT Afficher la liste des étudiants par ordre croissant de leur nom
PROMPT
select nom, prenom from etudiants order by nom asc;

PROMPT
pause press enter
PROMPT
