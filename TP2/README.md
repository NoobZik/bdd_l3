## TP 2 : Modification d'une base de données sous ORACLE ##

### A. Modification de contraintes ###

1.  Nécessité d'une meilleur définition de contrainte

```sql
CREATE TABLE ETUDIANTS(
  NUMERO        NUMBER(4)     NOT NULL,
  NOM           VARCHAR2(25)  NOT NULL,
  PRENOM        VARCHAR2(25)  NOT NULL,
  SEXE          CHAR(1)       CHECK (SEXE IN ('F', 'M')),
  DATENAISSANCE DATE          NOT NULL,
  POIDS         NUMBER,
  ANNEE         NUMBER,
  CONSTRAINT PK_ETUDIANTS PRIMARY KEY (NUMERO)
);
```

Il est évident qu'il faut une meilleur definition des contraintes car elles ne sont pas nommées, en effet, les contraintes actuelles sont affiché en codé de la manière suivante :

```sql
ttitle ‘La liste des contraintes créées, sur la table ETUDIANTS, est :’
select constraint_name from user_constraints where table_name='ETUDIANTS';
```

```bash
‘Lalistedescontraintescréées,surlatableETUDIANTS,est:’
CONSTRAINT_NAME
------------------------------
SYS_C001019223
SYS_C001019224
SYS_C001019225
SYS_C001019226
SYS_C001019227
PK_ETUDIANTS
```

2.  Redéfinition de la table en nommant les contraintes

```sql
DROP TABLE ETUDIANTS;

CREATE TABLE  ETUDIANTS (
  NUMERO          NUMBER(4),
  NOM             VARCHAR2(25),
  PRENOM          VARCHAR2(25),
  SEXE            CHAR(1),
  DATENAISSANCE   DATE,
  POIDS           NUMBER,
  ANNEE           NUMBER,
  CONSTRAINT PK_ETUDIANTS PRIMARY KEY (NUMERO),
  CONSTRAINT NN_ETUDIANTS_NOM CHECK (NOM IS NOT NULL),
  CONSTRAINT NN_ETUDIANTS_PRENOM CHECK (PRENOM IS NOT NULL),
  CONSTRAINT CHECK_FM_ETUDIANTS_SEXE CHECK (SEXE IN ('F', 'M')),
  CONSTRAINT NN_ETUDIANTS_DATENAISSANCE CHECK (DATENAISSANCE IS NOT NULL)
);

ttitle ‘La liste des contraintes créées, sur la table ETUDIANTS, est :’
select constraint_name from user_constraints where table_name='ETUDIANTS';
```

```bash
‘Lalistedescontraintescréées,surlatableETUDIANTS,est:’
CONSTRAINT_NAME
------------------------------
NN_ETUDIANTS_NOM
NN_ETUDIANTS_PRENOM
CHECK_FM_ETUDIANTS_SEXE
NN_ETUDIANTS_DATENAISSANCE
PK_ETUDIANTS
```

Grâce a cette définition des contraintes il est plus simple d'identifier les contraintes qui ont été violé puisqu'elle sont nommées.

3.  Ajout de 2 contraintes

```sql
ALTER TABLE etudiants ADD CONSTRAINT CHECK_ETUDIANTS_ANNEE CHECK (annee = 1 OR annee = 2);
ALTER TABLE etudiants ADD CONSTRAINT CHECK_ETUDIANTS_POIDS CHECK (POIDS > 30 OR POIDS < 200);
```

4.  Modification des noms des contraintes selon la norme

```sql
ALTER TABLE etudiants RENAME CONSTRAINT CHECK_FM_ETUDIANTS_SEXE to CK_ETUDIANTS_SEXE;
ALTER TABLE etudiants RENAME CONSTRAINT CHECK_ETUDIANTS_ANNEE to CK_ETUDIANTS_ANNEE;
ALTER TABLE etudiants RENAME CONSTRAINT CHECK_ETUDIANTS_POIDS to CK_ETUDIANTS_POIDS;
```
On obtient alors la table se trouvant dans etudiants.sql .

### B- Manipulations de la BD Ecole ###

2.  Ajout des clées étrangères

```sql
/* ACTIVITES_PRATIQUEES */
ALTER TABLE ACTIVITES_PRATIQUEES ADD CONSTRAINT FK_ActPr_num_Eleve_Eleves FOREIGN KEY (NUM_ELEVE) REFERENCES ELEVES(num_eleve);
-- Ici il faut mettre dans la contraintes tout les attributs en meme temps, sinon ca fait une erreur de correspondance de clée primaire ou unique pour cette liste de colonne.
ALTER TABLE ACTIVITES_PRATIQUEES ADD CONSTRAINT FK_ActPr_niveau_ACTIVITES FOREIGN KEY (niveau,nom) REFERENCES ACTIVITES(NIVEAU, NOM);
-- CHARGE
ALTER TABLE CHARGE ADD CONSTRAINT FK_CHARGE_NumProf_PROFESSEURS FOREIGN KEY (Num_Prof) REFERENCES PROFESSEURS(NUM_PROF);
ALTER TABLE CHARGE ADD CONSTRAINT FK_CHARGE_NumCours_COURS FOREIGN KEY (Num_Cours) REFERENCES COURS(NUM_COURS);
-- RESULTATS
ALTER TABLE RESULTATS ADD CONSTRAINT FK_RESULTATS_NumEleve_ELEVES FOREIGN KEY (num_eleve) REFERENCES ELEVES(num_eleve);
ALTER TABLE RESULTATS ADD CONSTRAINT FK_RESULTATS_NumCours_COURS FOREIGN KEY (Num_Cours) REFERENCES COURS(NUM_COURS);
```

3.  Affichage de la structure ELEVES

On fait appel à :

```sql
desc ELEVES;
```

pour afficher la structure de la table.

```bash
Nom             NULL ?   Type
--------------- -------- -------------
NUM_ELEVE       NOT NULL NUMBER(4)
NOM                      VARCHAR2(25)
PRENOM                   VARCHAR2(25)
DATE_NAISSANCE           DATE
POIDS                    NUMBER
ANNEE                    NUMBER
SEXE                     CHAR(1)
```

On fait appel à :

```sql
SELECT * FROM ELEVES;
```

pour lister le contenu de la table.

```bash
Lalistedescontraintescréées,surlatableETUDIANTS,est:
 NUM_ELEVE NOM           PRENOM   DATE_NAI POIDS  ANNEE S
---------- ------------- -------- -------- ------ ------ -
1          Brisefer      Benoit   10/12/78     35      1 M
2          Génial        Olivier  10/04/78     42      1 M
3          Jourdan       Gil      28/06/74     72      2 F
4          Spring        Jerry    16/02/74     78      2 M
5          Tsuno         Yoko     29/10/77     45      1 F
6          Lebut         Marc     29/04/74     75      2 M
7          Lagaffe       Gaston   08/04/75     61      1 M
8          Dubois        Robin    20/04/76     60      2 M
9          Walthéry      Natacha  07/09/77     59      1 F
10         Danny         Buck     15/02/73     82      2 M
```

4.  Ajout des attributs suivants

```sql
ALTER TABLE ELEVES ADD (
	CodePostal NUMBER(5),
	Ville  VARCHAR(20)
);
```

5.  MAJ des adresses des élèves

```sql
UPDATE ELEVES SET Ville = 'PARIS', CodePostal = 75013 WHERE num_eleve = 1;
UPDATE ELEVES SET Ville = 'EPINAY / seine', CodePostal = 93800 WHERE num_eleve = 2;
UPDATE ELEVES SET Ville = 'EPINAY SUR SEINE', CodePostal = 93430 WHERE num_eleve = 5;
UPDATE ELEVES SET Ville = 'EPINAY / ORGE', CodePostal = 91000 WHERE num_eleve = 7;
```

contenue de la table eleves après update :

```bash
NUM_ELEVE  NOM       PRENOM    DATE_NAI  POIDS ANNEE S CODEPOSTAL VILLE
---------- -------- ---------- -------- ------ ----- - ---------- --------------------
1          Brisefer  Benoit    10/12/78  35     1    M 75013       PARIS
2          Génial    Olivier   10/04/78  42     1    M 93800       EPINAY / seine
3          Jourdan   Gil       28/06/74  72     2    F
4          Spring    Jerry     16/02/74  78     2    M
5          Tsuno     Yoko      29/10/77  45     1    F 93430       EPINAY SUR SEINE
6          Lebut     Marc      29/04/74  75     2    M
7          Lagaffe   Gaston    08/04/75  61     1    M 91000       EPINAY / ORGE
8          Dubois    Robin     20/04/76  60     2    M
9          Walthéry  Natacha   07/09/77  59     1    F
10         Danny     Buck      15/02/73  82     2    M
```


6.  Création d'un nouvelle agglomeration

```sql
CREATE TABLE AGGLOMERATION (
  CodePostal NUMBER(5),
  VILLE VARCHAR(20),
  CONSTRAINT PK_AGGLOMERATION_CP PRIMARY KEY (CodePostal)
);
```

7.  Ajout des contraintes

```sql
ALTER TABLE AGGLOMERATION ADD CONSTRAINT CK_AGGLOMERATION_VILLE CHECK (VILLE=UPPER(VILLE));
ALTER TABLE ELEVES ADD CONSTRAINT FK_ELEVES_AGGLOMERATION FOREIGN KEY (CodePostal) REFERENCES agglomeration(CodePostal);
```

8.  Parmi les données qui sont rappelées juste en dessous, lequelles vont donner une erreur ?

93800 EPINAY SUR SEINE et 93800 EPINAY/SEINE ont le même code postale or CodePostal est une clée primaire.
Villetaneuse n'est pas en UPPER donc elle ne respecte pas la contrainte que nous avons ajouté a la question 7.

```sql
INSERT INTO agglomeration values (75001,'PARIS');
INSERT INTO agglomeration values (75013,'PARIS');
INSERT INTO agglomeration values (93800,'EPINAY SUR SEINE');
INSERT INTO agglomeration values (93430,'Villetaneuse');
INSERT INTO agglomeration values (91000,'EPINAY SUR ORGE');
INSERT INTO agglomeration values (93800,'EPINAY/SEINE');
```

```bash
SQL> INSERT INTO agglomeration values (75001,'PARIS');
1 ligne cr��e.

SQL> INSERT INTO agglomeration values (75013,'PARIS');
1 ligne cr��e.

SQL> INSERT INTO agglomeration values (93800,'EPINAY SUR SEINE');
1 ligne cr��e.

SQL> INSERT INTO agglomeration values (93430,'Villetaneuse')
ERREUR � la ligne 1 :
ORA-02290: violation de contraintes (L3INFO_131.CK_AGGLOMERATION_VILLE) de
v�rification

SQL> INSERT INTO agglomeration values (91000,'EPINAY SUR ORGE');
1 ligne cr��e.

SQL> INSERT INTO agglomeration values (93800,'EPINAY/SEINE')
ERREUR � la ligne 1 :
ORA-00001: violation de contrainte unique (L3INFO_131.PK_AGGLOMERATION_CP)
```

```sql
INSERT INTO agglomeration values (93430,'VILLETANEUSE');
```

9.  Mettez à jour la table des élèves pour corriger les nom des villes selon le code postal. Utilisez une seule requête. Affichez son contenu corrigé.

```sql
UPDATE ELEVES set Ville = (SELECT Ville from agglomeration where eleves.CodePostal = agglomeration.CodePostal );
select * from eleves;
```

```bash
NUM_ELEVE  NOM        PRENOM    DATE_NAISS  POIDS  ANNEE  S CODEPOSTAL VILLE
---------- ---------- ---------  ---------- ------ ------ - ---------- --------------------
1          Brisefe    Benoit     10-12-1978 3       1     M 75013      PARIS
2          Génial     Olivier    10-04-1978 42      1     M 93800      EPINAY SUR SEINE
3          Jourdan    Gil        28-06-1974 72      2     F
4          Spring     Jerry      16-02-1974 78      2     M
5          Tsuno      Yoko       29-10-1977 45      1     F 93430      VILLETANEUSE
6          Lebut      Marc       29-04-1974 75      2     M
7          Lagaffe    Gaston     08-04-1975 61      1     M 91000      EPINAY SUR ORGE
8          Dubois     Robin      20-04-1976 60      2     M
9          Walthéry   Natacha    07-09-1977 59	    1     F
10         Danny      Buck       15-02-1973 82      2     M
```
