## TP 7 : SQL Vues et Arbres ##

1.  Lancement du script famille.sql

```sql
@famille.sql
```

2.  Schémas E/A de la table.

               0,n              PERSONNES             0,n          
           Est mere de          -NOM               Est pere de       
MERE                            -PRENOM                              PERE
                                -DATENAISSANCE  
              0,1               -SEXE                 0,1
          Est enfant de                           Est enfant de  


3.  Création d'une vue  permettant d’afficher les ancêtres d’une personne avec la commande CONNECT BY

```sql
CREATE VIEW ancetres AS select * from personnes;
--
CREATE VIEW vue1 AS (SELECT * FROM PERSONNES WHERE (NUMERO != 1) START WITH NUMERO = &num CONNECT BY NUMERO = PRIOR PERE OR NUMERO = PRIOR MERE);
```

4.  Affichez le père, le grand père et l’arrière grand père d’une personne (&num) en n’utilisant pas CONNECT BY mais en utilisant plusieurs vues intermédiaires

```sql
CREATE VIEW pere AS (SELECT * FROM PERSONNES WHERE NUMERO = 3);

CREATE VIEW grandpere AS (SELECT * FROM PERSONNES WHERE NUMERO = (select pere from pere));

CREATE VIEW arrieregrandpere AS (SELECT * FROM PERSONNES WHERE NUMERO = (select pere from grandpere));

CREATE VIEW ancetres2 AS (SELECT * FROM pere union select * from grandpere union select * from arrieregrandpere );

select * from ancetres2;
```

5.  Affichez tous les descendants d’une personne.

```sql
SELECT * FROM personnes start with  numero = 99 CONNECT BY pere = prior numero or mere = prior numero;
```

6.  Affichez les frères et sœurs d’une personne.

```sql
CREATE VIEW parents AS (select PERE,MERE FROM PERSONNES WHERE NUMERO =
10);

CREATE VIEW FS As (select * from personnes where numero != 10 and pere in(select pere from parents) and mere in(select mere from parents));

select * from FS;
```

7.  Affichez seulement les sœurs d’une personne.

```sql
select * from FS where sexe = 'F';
```

8.  Affichez tous les enfants des femmes de plus de 40 ans

```sql
select * from personnes where level = 2 start with  sexe = 'F' and ((SYSDATE - DATENAISSANCE)/365 > 40) CONNECT BY mere = prior numero;
```

9.  Affichez les cousins et cousines d’une personne.

10. Affichez les cousins issus de germain d’une personne.

11. Affichez les petits enfants de la personne la plus âgée ayant des petits enfants.

12. Pour chaque personne, affichez les nom, prénom et âge de son descendant le plus jeune.
