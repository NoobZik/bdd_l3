PROMPT ## TP 7 : SQL Vues et Arbres ##
PROMPT 

PROMPT 1.  Lancement du script famille.sql
PROMPT

@famille.sql

PROMPT
pause Press enter
PROMPT

PROMPT 3.  Création d une vue  permettant d afficher les ancêtres d’une personne avec la commande CONNECT BY
PROMPT

CREATE VIEW ancetres1 AS (SELECT * FROM PERSONNES WHERE (NUMERO != 1) START
WITH NUMERO = 1 CONNECT BY NUMERO = PRIOR PERE OR NUMERO = PRIOR MERE);

select * from ancetres1;

PROMPT
pause Press enter
PROMPT

PROMPT 4.  Affichez le père, le grand père et l’arrière grand père d’une personne (&num) en n’utilisant pas CONNECT BY mais en utilisant plusieurs vues intermédiaires
PROMPT

CREATE VIEW pere AS (SELECT * FROM PERSONNES WHERE NUMERO = 3);

CREATE VIEW grandpere AS (SELECT * FROM PERSONNES WHERE NUMERO = (select pere from pere));

CREATE VIEW arrieregrandpere AS (SELECT * FROM PERSONNES WHERE NUMERO = (select pere from grandpere));

CREATE VIEW ancetres2 AS (SELECT * FROM pere union select * from grandpere union select * from arrieregrandpere );

select * from ancetres2;

PROMPT
pause Press enter
PROMPT

PROMPT 5.  Affichez tous les descendants d’une personne.
PROMPT

SELECT * FROM personnes start with  numero = 99 CONNECT BY pere = prior numero or mere = prior numero;


PROMPT 6.  Affichez les frères et sœurs d’une personne.
PROMPT

CREATE VIEW parents AS (select PERE,MERE FROM PERSONNES WHERE NUMERO =
10);

CREATE VIEW FS As (select * from personnes where numero != 10 and pere in(select pere from parents) and mere in(select mere from parents));

select * from FS;

PROMPT
pause Press enter
PROMPT

PROMPT 7.  Affichez seulement les sœurs d’une personne.
PROMPT

select * from FS where sexe = 'F';

PROMPT
pause Press enter
PROMPT

PROMPT 8.  Affichez tous les enfants des femmes de plus de 40 ans
PROMPT

select * from personnes where level = 2 start with  sexe = 'F' and ((SYSDATE - DATENAISSANCE)/365 > 40) CONNECT BY mere = prior numero;

